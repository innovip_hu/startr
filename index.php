<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'fooldal';
		include 'config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">

	<script>
		 $(window).on("load", function() {
			$('.fool_bizt_box').each(function(){  
				var highestBox = 0;
				$('h3', this).each(function(){

					if($(this).height() > highestBox) 
					   highestBox = $(this).height(); 
				});  
				$('h3',this).height(highestBox);
			});  
			$('.fool_bizt_box').each(function(){  
				var highestBox = 0;
				$('.box2', this).each(function(){

					if($(this).height() > highestBox) 
					   highestBox = $(this).height(); 
				});  
				$('.box2',this).height(highestBox);
			});  
			$('.miert_box').each(function(){  
				var highestBox = 0;
				$('.box1_cnt', this).each(function(){

					if($(this).height() > highestBox) 
					   highestBox = $(this).height(); 
				});  
				$('.box1_cnt',this).height(highestBox);
			});  
		});
	</script>
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <!--========================================================
                              CONTENT
    =========================================================-->
    <main>
        <section>
            <div class="camera_container">
                <div id="camera" class="camera_wrap">
                    <?php
                        $query = "SELECT * FROM ".$webjel."slider ORDER BY sorrend ASC";
                        foreach ($pdo->query($query) as $row)
                        {
                            $slider_class = 'camera-text';
                            if ($row['nev'] == "")
                            {
                                $slider_class = '';
                            }
                            print '

                            <div data-src="images/termekek/'.$row['kep'].'">
                                <div class="camera_caption fadeIn">
                                    <div class="container">
                                        <div class="row">
                                            <div class="grid_6">
                                                <div class="'.$slider_class.'">
                                                    <p>'.$row['nev'].'</p>';
                                                    if ($row['szoveg'] != '') {
                                                        print '<p style="font-size: 18px; line-height: 22px;">'.$row['szoveg'].'</p>';
                                                    }
                                                    if($row['link'] != '')
                                                    {
                                                        print '<a class="btn btn-sm btn-active btn-lg margtop10" href="'.$row['link'].'" >Tovább</a>';
                                                    }                                      
                                                    print '              
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            ';
                        }
                    ?>                    
                </div>
            </div>
        </section>
        <?php /*
        <section class="well1">
            <div class="container">
                <div class="row">
                    <div class="grid_3">
                        <div class="imgs-block">
                            <img class="mr1" src="images/page1_img01.jpg" alt="Egyéni és családi biztosítások">
                            <img src="images/page1_img02.jpg" alt="Vállalkozások, vállalatok biztosításai">
                            <img src="images/page1_img03.jpg" alt="Fundamenta lakáskassza termék">
                        </div>
                    </div>
                    <div class="grid_9">
                        <h2>Üdvözöljük weboldalunkon!</h2>
                        <h3 class="padtop0">Startoljon a biztonságra</h3>
                        <p>Alkusz cégünk a minőségi ügyfélkiszolgálást szem előtt tartva látja el közvetítői feladatait Ön és a Biztosítók között. Szaktudásunkkal, teljes körű ügyintézéssel szolgáljuk az Ön kényelmét.</p>
                    </div>
                </div>
            </div> 
        </section>
        */ ?>
        <section class="bg01 well2">
            <div class="container">
                <div class="row fool_bizt_box">
                    <div class="grid_12 wow zoomIn">
                        <h4>Biztosítások széles tárháza!</h4>
                    </div>
                    <div class="grid_3 wow zoomIn">
                        <div class="box2 box2_color6" data-equal-group="1">
                            <img src="images/page1_icon01.png" alt="Gépjármű biztosítás">
                            <h3>Gépjármű biztosítások</h3>
                            <a class="btn btn_white margtop20" href="/gepjarmu-biztositasok/">Tovább</a>
                        </div>
                    </div>
                    <div class="grid_3 wow zoomIn">
                        <div class="box2 box2_color9" data-equal-group="1">
                            <img src="images/page1_icon02.png" alt="Vagyonbiztos- és felelősségbiztosítások">
                            <h3>Vagyon- és felelősségbiztosítások </h3>
                            <a class="btn btn_white margtop20" href="/vagyon-biztositasok/">Tovább</a>
                        </div>
                    </div>
                    <div class="grid_3 wow zoomIn">
                        <div class="box2 box2_color7" data-equal-group="1">
                            <img src="images/page1_icon03.png" alt="Élet-, egészség-, baleset biztosítás">
                            <h3>Élet-, egészség-, balesetbiztosítások </h3>
                            <a class="btn btn_white margtop20" href="/eletbiztositasok/">Tovább</a>
                        </div>
                    </div>
                    <div class="grid_3 wow zoomIn">
                        <div class="box2 box2_color8" data-equal-group="1">
                            <img src="images/page1_icon04.png" alt="Utasbiztosítás">
                            <h3>Utasbiztosítások</h3>
                            <a class="btn btn_white margtop20" href="/utasbiztositasok/">Tovább</a>
                        </div>
                    </div>
                </div>
            </div>   
        </section>
		
        <section class="bg01 padbot50">
            <div class="container">
                <div class="banner">
                    <div class="banner-right">
                        <span>+36 62 548 696</span>
                    </div>
                     <div class="banner-title">
                        <span>Keressen minket bizalommal!</span>
                    </div>
                </div>   
            </div>
        </section>
		
        <section class="well3">
            <div class="container">
                <div class="row">                  
                    <div class="grid_6 wow zoomIn">
                        <h3>Hírek</h3>
						<?php
							$datum = date("Y-m-d");
							$query = "SELECT * FROM `".$webjel."hirek` Where `datum` <= '".$datum."' ORDER BY `datum` DESC, `id` DESC LIMIT 2";
							$szamlali_hir = 0;
							foreach ($pdo->query($query) as $row)
							{
								print '<div class="box3 margbot30">';
									// Kép
									$query_kep = "SELECT * FROM ".$webjel."hir_kepek WHERE hir_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
									$res = $pdo->prepare($query_kep);
									$res->execute();
									$row_kep = $res -> fetch();
									$alap_kep = $row_kep['kep'];
									if ($alap_kep != '') 
									{
										print '<div class="box3_aside">
											<a href="'.$domain.'/hirek/'.$row['nev_url'].'">
												<img class="img-border" src="'.$domain.'/images/termekek/'.$row_kep['kep'].'" alt="'.$row['nev_url'].'">
											</a>
										</div>';
									}
									print '<div class="box3_cnt">
										<div class="post-meta">
											<a href="'.$domain.'/hirek/'.$row['nev_url'].'" class="date">'.$row['cim'].'</a>
										</div>
										<p class="margbot10">'.$row['elozetes'].'</p>
										<a class="btn btn_white" href="'.$domain.'/hirek/'.$row['nev_url'].'">Tovább</a>
									</div>
								</div>';
							}
						?>
						<div class="box3 margbot30">
							<a class="btn btn_white" href="<?php print $domain; ?>/hirek/">További híreink</a>
						</div>
                    </div>                   
                    <div class="grid_6 wow zoomIn">
                        <h3>Tudta-e?</h3>
						<?php
							$datum = date("Y-m-d");
							$query = "SELECT * FROM `".$webjel."hirek2` Where `datum` <= '".$datum."' ORDER BY `datum` DESC, `id` DESC LIMIT 2";
							$szamlali_hir = 0;
							foreach ($pdo->query($query) as $row)
							{
								print '<div class="box3 margbot30">';
									// Kép
									$query_kep = "SELECT * FROM ".$webjel."hir2_kepek WHERE hir_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
									$res = $pdo->prepare($query_kep);
									$res->execute();
									$row_kep = $res -> fetch();
									$alap_kep = $row_kep['kep'];
									if ($alap_kep != '') 
									{
										print '<div class="box3_aside">
											<a href="'.$domain.'/tudta-e/'.$row['nev_url'].'">
												<img class="img-border" src="'.$domain.'/images/termekek/'.$row_kep['kep'].'" alt="'.$row['nev_url'].'">
											</a>
										</div>';
									}
									print '<div class="box3_cnt">
										<div class="post-meta">
											<a href="'.$domain.'/tudta-e/'.$row['nev_url'].'" class="date">'.$row['cim'].'</a>
										</div>
										<p class="margbot10">'.$row['elozetes'].'</p>
										<a class="btn btn_white" href="'.$domain.'/tudta-e/'.$row['nev_url'].'">Tovább</a>
									</div>
								</div>';
							}
						?>
						<div class="box3 margbot30">
							<a class="btn btn_white" href="<?php print $domain; ?>/tudta-e/">További érdekességek</a>
						</div>
                    </div>
                </div>
            </div>
        </section>
        <section class="bg01 well3">
            <div class="container">
                <h3 class="border">Miért minket válasszon</h3>
                <div class="row miert_box">
                    <div class="grid_3 wow zoomIn">
                        <div class="box1" data-equal-group="1">
                            <img src="images/1.jpg" alt="Szakmai tapasztalat">
                            <div class="box1_cnt">
                                <h5>több mint 20 éves szakmai tapasztalat</h5>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3 wow zoomIn">
                        <div class="box1" data-equal-group="1">
                            <img src="images/2.jpg" alt="Teljeskörű igényfelmérés">
                            <div class="box1_cnt">
                                <h5>teljeskörű igényfelmérés</h5>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3 wow zoomIn">
                        <div class="box1" data-equal-group="1">
                            <img src="images/3.jpg" alt="Elégedettség">
                            <div class="box1_cnt">
                                <h5>gyors és rugalmas ügyintézés</h5>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3 wow zoomIn">
                        <div class="box1" data-equal-group="1">
                            <img src="images/4.jpg" alt="Üzleti kapcsolat">
                            <div class="box1_cnt">
                                <h5>hosszú távú üzleti kapcsolat</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </section>
        <section>
            <div class="container">
                <ul class="brands-list">
                    <li><img src="images/logok/aegon.gif" alt="aegon"></li>
                    <li><img src="images/logok/allianz.gif" alt="allianz"></li>
                    <li><img src="images/logok/generali.gif" alt="generali"></li>
                    <li><img src="images/logok/genertel.gif" alt="genertel"></li>
                    <li><img src="images/logok/groupama.png" alt="groupama"></li>
                    <li><img src="images/logok/kh.jpg" alt="k&h"></li>
                    <li><img src="images/logok/signal_logo.jpg" alt="signal"></li>
                    <li><img src="images/logok/union.gif" alt="union"></li>
                    
                </ul>
            </div>
        </section>
		<?php
			include $gyoker.'/module/mod_terkep.php';
		?>
        <section>
            <div class="container">
                <ul class="brands-list">
                    <li><img src="images/logok/uniqa.gif" alt="uniqa"></li>
                    <li><img src="images/logok/pannonia_uj.jpg" alt="pannonia"></li>
                    <li><img src="images/logok/eub.gif" alt="generali"></li>
                    <li><img src="images/logok/kobe.jpg" alt="genertel"></li>
                    <li><img src="images/logok/posta.gif" alt="groupama"></li>
                    <li><img src="images/logok/ColonnadeLogo.jpg" alt="signal"></li>
                    <li><img src="images/logok/waberer_logo.jpg" alt="uniqa"></li>

                <?php /*
                    <li><img src="images/logok/aig.png" alt="aegon"></li>
                    <li><img src="images/logok/vienna_logo.jpg" alt="union"></li>
                    <li><img src="images/logok/mkb.jpg" alt="k&h"></li>
                    */ ?>
                </ul>
            </div>
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>