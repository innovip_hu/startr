<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'jogi-informaciok';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container">
                <h3 class="border">Titoktartási nyilatkozat</h3>
				
					<div class="row">
						<div class="grid_12">
							<p>Az űrlapokon a látogatóink által megadott személyes adatokat és pénzügyi információkat titokként, bizalmasan kezeljük, és az 1992. évi LXIII. számú adatvédelmi törvény értelmében harmadik fél részére nem adjuk át.</p>

							<p><strong>A hozzászólásaiban megadott személyes és pénzügyi adatokat viszont nem tudjuk titkosítani. Ügyeljen arra, hogy ne közöljön ilyen információkat írásaiban.</strong></p>

							<p>Ha oldalunkon megjelöli, hogy ügyfelünk, azzal hozzájárul ahhoz, hogy adatait a belső nyilvántartásunkban szereplő adatokkal az ügyintézés egyszerűsítés céljából összekapcsoljuk.</p>

						</div>

					</div>

            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>