<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'nyugdij-es-egeszsegpenztar';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Önkéntes nyugdíj- és egészségpénztár <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/biztositasi-termekek/">Vissza >></a></h3>
				

<h4>1.) Önkéntes nyugdíjpénztár:</h4>

<p>Az Önkéntes nyugdíjpénztár a nyugdíjcélú öngondoskodás egyik kedvező formája, tagként 16 éves kortól bárki beléphet, aki anyagi lehetőségeihez mérten kíván nyugdíj célú megtakarítást gyűjteni. A belépéssel a tagok vállalják az önkéntes nyugdíjpénztár alapszabályában meghatározott, minimum tagdíj megfizetését. Az önkéntes nyugdíjpénztár a befizetett tagdíjból működési és likviditási tartalékot képez. A tartalékképzés százalékos mértéke az éves befizetések mértékétől függ.</p>

<p>Portfolióválasztási lehetőségnek köszönhetően a tagok kockázatviselési hajlandóságukhoz mérten tudják gyarapítani a nyugdíjpénztári számlán felhalmozott összeget.</p>

<p><strong>Érdemes tudni:</strong></p>



<p><strong>Nyugdíjjogosultság megszerzése esetén a tag kérhet egyenlege terhére egyösszegű vagy határozott idejű járadék formájában történő kifizetést is.</strong></p>



<p>10 év után: a nyugdíjkorhatár elérése előtt, de a 10 év várakozási idő letelte után a hozam adómentesen, míg a tőkerész személyi jövedelemadó és egészségügyi hozzájárulás megfizetésének terhe mellett vehető fel.

<br/><strong>A tagdíjfizetést a munkáltató részben vagy egészben átvállalhatja.</strong>

<br/><strong>Adókedvezmény</strong> vehető igénybe: az egyéni tagdíjbefizetéseink után <strong>20%, de maximum 150.000 Ft </strong>adókedvezmény vehető igénybe.

<br/>Tagi kölcsön igényelhető: legalább hároméves tagság után az nyugdíjpénztári egyenleg terhére igényelhető az önkéntes nyugdíjpénztár alapszabályában meghatározott mértékig és feltétele szerint.

<br/>Banki hitelfedezetként is felhasználható meghatározott keretek között.

<br/>Örökölhető: lehetőség van kedvezményezett megadására, amely később szabadon módosítható.</p>



<h4>2.) Önkéntes egészségpénztár:</h4>

<p>Az egészségpénztárak célja és feladata az egészségügyi kockázatok csökkentése olyan szolgáltatások és termékek finanszírozásával, amely elsősorban egészségmegőrzést, betegségmegelőzést, betegségek felismerését és másodsorban a betegséggel kapcsolatos kiadások csökkentését szolgálják.</p>

<p>Az egészségpénztár alapszabálya határozza meg a minden pénztártagra kötelezően előírt egységes tagdíj mértékét.</p>

<p>Az egészségpénztári tagok befizetéseiből működési és likviditási tartalékot képez, amelynek mértékét a pénztár alapszabálya határozza meg.</p>

<p>Az egészségpénztárak pénztári kártyát bocsáthatnak ki, így szolgáltatásaik elszámolását kártyás elszámoló rendszeren keresztül is bonyolíthatják.</p>

<p><strong>Érdemes tudni</strong>

<br/>Az egyéni tagdíjbefizetés után <strong>20%, de maximum 150&nbsp;000 Ft adókedvezmény</strong> vehető igénybe.

<br/>Az egészségszámlán felhalmozott megtakarításból <strong>két évre lekötött</strong> egyenleg után további <strong>10% adókedvezmény vehető igénybe.</strong>

<br/>Kamatadó mentes megtakarítás.

<br/>Közeli hozzátartozó is igénybe veheti, amennyiben szolgáltatási kedvezményezettként a pénztárnak bejelentésre kerül.

<br/>Örökölhető: kedvezményezett megadása esetén hagyatéki eljárás nélkül a felhalmozott megtakarítás kifizethető.

<br/><strong>A tagdíjfizetést a munkáltató részben vagy egészben átvállalhatja.</strong></p>

<p><strong>Amennyiben az önkéntes egészségpénztár önsegélyező pénztárként is működik, úgy az alapszabályban meghatározott feltételek teljesülése esetén a számlán felhalmozott összeg terhére további szolgáltatások vehetők igénybe. Ide tartozhat például a szülési segély, gyermekgondozási és nevelési támogatás, munkanélküliek támogatása, rokkantsági járadék és ápolási díj kiegészítés, temetési szolgáltatás, beiskolázási támogatás, felsőoktatási költségtérítés, idősgondozás támogatása, lakáscélú jelzáloghitel törlesztésének támogatása.</strong></p>


<p><a class="link_bordo" href="<?php print $domain; ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></p>


            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>