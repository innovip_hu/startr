<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'utasbiztositasok';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Utasbiztosítások <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/biztositasi-termekek/">Vissza >></a></h3>
				

<p>Az utasbiztosítás megkötését indokolja, hogy a magyar állampolgárokat megillető társadalombiztosítási szolgáltatások jelentősen megváltoznak, ha átlépjük az országhatárt. Az EU tagállamaiban elfogadott EEK kártya, részben megoldás, azonban nem fedezi a szállítás és az esetlegesen előforduló önrész költségeit. Sokan meglepődtek, amikor a statisztikusok kimutatták, hogy külföldön körülbelül ugyanannyi váratlan esemény (baleset, betegség) történik velünk, mint idehaza. A külföldön minket érő ilyen jellegű események jóval kellemetlenebbek az itthoniaknál, elsősorban az idegen környezet, kultúra, szokások, az eltérő gazdasági viszonyok, vagy éppen a nyelvi nehézségek okán.</p>

<p><strong>Fedezet balesetre, betegségre, poggyászkárra:</strong></p>

<p>A biztosítók által kidolgozott utasbiztosítások összevont kockázatúak: fedezetet ígérnek baleset, betegség és a poggyászban keletkezett károk esetén is. (Hivatalos neve ezért betegség-, baleset- és poggyászbiztosítás, rövidítve: BBP).</p>

<p>A baleseti kockázat hagyományos baleset-biztosítást tartalmaz, a poggyászkockázat pedig vagyonbiztosítást.</p>

<p>A legfontosabb azonban a betegségi kockázat, amely a külföldön bekövetkezett, sürgős orvosi beavatkozást igénylő esetekre nyújt anyagi fedezetet.</p>

<p><strong>Utasbiztosítások különböző úticélokra:</strong>
<br/>Városnézésre, körutazásra
<br/>Tengerparti üdülésre, egzotikus utazáshoz
<br/>Hajós körutazásra
<br/>Repülős utazásokra
<br/>Téli sportokra
<br/>Búvárkodásra
<br/>Hegymászásra
<br/>Fizikai munkavégzésre
<br/>Üzleti útra
<br/>Sztornó (útlemondás) biztosítás
<br/>Diákok utazására, szakmai gyakorlatára</p>



<p><a class="link_bordo" href="<?php print $domain; ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></p>




            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>