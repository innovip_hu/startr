<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'eletbiztositasok';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Élet-, egészség és balesetbiztosítások <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/biztositasi-termekek/">Vissza >></a></h3>
				

<p>Összegbiztosítások esetén a <strong>szerződő</strong> (a biztosítást megkötő és fizető fél) és a biztosító a biztosított hozzájárulásával olyan biztosítási szerződést köt, hogy a szerződő díjfizetés ellenében valamilyen, a <strong>biztosított</strong> életével kapcsolatos esemény bekövetkezése esetén a biztosító szolgáltatást (bizonyos összeget) nyújt. A biztosított hozzájárulása nélkül kötött biztosítási szerződésnek a <strong>kedvezményezett</strong> kijelölésére vonatkozó része semmis.</p>

<p>Azonos biztosítási érdekre és azonos biztosítási kockázatokra több biztosítás is köthető a biztosítási szolgáltatások halmozhatóak.</p>



<p>Ide tartoznak:</p>



<h4>I.) Életbiztosítások:</h4>



<p>Az életbiztosítási szerződés alapján a biztosító a természetes személy biztosított:</p>

<ul>
	<li>halála, vagy</li>
	<li>meghatározott időpont elérése, vagy</li>
	<li>az egészségi állapot változás bekövetkezése esetén</li>
</ul>

<p style="margin-left:3.0pt">a szerződésben meghatározott biztosítási összeg kifizetésére, járadék élethosszig tartó vagy meghatározott időszakra történő folyósításra vállal kötelezettséget</p>



<p>1.) <strong>Kockázat életbiztosítások,</strong> amelyeknek sem lejárati szolgáltatása, sem visszavásárlási értékük nincs. A szerződő díjfizetése ellenében a biztosító vállalja, hogy a biztosított halála esetén a biztosítási összeget kifizeti. Ebben az esetben a kifizetéssel a biztosítási szerződés megszűnik. Ha a tartam lejártakor él a biztosított, akkor a szerződés kifizetés nélkül szűnik meg. Erre a biztosítási fajtára adókedvezmény nem vehető igénybe, mivel az csak kockázati elemet tartalmaz.</p>



<p>2.) <strong>Hagyományos életbiztosítások:</strong> A hagyományos életbiztosítások legfőbb jellemzője, hogy garantált hozamot nyújtanak (2-2.5 %) ,valamint a garantált hozamon felül a megtakarításon elért nyereség 80-100%-át is jóváírják a biztosítók. Így <strong>nem a szerződő viseli a befektetési kockázatot</strong> és elkerüli az esetleges befektetési veszteségeket.</p>

<p>Nyugdíjbiztosítási záradékkal kiegészíthetőek, ha annak feltételi teljesülnek.</p>



<p>A.) Elérési életbiztosítás<strong>:</strong> A szerződő díjfizetése ellenében a biztosító vállalja, hogy amennyiben a biztosított egy adott időszak végén életben van, úgy a biztosítási összeget kifizeti.<br />
Míg a kockázati életbiztosításoknál a biztosítási funkció az elsődleges, az elérési biztosításoknál a megtakarítás, melynek célja lehet egy pénzösszeg előteremtése jövőbeli terveink megvalósításához.</p>



<p>B.) Vegyes életbiztosítás: A vegyes életbiztosítás egy azonos tartamra szóló (de nem szükségszerűen azonos biztosítási összegre kötött) kockázati és elérési biztosítás kombinációja. A biztosított tartam alatti halála vagy a tartam végének életben történő elérésekor egyaránt fennáll a biztosító teljesítési kötelezettsége. A biztosító tehát minden esetben szolgáltat, hiszen a két esemény közül az egyik (tartamon belüli halál vagy a lejárat elérése) mindenképpen bekövetkezik.</p>



<p>C.) Kötött időpontban lejáró (ún. terme fix) életbiztosítás: A szerződő díjfizetése ellenében a biztosító vállalja, hogy a biztosítási tartam végén, függetlenül attól, hogy a biztosított életben van-e, a biztosítási összeget kifizeti. A biztosítás díjfizetési periódusa a tartam végéig, de legfeljebb a biztosított haláláig tart, a biztosító a tartamon belül bekövetkező halál esetén átvállalja a díjfizetést.</p>

<p>A kötött időpontú életbiztosítással az előre látható események finanszírozásához szükséges tőke felhalmozásáról gondoskodhatunk, például a gyermek taníttatási költségeinek fedezetéről.</p>



<p>3.) <strong>Befektetéshez kötött (unit linked) életbiztosítások:</strong> A befektetéshez kötött életbiztosítások a vegyes életbiztosítás (kifizetés halálesetkor, vagy tartam végén) és a befektetési alapok által kínálta lehetőségek kombinációja.<br />
A konstrukció lényege, hogy a befizetett díjakat a biztosító által felkínált és a szerződő által kiválasztott alacsony, közepes, vagy magas kockázatú befektetési alapokba fektetik.<br />
A befektetési döntéseket a szerződő hozza meg, így az ezzel járó pénzügyi kockázatot is neki kell vállalnia.</p>

<p>Lényeges, hogy a befektetés és a biztosítás költségeit a biztosítási feltételek kondíciós listái tartalmazzák és ezekről a szerződő biztosítás megkötésekor tájékozódhat.</p>



<p>Ez a fajta biztosítás azoknak ajánlható, akiknek fontosak az alábbi szempontok:<br />
- A célok megvalósításához szükséges tőke részletekben is összegyűjthető.<br />
- Lehetőség van rendkívüli megtakarítások elhelyezésére.<br />
- Időközben kivehető a tőke és a hozamok egy része.<br />
- A nyugdíjbiztosítási záradékkal megkötött szerződések után adókedvezmény vehető igénybe.<br />
<strong>- </strong>A szerződő hozamigénye és kockázattűrő képessége szerint irányíthatja megtakarított pénze befektetését és lehetősége van a pénzpiaci mozgásokban rejlő lehetőségek kiaknázására.</p>



<p>4.)<strong> Nyugdíjbiztosítás:</strong> Hagyományos, vagy befektetéshez kötött életbiztosítás is lehet.</p>



<p><strong>Legfőbb jellemzői:</strong></p>



<ul>
	<li>Egyesíti a nyugdíjcélú megtakarítás és az életbiztosítás előnyeit. Az Szja TV-ben meghatározott adó-visszatérítés igényelhető.</li>
	<li>A szerződő az a személy, aki a biztosítást megköti és a biztosítási díjakat fizeti, eltérhet a biztosítottól, vagy más számára is köthet nyugdíjbiztosítást, adó-visszatérítést is igénybevéve. Biztosított olyan természetes személy lehet, aki legalább 5 év múlva tölti be a nyugdíjkorhatárt, jelenleg nem jogosult nyugdíjellátásra, nem indította meg az eljárást és nem állapítottak meg nála legalább 40 %-ot elérő egészségkárosodást.</li>
	<li>Mikor szolgáltat?</li>
	<li>öregségi nyugdíjkorhatár elérésekor,</li>
</ul>

<p style="margin-left:36.0pt">-&nbsp;&nbsp;&nbsp;&nbsp; saját jogú nyugdíjellátási szolgáltatás esetén, ha ez az öregségi nyugdíjkorhatár előtt következik be,</p>

<ul>
	<li>egészségkárosodásra szóló szolgáltatás, ha az egészségkárosodás 40-100 % között van,</li>
	<li>haláleseti szolgáltatás a biztosított halála esetén.</li>
	<li>A biztosító szerződéssel kapcsolatos tevékenységeinek fedezésére különböző költségeket számol fel. Ezeket a befizetet díjakból, befektetett megtakarításokból vonja le. A költségeket jellemzően kondíciós lista, vagy függelék tartalmazza.</li>
	<li>Az alapbiztosításra befizetett díjak 20 %-át a biztosítás magán személy szerződője az összevont adóalapja után megfizetett szja-ból visszaigényelheti. Ez max. 130&nbsp;000 Ft lehet. Az összeget a NAV a nyugdíjbiztosításra utalja át. A szerződés lejárat előtti megszüntetése estén a szerződésen korábban jóváírt adó-visszatérítéseket büntetőkamatokkal vissza kell utalni a NAV számára.</li>
</ul>



<p style="margin-left:3.0pt">5.) <strong>Temetési biztosítások:</strong> Olyan rendszeres, vagy egyszeri díjas életbiztosítások, melyek lejárati összegük nagyságát tekintve fedezik a temetési költségeket.</p>





<p><a class="link_bordo" href="<?php print $domain; ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></p>



<h4>II.) Egészségbiztosítás:</h4>



<p>Az egészségbiztosítási szerződés alapján a biztosító a biztosított megbetegedése esetén a szerződésben meghatározott szolgáltatások teljesítésére vállal kötelezettséget. A biztosító szolgáltatása kiterjedhet a szerződés típusától függően a szerződésben meghatározott egészségügyi szolgáltatások egészséges személy általi igénybevételkor felmerülő költségek megtérítésére is.</p>



<p><strong>Szolgáltatási formák lehetnek: </strong></p>



<p>1.) Kórházi napi térítéses biztosítás, ha a biztosítottat kórházban fekvőbetegként ápolják.</p>

<p>2.) Műtéti térítéses biztosítás és ennek kiegészítő biztosítása: az amputáció, csonkolás vagy érzék elvesztésének biztosítása. A térítés mértéke a biztosítási szerződésben szereplő összegtől és a műtét orvosi besorolásától függ.</p>

<p>3.) Gyógyulási támogatási biztosítás további anyagi támaszt jelent kórházi ápolás, műtét esetén.</p>

<p>4.) Kiemelt kockázatú betegségekre vonatkozó biztosítás daganatos megbetegedés, szívinfarktus, agyi érkatasztrófa, krónikus veseelégtelenség, AIDS, szívkoszorúér-műtét (by-pass) diagnosztizálása, illetve műtéte, valamint a fenti okokból bekövetkező halál esetén nyújt támogatást.</p>

<p>5.) Végleges, 100%-os munkaképesség-csökkenésre vonatkozó biztosítás egyszeri, a biztosítási szerződésben szereplő összeg kifizetése.</p>

<p>6.) Keresőképtelenség napi térítéses biztosítása a keresőképtelen állomány meghatározott napjától.</p>

<p>7.) Emelt szintű kórházi szolgáltatás (külön szoba, telefon, televízió, à la carte étkezés).</p>

<p>8.) Egészségügyi szűrővizsgálatok.</p>



<p>A biztosító szolgáltatása felhasználható belföldi vagy külföldi gyógykezelésre, a megváltozott munkalehetőségekből adódó jövedelem kiesés finanszírozására, a családról való gondoskodásra, az orvosi költségek, a betegápolás finanszírozására.</p>



<p><a class="link_bordo" href="<?php print $domain; ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></p>



<h4>III.) Balesetbiztosítás:</h4>



<p>A balesetbiztosítási szerződés alapján különösen a biztosított baleset miatt bekövetkező halála, balesetből eredő rokkantsága esetére a szerződésben meghatározott biztosítási összeg, vagy járadék fizetésére, vagy a szerződésben meghatározott egyéb szolgáltatásra vállal kötelezettséget.</p>

<p>A baleset egy olyan - a biztosított akaratától függetlenül, hirtelen fellépő, váratlan, előre nem látható, véletlenszerű - külső ok, amelynek következtében a biztosított életét veszti, vagy testi épségében, egészségében károsodik.</p>



<p>Biztosítható kockázatok lehetnek:</p>

<ol>
	<li>baleseti halál</li>
	<li>balesetből eredő maradandó egészségkárosodás</li>
	<li>kullancs okozta bénulás</li>
	<li>égési sérülés</li>
	<li>csonttörés</li>
	<li>baleseti eredetű kórházi napi térítés</li>
	<li>baleseti műtéti térítés</li>
	<li>személyi felelősségbiztosítás</li>
	<li>tanulási támogatás</li>
	<li>közlekedési baleseti halál</li>
	<li>közlekedési baleseti rokkantság</li>
</ol>

<p>A felsorolás nem teljes körű.</p>



<p>A biztosító szolgáltatása felhasználható a kedvezményezett választásától függően új életforma kialakítására, hitel visszafizetésére, a megváltozott munkalehetőségekből adódó jövedelem kiesés finanszírozására, a családról való gondoskodásra, az orvosi költségek, a betegápolás finanszírozására.</p>



<p><a class="link_bordo" href="<?php print $domain; ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></p>


            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>