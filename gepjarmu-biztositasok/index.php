<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'gepjarmu-biztositasok';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Gépjármű biztosítások <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/biztositasi-termekek/">Vissza >></a></h3>
				
				<h4>1.) Kötelező gépjármű-felelősségbiztosítás (Röviden: Kgfb)</h4>

				<p>A kötelező gépjármű-felelősségbiztosítás a 2009. évi LXII. Törvény által szabályozottan a közúti forgalomban való részvétel feltétele. A biztosítási szerződés megkötésére az üzembentartó köteles. A gépjármű tulajdonjog/üzembentartói jog megszerzése esetén az új üzembentartó a biztosítási szerződés megkötésére haladéktalanul köteles. Amennyiben törvényi kötelezettségének az üzembentartó nem tesz eleget, úgy fedezetlenségi díjat köteles fizetni. A törvényi kötelezettség a hatósági engedélyre és jelzésre kötelezett gépjárművekre vonatkozik, amelyeket beépített erőgép hajt, továbbá ebbe a körbe tartozik a pótkocsi, a félpótkocsi, a mezőgazdasági vontató, a négykerekű segédmotoros kerékpár (quad), lassú jármű és munkagép, továbbá hatósági engedélyre és jelzésre nem kötelezett segédmotoros kerékpár.
				A biztosított által mások vagyonában, testi épségében okozott károkra nyújt fedezetet önrész alkalmazása nélkül.</p>

				<p><a class="link_bordo" href="<?php print $domain; ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></p>

				<h4>2.) Casco</h4>

				<p>Olyan önkéntesen választható vagyonbiztosítás, amely a biztosító által vállalt alap- és kiegészítő kockázatok bekövetkezése estén nyújt fedezetet, jellemzően önrész kikötésével.  </p>

				<p><a class="link_bordo" href="<?php print $domain; ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></p>

            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>