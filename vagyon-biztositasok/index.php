<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'vagyon-biztositasok';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Vagyon és felelősségbiztosítások <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/biztositasi-termekek/">Vissza >></a></h3>
				

<h4>1.) Vagyonbiztosítások:</h4>



<p>Vagyonbiztosítás alatt értünk minden olyan biztosítást, amely valamilyen vagyontárgyra (magán és vállalkozás) és nem személyre vonatkozik. A biztosított vagyonában bekövetkezett kárt pótolja a szerződésben rögzített módon és feltételekkel. Vagyonbiztosítást csak az köthet, aki a vagyontárgy megóvásában érdekelt. Az érdekeltség a tulajdonoson kívül, a vagyontárgy őrzőjénél, szállítójánál, bérlőjénél, haszonélvezőjénél is megállapítható.</p>



<p>A vagyonbiztosítás általában kombinált többkockázatú formában gyakori, ahol a biztosító több veszélynemre (kockázatra) vállalja a kockázatviselést, majd a szolgáltatás teljesítését.</p>



<p>A vagyonbiztosítási szerződés alapján a biztosító a biztosítás feltételében meghatározott jövőbeni esemény (biztosítási esemény) bekövetkezésétől függően az eseményből eredő kár esetén a biztosított részére kártérítést nyújt. A biztosító a biztosítási szerződésben rögzített és szabályozott összegnek a megfizetésére vagy más szolgáltatás teljesítésére, a biztosított, illetőleg a másik szerződő fél pedig a díj fizetésére kötelezi magát.</p>



<p><strong>Túlbiztosítás tilalma:</strong> A biztosítási összeg nem haladhatja meg a biztosított vagyontárgy értékét. A biztosított érdek értékét meghaladó részben a biztosítási összegre vonatkozó megállapodás semmis és a díjakat megfelelően le kell szállítani. E rendelkezés ellenére is lehet biztosítási szerződést kötni valamely vagyontárgy várható értéke, továbbá helyreállításának vagy új állapotban való beszerzésének értéke erejéig.</p>

<p><strong>Többszörös biztosítás:</strong> Ha ugyanazt az érdeket több biztosító egymástól függetlenül biztosítja, a biztosított jogosult igényét ezek közül egyhez vagy többhöz benyújtani. A biztosító, amelyhez a kárigényt benyújtották, az általa kiállított fedezetet igazoló dokumentumban írt feltételek szerint és az abban megállapított összeg erejéig köteles fizetést teljesíteni, fenntartva azt a jogát, hogy a többi biztosítóval szemben arányos megtérítési igényt érvényesíthet.</p>

<p>A túlbiztosítás tilalma ebben az esetben is érvényesül.</p>



<p><a class="link_bordo" href="<?php print $domain; ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></p>



<h4>2.) Felelősségbiztosítások:</h4>



<p>Felelősségbiztosítási szerződés alapján a <strong>biztosított követelheti</strong>, hogy a biztosító a <strong>szerződésben meghatározott mértékben mentesítse őt olyan kár megtérítése, illetve sérelem díj megfizetése alól, amelyért jogszabály értelmében köteles.</strong></p>



<p>A biztosítás kiterjed az eljárási költségekre, ha e költségek a biztosító útmutatásai alapján vagy előzetes jóváhagyásával merültek fel.</p>



<p>A biztosított a szerződésben megállapított határidőn belül – a bejelentési kötelezettség megszegése esetére megállapított jogkövetkezmények mellett – köteles a biztosítónak írásban bejelenteni, ha vele szemben a szerződésben meghatározott tevékenységgel kapcsolatban kárigényt közölnek, vagy ha olyan körülményről szerez tudomást, amely ilyen kárigényre adhat alapot.</p>



<p><strong>Felelősségi károk lehetnek:</strong></p>

<ul>
	<li>Dologi károk, amely során valamely tárgy megsérül, vagy tönkremegy, ezáltal a károsult vagyonában értékcsökkenés áll be.</li>
	<li>Személyi sérüléses károk.</li>
	<li>Lehet nem vagyoni kár, vagy sérelem díj, amikor valakit a személyiségi jogában megsértenek, sérelem díjat követelhet az őt ért nem vagyoni sérelemért.</li>
</ul>



<p>A felelősségbiztosítás különböző vagyonbiztosítási szerződéseken belül is megköthető (például: lakásbiztosítás, társasház biztosítás, vagyonbiztosítás), de bizonyos területeken megköthetőek önálló szerződésként is. (Vagyonbiztosítás nélkül kötött felelősségbiztosítás illetve szakmai felelősségbiztosítások.) Érintheti a magán és vállalkozói területet egyaránt.</p>



<p><strong>A vállalkozások felelősségbiztosítása az alábbi módozatokat foglalhatja magába:</strong></p>



<ul>
	<li><strong>Tevékenységi</strong><strong> felelősségbiztosítás:</strong> a cég tevékenysége által okozott személyi és dologi károk megtérítése.</li>
	<li><strong>Szolgáltatói </strong><strong>felelősségbiztosítás:</strong> a cég által nyújtott szolgáltatás teljesítése során, illetve hibás teljesítése következtében a partnereinek okozott dologi károk térítése.</li>
	<li><strong>Munkáltatói </strong><strong>felelősségbiztosítás:</strong> a cég alkalmazásában és szerződéses viszonyban álló személyek munkabalesetei térítésére, amiért a cég a felelős.</li>
	<li><strong>Termékfelelősség</strong><strong> biztosítás:</strong> a gyártott hibás termék révén okozott személysérüléses károk és dologi károk megtérítésére.</li>
	<li><strong>Környezetszennyezési </strong><strong>felelősségbiztosítás:</strong> tevékenység folytatása közben véletlenszerű környezetveszélyezés által okozott kár megtérítése.</li>
</ul>

<p><strong>Kiterjeszthető</strong> a felelősségbiztosítás alvállalkozókra, valamint Magyarországon kívül eső munkavégzési, szolgáltatási területekre.</p>



<p>A <strong>szakmai felelősségbiztosítások</strong> a felelősségbiztosítások speciális területe. Meghatározott vállalkozások köthetik, szakmai hibával okozott kárra fizet a biztosítás. Néhány speciális, nagy felelősséggel járó szakmára jogszabály kötelezővé teszi a szakmai felelősségbiztosítás kötését.</p>

<p>A leggyakrabban előfordulók:

<br/>Egészségügyi és szociális tevékenység felelősségbiztosítása
<br/>Állat-egészségügyi tevékenység felelősségbiztosítása 
<br/>Könyvvizsgálói, adótanácsadói és könyvelői felelősségbiztosítás
<br/>Építészeti, műszaki tervezési és kivitelezői felelősségbiztosítás
<br/>Vagyonvédelmi vállalkozások, őrző és védő szolgáltatók, magánnyomozók felelősségbiztosítása
<br/>Önálló bírósági végrehajtók felelősségbiztosítása
<br/>Felszámolók felelősségbiztosítása
<br/>Vezető tisztségviselők és felügyelőbizottsági tagok felelősségbiztosítása
<br/>Társasházkezelők felelősségbiztosítása</p>


				<p><a class="link_bordo" href="<?php print $domain; ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></p>

            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>