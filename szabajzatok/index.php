<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'szabajzatok';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container">
                <h3 class="border">Belső szabályzataink </h3>
				
					<div class="row sor">
						<div class="grid_2 wow zoomIn">
							<a href="GDPR-Adatkezelesi-tajekoztato_2018.05.28.pdf" target="_blank">
							<div class="box1 muntars_box">
								<div class="box_kep"><img src="adatv.jpg"></div>
								<div class="box1_cnt">
									<p>Adatkezelési tájékoztató</p>
								</div>
							</div>
							</a>
						</div>
						<div class="grid_2 wow zoomIn">
							<a href="Panaszkezelesi_szabalyzat_Start-R_Kft.pdf" target="_blank">
							<div class="box1 muntars_box">
								<div class="box_kep"><img src="panaszk.jpg"></div>
								<div class="box1_cnt">
									<p>Panaszkezelési szabályzat</p>
								</div>
							</div>
							</a>
						</div>
						<div class="grid_2 wow zoomIn">
							<a href="Penzmosasi_szabalyzat_Start-R_Kft.pdf" target="_blank">
							<div class="box1 muntars_box">
								<div class="box_kep"><img src="penzmos.jpg"></div>
								<div class="box1_cnt">
									<p>Pénzmosási szabályzat</p>
								</div>
							</div>
							</a>
						</div>
					</div>

            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>