<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'munkatarsak';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
	<script>
		 $(window).on("load", function() {
			$('.sor').each(function(){  
				var highestBox = 0;
				$('.box_kep', this).each(function(){

					if($(this).height() > highestBox) 
					   highestBox = $(this).height(); 
				});  
				$('.box_kep',this).height(highestBox);
			});  
			$('.sor').each(function(){  
				var highestBox = 0;
				$('.muntars_box', this).each(function(){

					if($(this).height() > highestBox) 
					   highestBox = $(this).height(); 
				});  
				$('.muntars_box',this).height(highestBox);
			});  
		});
	</script>

</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container">
				<div class="row">
					<div class="grid_12">
						<h3 class="border">Cégvezetők</h3>
						
						<div class="row sor">
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/rethi-istvan.jpg" alt="Réthi István"></div>
									<div class="box1_cnt">
										<h5>Réthi István </h5>
										<p>szakmai vezető
										<br/>független biztosítási alkusz
										<br/>+36 30 308-4496
										<br/><a href="mailto:rethi.istvan@startr.hu">rethi.istvan@startr.hu</a>
										<br/><a href="mailto:rethi.istvan@korosblank.hu">rethi.istvan@korosblank.hu</a>
										<br/>MNB nyilvántartási szám: 105012771677</p>
									</div>
								</div>
							</div>
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/rethine.jpg" alt="Réthiné Bartos Zsuzsanna"></div>
									<div class="box1_cnt">
										<h5>Réthiné Bartos Zsuzsanna </h5>
										<p>ügyvezető
										<br/>független biztosítási alkusz
										<br/>+36 30 306-3294
										<br/><a href="mailto:rethine.zsuzsa@startr.hu">rethine.zsuzsa@startr.hu</a>
										<br/><a href="mailto:rethine.zsuzsa@korosblank.hu">rethine.zsuzsa@korosblank.hu</a>
										<br/>MNB nyilvántartási szám: 105031623517</p>
									</div>
								</div>
							</div>
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/szabone_dalma.jpg" alt="Szabóné Réthi Dalma"></div>
									<div class="box1_cnt">
										<h5>Szabóné Réthi Dalma</h5>
										<p>ügyvezető
										<br/>független biztosítási alkusz
										<br/>+36 30 477-8867
										<br/><a href="mailto:rethi.dalma@startr.hu">rethi.dalma@startr.hu</a>
										<br/>MNB nyilvántartási szám: 115091604557</p>
									</div>
								</div>
							</div>							
						</div>
					</div> 

					<div class="grid_12">
						<h3 class="border">Irodai munkatársak</h3>
						
						<div class="row sor">
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/adanko-edina.jpg" alt="Adankó Edina"></div>
									<div class="box1_cnt">
										<h5>Masáné Adankó Edina</h5>
										<p>független biztosítási alkusz
										<br/>+ 36 20 661-4374
										<br/><a href="mailto:adanko.edina@startr.hu">adanko.edina@startr.hu</a>
										<br/><a href="mailto:adanko.edina@korosblank.hu">adanko.edina@korosblank.hu</a>
										<br/>MNB nyilvántartási szám: 117060154353</p>
									</div>
								</div>
							</div>

							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/toth-jozsef.jpg" alt="Tóth József"></div>
									<div class="box1_cnt">
										<h5>Tóth József</h5>
										<p>független biztosítási alkusz
										<br/>+36 30 722-8273
										<br/><a href="mailto:toth.jozsef@startr.hu">toth.jozsef@startr.hu</a>
										<br/><a href="mailto:toth.jozsef@korosblank.hu">toth.jozsef@korosblank.hu</a>
										<br/>MNB nyilvántartási szám: 117020798218</p>
									</div>
								</div>
							</div>														
							<?php /*
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/novak-mariann.jpg"></div>
									<div class="box1_cnt">
										<h5>Novák Mariann</h5>
										<p>Üzleti főmunkatárs
										<br/>20/661 4374
										<br/><a href="mailto:novak.mariann@startr.hu">novak.mariann@startr.hu</a>
										<br/>MNB nyilvántartási szám: 111120579771</p>
									</div>
								</div>
							</div>
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/rethi-Zsuzsanna-Kata.jpg"></div>
									<div class="box1_cnt">
										<h5>Réthi Zsuzsanna Kata</h5>
										<p>biztosítási ügyintéző
										<br/>30/365 8465
										<br/><a href="mailto:rethi.zsuzsanna@startr.hu">rethi.zsuzsanna@startr.hu</a>
										<br/>MNB nyilvántartási szám: -</p>
									</div>
								</div>
							</div>
							*/ ?>
						</div>

					</div> 					
<?php  /*
					<div class="grid_12">
						<h3 class="border">Vállalkozók</h3>
						
						<div class="row sor">
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/bakos.jpg"></div>
									<div class="box1_cnt">
										<h5>Bakos László</h5>
										<p>20/388 3003
										<br/><a href="mailto:bakoslaci87@gmail.com">bakoslaci87@gmail.com</a>
										<br/>MNB nyilvántartási szám: 105031410656</p>
									</div>
								</div>
							</div>
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/ferencine.jpg"></div>
									<div class="box1_cnt">
										<h5>Ferenci Ferencné</h5>
										<p>20/938 7907
										<br/><a href="mailto:ferenczi@vnet.hu">ferenczi@vnet.hu</a>
										<br/>MNB nyilvántartási szám: 105021521366</p>
									</div>
								</div>
							</div>
							
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/goll.jpg"></div>
									<div class="box1_cnt">
										<h5>Goll Imre</h5>
										<p>(Domico 2009 Kft.)
										<br/>20/395 4255
										<br/><a href="mailto:domico@freemail.hu">domico@freemail.hu</a>
										<br/>MNB nyilvántartási szám: 110030415342</p>
									</div>
								</div>
							</div>
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/karacsonyine.jpg"></div>
									<div class="box1_cnt">
										<h5>Karácsonyiné Börcsök Edit Erika</h5>
										<p>70/208 4088
										<br/><a href="mailto:edit.borcsok@gmail.com">edit.borcsok@gmail.com</a>
										<br/>MNB nyilvántartási szám: 113062133257</p>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row sor">
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/najdanovics.jpg"></div>
									<div class="box1_cnt">
										<h5>Najdanocsics Alexander</h5>
										<p>70/338 7871
										<br/><a href="mailto:najdan@citromail.hu">najdan@citromail.hu</a>
										<br/>MNB nyilvántartási szám: 105031410791</p>
									</div>
								</div>
							</div>
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/ordog.jpg"></div>
									<div class="box1_cnt">
										<h5>Ördög Mihály Gábor</h5>
										<p>70/946 4477
										<br/><a href="mailto:edit.borcsok@gmail.com">ordog.mihaly@gmail.com</a>
										<br/>MNB nyilvántartási szám: 108042599862</p>
									</div>
								</div>
							</div>
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/seller.jpg"></div>
									<div class="box1_cnt">
										<h5>Seller András</h5>
										<p>(Ks Credit Bt.)
										<br/>30/619 1540
										<br/><a href="mailto:seller.andras@gmail.com">seller.andras@gmail.com</a>
										<br/>MNB nyilvántartási szám: 105031088365</p>
									</div>
								</div>
							</div>
							<div class="grid_3 wow zoomIn">
								<div class="box1 muntars_box">
									<div class="box_kep"><img src="../images/torok.jpg"></div>
									<div class="box1_cnt">
										<h5>Török Gábor Mátyásné</h5>
										<p>30/399 7202
										<br/><a href="mailto:iroda@kedves-haz.t-online.hu">iroda@kedves-haz.t-online.hu</a>
										<br/>MNB nyilvántartási szám: 113050923374</p>
									</div>
								</div>
							</div> 
						</div>
					</div> 

					*/ ?>
				</div> 
            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>