    <footer>
        <section class="top-footer">
            <div class="container well4">
                <div class="row">
                    <div class="grid_3 wow zoomIn">
                        <h6>Navigáció</h6>
                        <ul class="list2">
                          <li><a href="<?php print $domain; ?>/">Főoldal</a></li>
                          <li><a href="<?php print $domain; ?>/bemutatkozas/">Bemutatkozás</a></li>
                          <li><a href="<?php print $domain; ?>/munkatarsak/">Munkatársak</a></li>
                          <li><a href="<?php print $domain; ?>/biztositasi-termekek/">Biztosítási termékek</a></li>
                          <li><a href="<?php print $domain; ?>/kapcsolat/">Kapcsolat</a></li>
                          <li><a href="<?php print $domain; ?>/kapcsolat/">Ajánlatkérés</a></li>
                          <li><a href="<?php print $domain; ?>/aszf/Adatkezelesi_tajekoztato.pdf" target="_blank">Adatkezelési tájékoztató</a></li>
                          <li><a href="<?php print $domain; ?>/tudnivalok/">Hasznos tudnivalók</a></li>
                        </ul>
                    </div>
                    <div class="grid_3 wow zoomIn">
                        <h6 class="pb1">Elérhetőségek</h6>
                        <address class="addr" style="font-size: 12px; line-height: 18px;">
                            <p>6724 Szeged, Csemegi utca 11. fsz. 3. <br/>(bejárat a Vasas Szent Péter utca felől)</p>
                            <dl>
                                <dt>Telefon:</dt>
                                <dd>+36 62 548 696</dd>
                            </dl>
                            <dl>
                                <dt>Fax:</dt>
                                <dd>+36 62 548 695</dd>
                            </dl>
                            <dl>
                                <dt>E-mail:</dt>
                                <dd><a class="link2" href="mailto:info@startr.hu">info@startr.hu</a></dd>
                            </dl>
                        </address>
                    </div>
                    <div class="grid_3 wow zoomIn">
                        <h6 class="pb1">NYITVA TARTÁS</h6>
                        <address class="addr" style="font-size: 12px; line-height: 18px;">
                            <dl>
                                <dt>Hétfő:</dt>
                                <dd>9-16</dd>
                            </dl>
                            <dl>
                                <dt>Kedd:</dt>
                                <dd>9-16</dd>
                            </dl>
                            <dl>
                                <dt>Szerda:</dt>
                                <dd>9-16</dd>
                            </dl>
                            <dl>
                                <dt>Csütörtök:</dt>
                                <dd>9-16</dd>
                            </dl>
                            <dl>
                                <dt>Péntek:</dt>
                                <dd>9-12</dd>
                            </dl>
                            <dl>
                                <dt>Szombat:</dt>
                                <dd>zárva</dd>
                            </dl>                                                                                                                                            
                            <dl>
                                <dt>Vasárnap:</dt>
                                <dd>zárva</dd>
                            </dl>
                        </address>
                    </div>
                    <div class="grid_3 wow zoomIn">
                      <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fstartrkft%2F&tabs&width=280&height=218&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true&appId" width="280" height="218" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>  
                    </div>                                        
               </div>   
            </div>
        </section>
        <section>
            <div class="container">
                <div class="pull-right">
                  <p class="text-italic text-white">Kövessen minket: <a href="https://www.facebook.com/startrkft/" target="_blank" class="fa-facebook link" style="color: #3B5998;"></a></p>
                </div>                 
                <p class="copyright"><span class="copy-brand">Start-R Biztosítási Alkusz Kft.</span> © <span id="copyright-year"></span><p>
                Készítette: <a rel="nofollow" href="http://www.innovip.hu/" target="_blank">Innovip.hu Kft.</a>               
            </div>
       </section> 
    </footer>
<?php
// Cookie figyelmeztetés
	if(!isset($_COOKIE['oldal_ell']))
	{
		?>
		<style>
			#cookie_div {
				background-color: rgba(51,51,51,0.8);
				border-radius: 0;
				box-shadow: 0 0 5px rgba(0,0,0,0.5);
				padding: 20px 0;
				position: fixed;
				left: 0;
				right: 0;
				bottom: 0;
				z-index: 9999;
				color: white;
				text-align: center;
			}
			.cookie_btn {
				background-color: #AC1D32;
				padding: 3px 9px;
				cursor: pointer;
			}
		</style>
		<script>
			function cookieRendben() {
				document.getElementById('cookie_div').style.display = 'none';
				var expires;
				var days = 100;
				var date = new Date();
				date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();
				document.cookie = "oldal_ell=1" + expires + "; path=/";
			}
		</script>
		<div id="cookie_div" style="">A weboldalon sütiket (cookie-kat) használunk, hogy a biztonságos böngészés mellett a legjobb felhasználói élményt nyújthassuk látogatóinknak. <a href="<?php echo $domain; ?>/szabajzatok/GDPR-Adatkezelesi-tajekoztato_2018.05.28.pdf" target="_blank" style="color: white; text-decoration:underline;">További információk.</a> <span class="cookie_btn" onClick="cookieRendben()">Elfogadom</span></div>
		<?php
	}
?>