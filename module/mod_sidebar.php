	<div class="sidepanel widget_search margbot20 mobilon_eltunik">
		<form class="search_form" action="<?php print $domain; ?>/kereso/" method="GET">
			<input type="text" name="keres_nev" value="<?php if(isset($_GET['keres_nev'])) {print $_GET['keres_nev'];} else if(isset($_SESSION['keres_nev'])) {print $_SESSION['keres_nev'];} ?>" placeholder="Kereső...">
		</form>
	</div>
	<div class="term_cim sidebar_term_cim margtop0">Termékeink <span class="mobil_kat_lenyito<?php if($oldal=='termekek'){print '_allando';} ?>" id="mobil_kat_lenyito"><i class="fe icon_menu"></i></span></div>
	<ul class="topnav sidebar_topnav <?php if($oldal=='termekek'){print 'kategoria_menu_zart';} ?>" id="kategoria_menu">
	<?php
		//aktív szülőkategória meghatározása
		$szulkat = 0;
		$szul_szulkat = 0;
		$szul_szul_szulkat = 0;
		if (isset($_GET['kat_urlnev']))
		{
			$query = "SELECT * FROM ".$webjel."term_csoportok where nev_url='".$_GET['kat_urlnev']."'";
			$res = $pdo->prepare($query);
			$res->execute();
			$row  = $res -> fetch();
			$szulkat = $row['csop_id'];
			$szulkat_nev = $row['nev'];
			$szulkat_url = $row['nev_url'];
			if ($szulkat > 0)
			{
				$query = "SELECT * FROM ".$webjel."term_csoportok where id=".$szulkat;
				$res = $pdo->prepare($query);
				$res->execute();
				$row  = $res -> fetch();
				$szul_szulkat = $row['csop_id'];
				$szul_szulkat_nev = $row['nev'];
				$szul_szulkat_url = $row['nev_url'];
				if ($szul_szulkat != 0 || $szul_szulkat != '')
				{
					$query = "SELECT * FROM ".$webjel."term_csoportok where id=".$szul_szulkat;
					$res = $pdo->prepare($query);
					$res->execute();
					$row  = $res -> fetch();
					$szul_szul_szulkat = $row['csop_id'];
					$szul_szul_szulkat_nev = $row['nev'];
					$szul_szul_szulkat_url = $row['nev_url'];
				}
			}
		}
		if ($config_ovip == 'I')
		{
			$query = "SELECT * FROM ".$ovipjel."term_csoportok WHERE csop_id=0 ORDER BY sorrend asc";
		}
		else
		{
			$query = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id=0 ORDER BY sorrend asc";
		}
		foreach ($pdo->query($query) as $row)
		{
			if ($config_ovip == 'I')
			{
				$res = $pdo->prepare("SELECT COUNT(*) FROM ".$ovipjel."term_csoportok WHERE csop_id = ".$row['id']." AND lathato=0 ORDER BY sorrend asc");
			}
			else
			{
				$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id = ".$row['id']." AND lathato=0 ORDER BY sorrend asc");
			}
			$res->execute();
			$rownum = $res->fetchColumn();
			if ( $rownum > 0)
			{
				print '<li class="cat-header"><a href="#" >'.$row['nev'].'</a><span></span>';
				if ($szulkat == $row['id'] || $szul_szulkat == $row['id']) //Ha van aktív szülőkategória, akkor nincs lenyíló efekt
				{
					print '<ul style="display: block;">';
				}
				else
				{
					print '<ul>';
				}
				if ($config_ovip == 'I')
				{
					$query1 = "SELECT * FROM ".$ovipjel."term_csoportok WHERE csop_id = ".$row['id']." AND lathato=0 ORDER BY sorrend asc";
				}
				else
				{
					$query1 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id = ".$row['id']." AND lathato=0 ORDER BY sorrend asc";
				}
				foreach ($pdo->query($query1) as $row1)
				{
					if ($config_ovip == 'I')
					{
						$res = $pdo->prepare("SELECT COUNT(*) FROM ".$ovipjel."term_csoportok WHERE csop_id = ".$row1['id']." AND lathato=0 ORDER BY sorrend asc");
					}
					else
					{
						$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id = ".$row1['id']." AND lathato=0 ORDER BY sorrend asc");
					}
					$res->execute();
					$rownum = $res->fetchColumn();
					if ( $rownum > 0)
					{
						print '<li class="cat-header"><a href="#" >'.$row1['nev'].'</a><span></span>';
						if ($szulkat == $row1['id'] || $szul_szulkat == $row1['id'] || $szul_szul_szulkat == $row1['id']) 
						{
							print '<ul style="display: block;">';
						}
						else
						{
							print '<ul>';
						}
						if ($config_ovip == 'I')
						{
							$query2 = "SELECT * FROM ".$ovipjel."term_csoportok WHERE csop_id = ".$row1['id']." AND lathato=0 ORDER BY sorrend asc";
						}
						else
						{
							$query2 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id = ".$row1['id']." AND lathato=0 ORDER BY sorrend asc";
						}
						foreach ($pdo->query($query2) as $row2)
						{
							if ($row2['lathato'] == 0)
							{
								if(isset($_GET['kat_urlnev']) && $_GET['kat_urlnev'] == $row2['nev_url'])
								{
									print '<li class="cat-header"><a href="'.$domain.'/termekek/'.$row2['nev_url'].'/" class="nyitva">'.$row2['nev'].'</a></li>';
								}
								else
								{
									print '<li class="cat-header"><a href="'.$domain.'/termekek/'.$row2['nev_url'].'/">'.$row2['nev'].'</a></li>';
								}
							}
						}
						print '</ul>';
						print '</li>';
					}
					else
					{	
						if ($row1['lathato'] == 0)
						{
							if(isset($_GET['kat_urlnev']) && $_GET['kat_urlnev'] == $row1['nev_url'])
							{
								print '<li class="cat-header"><a href="'.$domain.'/termekek/'.$row1['nev_url'].'/"  class="nyitva">'.$row1['nev'].'</a><span></span></li>';
							}
							else
							{
								print '<li class="cat-header"><a href="'.$domain.'/termekek/'.$row1['nev_url'].'/" >'.$row1['nev'].'</a><span></span></li>';
							}
						}
					}
				}
				print '</ul>';
				print '</li>';
			}
			else
			{	
				if ($row['lathato'] == 0)
				{
					if(isset($_GET['kat_urlnev']) && $_GET['kat_urlnev'] == $row['nev_url'])
					{
						print '<li class="cat-header"><a href="'.$domain.'/termekek/'.$row['nev_url'].'/"  class="nyitva">'.$row['nev'].'</a><span></span></li>';
					}
					else
					{
						print '<li class="cat-header"><a href="'.$domain.'/termekek/'.$row['nev_url'].'/" >'.$row['nev'].'</a><span></span></li>';
					}
				}
			}
		}
	?>
	</ul>
<div class="mobilon_eltunik">
	<?php
		// Szűrő
		if($oldal="termekek" && isset($_GET['kat_urlnev']) && $_GET['kat_urlnev'] != '')
		{
			print '<hr class="divider_colored">';
			include $gyoker.'/webshop/termek_csoport_termek_parameterek_szuro/view.php';
		}
	?>
		
	<hr class="divider_colored">

		<h5>Utoljára megtekintett termékek</h5>
		<?php
			include $gyoker.'/webshop/utoljara_megtekintett_termekek.php';
		?>

	<hr class="divider_colored">

		<?php
			$query = "SELECT * FROM ".$webjel."banner WHERE kep!='' ORDER BY rand() LIMIT 1";
			foreach ($pdo->query($query) as $row)
			{
				$link = '';
				if($row['link'] != '')
				{
					print '<a href="'.$row['link'].'" target="_blank"><img src="'.$domain.'/images/termekek/'.$row['kep'].'" alt="'.$row['kep'].'" title="'.$row['nev'].'"/></a>';
				}
				else
				{
					print '<img src="'.$domain.'/images/termekek/'.$row['kep'].'" alt="'.$row['kep'].'" title="'.$row['nev'].'"/>';
				}
			}
		?>

	<hr class="divider_colored">

		<h5>Hírlevél feliratkozás</h5>
		<p>Értesüljön első kézből legfrissebb akcióinkról, rendezvényeinkről, nyereményjátékainkról!</p>
		<script>
			function hirlevel_ellenorzes()
			{
				var hirl_oke = 0;
				if(document.getElementById("hirl_nev").value == '')
				{
					// document.getElementById("hirl_note").style.display = 'block';
					var hirl_oke = 1;
				}
				
				if(document.getElementById("hirl_email").value == '')
				{
					// document.getElementById("hirl_note").style.display = 'block';
					var hirl_oke = 1;
				}
				
				if(hirl_oke == 1)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		</script>
		<div class="box_sarga">
			<form method="POST" action="<?php print $domain; ?>/hirlevel/" name="level" onsubmit="return hirlevel_ellenorzes()">
				<input type="text" id="hirl_nev" name="nev" value="" placeholder="Név">
				<input type="text" id="hirl_email" name="email" value="" placeholder="E-mail">
				<div class="row">
					<div class="col-lg-7 col-md-12 col-sm-12">
						<input type="checkbox" name="webshop" id="webshop" value="webshop" checked> Webshop
						<br/><input type="checkbox" name="kozbeszerzes" id="kozbeszerzes" value="kozbeszerzes" checked> Közbeszerzés
					</div>
					<div class="col-lg-5 col-md-12 col-sm-12">
						<input type="submit" value="Feliratkozás">
						<input type="hidden" name="command" value="hirl_felir">
					</div>
				</div>
			</form>
		</div>

	<hr class="divider_colored">

		<h5>Facebook</h5>
		<div class="fb-page" data-href="https://www.facebook.com/Innovip-444979048871744/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"></div>
</div>