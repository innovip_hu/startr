    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
    <link rel="icon" href="<?php print $domain; ?>/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php print $domain; ?>/css/grid.css">
    <link rel="stylesheet" href="<?php print $domain; ?>/css/camera.css">
    <link rel="stylesheet" href="<?php print $domain; ?>/css/search.css">
    <link rel="stylesheet" href="<?php print $domain; ?>/css/owl-carousel.css">
    <link rel="stylesheet" href="<?php print $domain; ?>/css/google-map.css"> 
    <link rel="stylesheet" href="<?php print $domain; ?>/css/style.css">
     <link rel="stylesheet" href="<?php print $domain; ?>/css/contact-form.css">
   <link rel="stylesheet" href="<?php print $domain; ?>/update.css">
	
<?php
  	// PDO
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsol�dni az adatb�zishoz!");
		}
	if($oldal=='hirek')
	{
		// Facebook megosztáshoz kép
		if (isset($_GET['nev_url']) && $_GET['nev_url'] != '')
		{
			$res = $pdo->prepare("SELECT * FROM ".$webjel."hirek WHERE nev_url='".$_GET['nev_url']."'");
			$res->execute();
			$row  = $res -> fetch();
			// Kép
			$query_kep = "SELECT * FROM ".$webjel."hir_kepek WHERE hir_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep == '') 
			{
				// $kep_link = ''.$domain.'/webshop/images/noimage.png';
				$kep_link = '';
			}
			else
			{
				$kep_link = ''.$domain.'/images/termekek/'.$row_kep['kep'];
			}
			print '<meta property="og:locale" content="hu_HU" />';
			print '<meta property="og:title" content="'.$row['cim'].'" />';
			print '<meta property="og:type" content="article"/>';
			print '<meta property="og:url" content="'.$domain.'/hirek/'.$_GET['nev_url'].'" />';
			print '<meta property="og:image" content="'.$kep_link.'" />';
			print '<meta property="og:description" content="'.$row['elozetes'].'" />';
			print '<meta property="og:site_name" content="'.$webnev.'" />';
			
			print '<meta name="description" content="'.$row['elozetes'].'" />
			<title>'.$row['cim'].'</title>';
		}
	}
	elseif ($oldal=='tudta-e')
	{
		// Facebook megosztáshoz kép
		if (isset($_GET['nev_url']) && $_GET['nev_url'] != '')
		{
			$res = $pdo->prepare("SELECT * FROM ".$webjel."hirek2 WHERE nev_url='".$_GET['nev_url']."'");
			$res->execute();
			$row  = $res -> fetch();
			// Kép
			$query_kep = "SELECT * FROM ".$webjel."hir2_kepek WHERE hir_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep == '') 
			{
				// $kep_link = ''.$domain.'/webshop/images/noimage.png';
				$kep_link = '';
			}
			else
			{
				$kep_link = $domain.'/images/termekek/'.$row_kep['kep'];
			}
			print '<meta property="og:locale" content="hu_HU" />';
			print '<meta property="og:title" content="'.$row['cim'].'" />';
			print '<meta property="og:type" content="article"/>';
			print '<meta property="og:url" content="'.$domain.'/tudta-e/'.$_GET['nev_url'].'" />';
			print '<meta property="og:image" content="'.$kep_link.'" />';
			print '<meta property="og:description" content="'.$row['elozetes'].'" />';
			print '<meta property="og:site_name" content="'.$webnev.'" />';
			
			print '<meta name="description" content="'.$row['elozetes'].'" />
			<title>'.$row['cim'].'</title>';
		}
		else
		{
			?>
			<meta property='og:locale' content='hu_HU'>
			<meta property='og:type' content='<?php echo $og_type; ?>'/>
			<meta property='og:title' content='<?php print $title; ?>'>
			<meta property='og:description' content='<?php print $description; ?>'>
			<meta property='og:url' content='<?php echo "{$domain}/"; ?>'>
			<meta property='og:site_name' content='<?php print $title; ?>'>
			<?php				
			print '<meta name="description" content="'.$description.'" />
			<title>'.$title.'</title>';					
		}	
	}	
	else
	{
		?>
		<meta property='og:locale' content='hu_HU'>
		<meta property='og:type' content='<?php echo $og_type; ?>'/>
		<meta property='og:title' content='<?php print $title; ?>'>
		<meta property='og:description' content='<?php print $description; ?>'>
		<meta property='og:url' content='<?php echo "{$domain}/"; ?>'>
		<meta property='og:site_name' content='<?php print $title; ?>'>
		<?php
	}
?>

    <script src="<?php print $domain; ?>/config.js"></script>
    <script src="<?php print $domain; ?>/js/jquery.js"></script>
    <script src="<?php print $domain; ?>/js/jquery-migrate-1.2.1.js"></script>

    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a> 
    </div>
    <script src="js/html5shiv.js"></script>
    <![endif]-->
 
    <script src='<?php print $domain; ?>/js/device.min.js'></script> 
	<script src='https://www.google.com/recaptcha/api.js'></script>