    <header>
        <div class="top-header">
            <div id="stuck_container" class="stuck_container">
                <div class="container">
                    <nav class="nav">
                        <ul class="sf-menu">
                            <li <?php if($oldal == 'fooldal'){ print 'class="active"'; } ?>><a href="<?php print $domain; ?>/">Főoldal</a></li>
                            <li>
                                <a href="#">Céginfó</a>
                                <ul>
                                    <li><a href="<?php print $domain; ?>/bemutatkozas/">Bemutatkozás</a></li>
                                    <li><a href="<?php print $domain; ?>/munkatarsak/">Munkatársak</a></li>
                                    <li><a href="<?php print $domain; ?>/biztosito-partnereink/">Biztosító partnereink</a></li>
                                    <li><a href="<?php print $domain; ?>/egyuttmukodo-partnereink/">Együttműködő partnereink</a></li>
                                    <li><a href="<?php print $domain; ?>/panaszkezeles/">Panaszkezelés</a></li>
                                </ul>
                            </li>
                            <li <?php if($oldal == 'szolgaltatasok'){ print 'class="active"'; } ?>><a href="<?=$domain?>/szolgaltatasaink">Szolgáltatásaink</a></li>
                            <li <?php if($oldal == 'biztositasi-termekek'){ print 'class="active"'; } ?>><a href="<?php print $domain; ?>/biztositasi-termekek/">Biztosítási termékek</a></li>
                            <li>
                                <a href="#">Információk</a>
                                <ul>
                                    <li><a href="<?php print $domain; ?>/aszf/">ÁSZF</a></li>
                                    <li><a href="<?php print $domain; ?>/jogi-informaciok/">Jogi információk</a></li>
                                    <li><a href="<?php print $domain; ?>/nyomtatvanyok/">Nyomtatványok </a></li>
                                    <li><a href="<?php print $domain; ?>/tudnivalok/">Hasznos tudnivalók</a></li>
                                    <li><a href="<?php print $domain; ?>/karrier/">Karrier</a></li>
                                    <li><a href="http://www.biztositasifeltetelek.hu" target="_blank">Biztosítási feltételek</a></li>
                                </ul>
                            </li>
                            <?php /*
                            <li>
                                <a href="#">Díjkalkuláció</a>
                                <ul>
                                    <li><a href="http://startronline.biztositasbazis.hu/online/kgfb.php" target="_blank">Gépjármű biztosítás</a></li>
                                </ul>
                            </li>
                            */ ?>
                            <li <?php if($oldal == 'kapcsolat'){ print 'class="active"'; } ?>><a href="<?php print $domain; ?>/kapcsolat/">Kapcsolat</a></li>
                        </ul>
                    </nav>
                    <ul class="social-list">
                        <li style="margin-right: 10px;"><img src="<?=$domain?>/images/home-icon-silhouette.svg" style="max-width: 20px; max-height: 20px; padding-right: 5px;" /><span><a href="#google-map" style="display: inline;">6724 Szeged, Csemegi utca 11. fsz. 3.</a></span></li>
                        <li><img src="<?=$domain?>/images/old-handphone.svg" style="max-width: 20px; max-height: 20px; padding-right: 5px;" /><span><a style="display: inline;" href="<?=$domain?>/kapcsolat">+36 62 548 696</a></span></li>
                    </ul>
                </div>
            </div>
        </div>    
        <div class="container main-header">
            <div class="brand">
                <h1 class="brand_name">
                    <div style="display: inline-flex;">
                        <div style="width: 50%;">
                            <div style="display: inline-block;">
                                <a href="<?php print $domain; ?>/"><img src="<?php print $domain; ?>/images/logo.png" alt="Start-R Biztosítási Alkusz Kft."></a>
                                <p style="color: #630d0e; padding-top: 5px; font-style: italic; text-align: center;">Startoljon a biztonságra!</p>
                            </div>
                        </div>
                        <div style="width: 50%;"><a href="https://www.korosblank.hu/irodaink/szeged-startr-kft-iroda.html" target="_blank"><img src="<?php print $domain; ?>/images/koroslogo.webp" alt="Start-R Biztosítási Alkusz Kft."></a></div>
                    </div>
                </h1>
            </div>
            <div class="hdr-right">
                <address class="hdr-address hdr-idezet">
					<?php
						$query = "SELECT * FROM ".$webjel."bolcsessegek ORDER BY RAND() LIMIT 1";
						$res = $pdo->prepare($query);
						$res->execute();
						$row = $res -> fetch();
						print '<i>"'.$row['idezet'].'" </i><br/>('.$row['szemely'].')';
					?>
                </address>
            </div>            
        </div>
    </header>

    <a href="<?=$domain?>/kapcsolat">
        <div id="oldalso_ajanlatkeres">Ajánlatkérés</div>
    </a>
