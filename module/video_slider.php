<?php $video_slider = $pdo->query("SELECT * FROM ".$webjel."video_slider WHERE kep!='' AND video!='' ORDER BY RAND() ")->fetch(); ?>
<?php if ($video_slider): ?>
<style>
	#video_slider {
		position: relative;
		margin: 0;
		border: none;
		width: 100%;
		height: 568px;
		padding: 0;
		overflow: hidden;
	}
	@media (max-width:1023px) {
		#video_slider {
			height: 426px;
		}
	}
	@media (max-width:767px) {
		#video_slider {
			height: 266px;
		}
	}
	@media (max-width:479px) {
		#video_slider {
			height: 177px;
		}
	}
	@media (max-width:319px) {
		#video_slider {
			height: 132px;
		}
	}
	#video_slider_poster {
		position: relative;
		width: 100%;
	}
	@media (max-width:767px) {
		#video_slider_video {
			display: none;
		}
	}
	#video_slider_video {
		position: relative;
		width: 100%;
	}
	#video_slider_text {
		position: absolute;
		top: 0;
		width: 100%;
	}
	#video_slider_text_title {
		display: block;
		text-align: center;
		font-size: 50px;
		color: white;
		line-height: normal;
		text-shadow: 2px 2px black;
	}
	a #video_slider_text_title:hover {
		text-shadow: 2px 2px gray;
	}
	#video_slider_text_description {
		display: block;
		text-align: center;
		font-size: 25px;
		color: white;
		line-height: normal;
		text-shadow: 2px 2px black;
	}
	@media (max-width:1023px) {
		#video_slider_text_title {
			font-size: 40px;
		}
		#video_slider_text_description {
			font-size: 20px;
		}
	}
	@media (max-width:767px) {
		#video_slider_text_title {
			font-size: 30px;
		}
		#video_slider_text_description {
			font-size: 15px;
		}
	}
	@media (max-width:479px) {
		#video_slider_text_title {
			display: none;
		}
		#video_slider_text_description {
			display: none;
		}
	}
	@media (max-width:319px) {
		#video_slider_text_title {
			display: none;
		}
		#video_slider_text_description {
			display: none;
		}
	}
</style>
<section id="video_slider">
	<img id="video_slider_poster" src="<?php echo "{$domain}/images/video_slider/{$video_slider['kep']}"; ?>" alt="" />
	<video id="video_slider_video" autoplay muted poster="<?php echo "{$domain}/images/video_slider/{$video_slider['kep']}"; ?>">
		<source src="<?php echo "{$domain}/images/video_slider/{$video_slider['video']}"; ?>" type="video/mp4">
	</video>
	<table id="video_slider_text">
		<tr>
			<td>
				<?php if (!empty($video_slider['nev'])): ?>
				<?php if (!empty($video_slider['link'])): ?><a href="<?php echo $video_slider['link']; ?>"><?php endif; ?>
				<span id="video_slider_text_title"><?php echo $video_slider['nev']; ?></span>
				<?php if (!empty($video_slider['link'])): ?></a><?php endif; ?>
				<?php endif; ?>

				<?php if (!empty($video_slider['szoveg'])): ?>
				<span id="video_slider_text_description"><?php echo $video_slider['szoveg']; ?></span>
				<?php endif; ?>
			</td>
		</tr>
	</table>
</section>
<script>
	var video_slider_height = document.getElementById('video_slider').offsetHeight;
	var video_height = document.getElementById('video_slider_poster').offsetHeight;
	document.getElementById("video_slider_poster").style.top = -((video_height - video_slider_height) / 2)+'px';
	document.getElementById("video_slider_video").style.top = -(video_height + ((video_height - video_slider_height) / 2))+'px';
	document.getElementById("video_slider_text").style.height = video_slider_height+'px';
</script>
<?php endif; ?>