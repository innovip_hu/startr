<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'biztositasi-termekek';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <!--========================================================
                              CONTENT
    =========================================================-->
    <main>
        <section class="well6">
            <div class="container">
                <h3 class="border">Biztosítási termékek</h3>
				
				<div class="row">
					<div class="grid_6">
						<h4><a href="<?php print $domain; ?>/gepjarmu-biztositasok/">Gépjármű biztosítások</a> <?php /* <span class="h4_link"><a href="http://startronline.biztositasbazis.hu/online/kgfb.php" target="_blank">on-line díjszámítás</a></span> */ ?></h4>
						<ul class="margbot20">
							<li><i class="fa fa-check"></i> Kötelező gépjármű felelősségbiztosítás (Kgfb)</li>
							<li><i class="fa fa-check"></i> Casco biztosítások</li>

						</ul>
						
						<h4><a href="<?php print $domain; ?>/vagyon-biztositasok/">VAGYON ÉS FELELŐSSÉGBIZTOSÍTÁSOK</a></h4>
						<ul class="margbot20">
							<li><i class="fa fa-check"></i> Lakásbiztosítások</li>
							<li><i class="fa fa-check"></i> Társasház biztosítások</li>
							<li><i class="fa fa-check"></i> Kis-és középvállalkozói vagyonbiztosítások</li>
							<li><i class="fa fa-check"></i> Nagyipari vagyonbiztosítások</li>
							<li><i class="fa fa-check"></i> Építés-szerelés biztosítások (C.A.R.)</li>
							<li><i class="fa fa-check"></i> Gépbiztosítások</li>
							<li><i class="fa fa-check"></i> Elektronikai berendezés biztosítások (ELBER)</li>
							<li><i class="fa fa-check"></i> Üzemszünet-biztosítások</li>
							<li><i class="fa fa-check"></i> Szállítmánybiztosítások (Belföldi, Nemzetközi)</li>
							<li><i class="fa fa-check"></i> Fuvarozói biztosítások (Belföldi – BÁF, Nemzetközi – CMR)</li>
							<li><i class="fa fa-check"></i> Mezőgazdasági biztosítások (Vagyon-, Állat- és Növény)</li>
							<li><i class="fa fa-check"></i> Kezesi biztosítások</li>
							<li><i class="fa fa-check"></i> Jogvédelem biztosítások</li>
							<li><i class="fa fa-check"></i> Hitelbiztosítások</li>
							<li><i class="fa fa-check"></i> Általános felelősségbiztosítások (Tevékenységi, Szolgáltatói, Munkáltatói, Termék, Környezetszennyezési)</li>
							<li><i class="fa fa-check"></i> Szakmai felelősségbiztosítások</li>

						</ul>
						
					</div> 
					<div class="grid_6">
						<h4><a href="<?php print $domain; ?>/eletbiztositasok/">ÉLET-, EGÉSZSÉG-, BALESETBIZTOSÍTÁSOK</a></h4>
						<ul class="margbot20">
							<li><i class="fa fa-check"></i> Kockázati életbiztosítások</li>
							<li><i class="fa fa-check"></i> Hagyományos életbiztosítások</li>
							<li><i class="fa fa-check"></i> Befektetéshez kötött (unit linked) életbiztosítások</li>
							<li><i class="fa fa-check"></i> Egészségbiztosítások (Egyéni, Családi és Csoportos)</li>
							<li><i class="fa fa-check"></i> Balesetbiztosítások (Egyéni, Családi és Csoportos)</li>
							<li><i class="fa fa-check"></i> Nyugdíjbiztosítások</li>
							<li><i class="fa fa-check"></i> Temetési biztosítások</li>
						</ul>
						
						<h4><a href="<?php print $domain; ?>/utasbiztositasok/">UTASBIZTOSÍTÁSOK</a></h4>
						<ul class="margbot20">
							<li><i class="fa fa-check"></i> Egyéni, családi, csoportos</li>
							<li><i class="fa fa-check"></i> Bérletek</li>
							<li><i class="fa fa-check"></i> Vállalati utasbiztosítások</li>
							<li><i class="fa fa-check"></i> Városnézésre, körutazásra</li>
							<li><i class="fa fa-check"></i> Tengerparti üdülésre, egzotikus utazáshoz</li>
							<li><i class="fa fa-check"></i> Hajós körutazásra</li>
							<li><i class="fa fa-check"></i> Repülős utazásokra</li>
							<li><i class="fa fa-check"></i> Téli sportokra</li>
							<li><i class="fa fa-check"></i> Búvárkodásra</li>
							<li><i class="fa fa-check"></i> Hegymászásra</li>
							<li><i class="fa fa-check"></i> Fizikai munkavégzésre</li>
							<li><i class="fa fa-check"></i> Üzleti útra</li>
							<li><i class="fa fa-check"></i> Sztornó (útlemondás) biztosítás</li>
							<li><i class="fa fa-check"></i> Diákok utazására, szakmai gyakorlatára</li>

						</ul>
						
						<h4><a href="<?php print $domain; ?>/nyugdij-es-egeszsegpenztar/">Önkéntes nyugdíj- és egészségpénztár</a></h4>
					</div> 
				</div> 
            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>