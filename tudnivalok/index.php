<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'tudnivalok';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container">
                <h3 class="border">Hasznos tudnivalók</h3>
				
				<div class="row">
					<div class="grid_6">
						<h4><a href="<?php print $domain; ?>/tudnivalok/mit-takar-a-biztositasi-alkusz-elnevezes/">Mit takar a biztosítási alkusz elnevezés?</a></h4>
						<h4><a href="<?php print $domain; ?>/tudnivalok/karrendezesi-utmutato/">Kárrendezési útmutató</a></h4>
						<h4><a href="<?php print $domain; ?>/tudnivalok/gepjarmukarral-kapcsolatos-teendok/">Gépjárműkárral kapcsolatos teendők</a></h4>
						<h4><a href="<?php print $domain; ?>/tudnivalok/kotelezo-gepjarmu-felelossegbiztositas/">Kötelező gépjármű felelősségbiztosítás - biztosító váltás</a></h4>
						<h4><a href="<?php print $domain; ?>/tudnivalok/lakasbiztositas/">Lakásbiztosítással kapcsolatos hasznos kérdések</a></h4>
					</div> 
					<div class="grid_6">
						<h4><a href="<?php print $domain; ?>/tudnivalok/tarsashaz-biztositas/">Társasház biztosítással kapcsolatos hasznos kérdések</a></h4>
						<h4><a href="<?php print $domain; ?>/tudnivalok/felelossegbiztositas/">Felelősségbiztosításokról röviden, közérthetően</a></h4>
						<h4><a href="<?php print $domain; ?>/tudnivalok/szakmai-felelossegbiztositas/">Szakmai felelősségbiztosításokról röviden, közérthetően</a></h4>
						<h4><a href="<?php print $domain; ?>/tudnivalok/szallitmanyozashoz-kotodo-biztositas/">Szállításhoz, szállítmányozáshoz kötődő biztosításokról röviden</a></h4>
					</div> 
				</div> 
				
				<div class="row">
					<div class="grid_12 margtop30">
						<p style="text-align:center;">A tájékoztatók a régi PTK hatálya alá tartozó szerződésekre vonatkoznak.</p>
					</div> 
				</div> 
            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>