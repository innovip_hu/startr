<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'mit-takar-a-biztositasi-alkusz-elnevezes';
		include '../../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Mit takar a biztosítási alkusz elnevezés? <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/tudnivalok/">Vissza >></a></h3>
				
				<p>
				Az alkusz vagy biztosítási bróker, olyan biztosításközvetítő, aki a biztosítási szerződés létrejöttét, fennmaradását, a biztosítási védelmet kereső megbízásából segíti. Gyakorlatilag az ügyfél ügynöke! Segítséget nyújt a biztosítási igény megfogalmazásában, ennek megfelelően az ajánlat és a megfelelő biztosító kiválasztásában. Több biztosító ajánlatát elemzi és bemutatja. A biztosítási szerződés véglegesítése előtt ismerteti a megkötendő szerződés feltételeit, tájékoztatja az ügyfelet a biztosító, a felügyeleti szerv és a saját fontosabb nyilvános adatairól. Képviseli az ügyfele érdekeit a biztosítási feltételek szerinti kárrendezésben a jogszerű károk érvényesítésében, a biztosító felé közvetíti az igényeit, közreműködhet a díjbeszedésben is. Az alkusz független, nem köthet olyan megállapodást, hogy az ajánlatokat csak egyetlen biztosítóhoz közvetít, hanem az alkusz a vele szerződéses kapcsolatban lévő biztosítók egymással konkurens termékeit egyaránt közvetíti. Irodával kell rendelkeznie. Az alkusz a jelenleg érvényes szabályozás szerint a biztosítóktól kap jutalékot.</p>
				<p>Amennyiben igénybe kívánja venni egy biztosítási alkusz szolgáltatásait, akkor első lépésként egy meghatalmazást kell adnia. Ez az un. alkuszi megbízás, ami feljogosítja az alkuszt, hogy a megbízója nevében és helyett, a biztosítóknál a biztosítási igényekkel kapcsolatos intézkedéseket megtegye. Az alkuszi meghatalmazást aláírva, eredetiben kell átadni a biztosítási alkusz számára.</p>
				<p>A biztosítási alkusz cégek működési formája Rt., Kft., illetve külföldi független biztosításközvetítői fióktelep formájában, a Pénzügyi Szervezetek állami Felügyelete (PSZÁF) engedélye alapján lehetséges. Az alkusz saját maga felelős az esetleges hibáiért, felelősség-biztosítással rendelkezik, melyet a 2003. évi LX. Törvény 39. &sect;(1) (Bit.) az alábbiak szerint határoz meg:</p>
				<p>Legalább káreseményenkénti 1 120 200.- euró/káresemény, és évente összesen minimum 1 680 300.- euró összegű, az egész Európai Unióra kiterjedő felelősség-biztosítással, vagy 1 680 300.- euró értékben vagyoni biztosítékkal kell rendelkeznie.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<p><a name="2"></a><strong>Az alkusz szakképesítése</strong></p>
				<p>A Biztosítási Törvény (2003. évi LX. Törvény) előírja, hogy a biztosítási alkusz rendelkezzen felsőfokú végzettséggel, vagy OKJ államilag elismert biztosításszakmai szakképesítéssel, vagy szakirányú független hatósági vizsgával (PSZÁF), és ezen okiratot az ügyfél illetve a szakhatóság kérésére be is tudja mutatni.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<p><a name="3"></a><strong>A biztosítási alkuszok nyilvántartása</strong></p>
				<p>A Biztosítási Törvény (2003. évi LX. Törvény) előírása szerint a Pénzügyi Szervezetek állami Felügyelete (PSZÁF) tartja nyilván az alkuszokat. A Felügyelet a biztosítási szektorban szereplő biztosítók, alkuszok, többes ügynökök és szaktanácsadók nyilvántartására egy on-line regisztert működtet. A biztosításközvetítői tevékenység nem kezdhető el és nem folytatható regisztrálás nélkül, vagyis biztosításközvetítői tevékenységet kizárólag olyan gazdálkodó szervezet és természetes személy végezhet, aki szerepel a Felügyelet által vezetett nyilvántartásban. A nyilvántartás egyik célja, hogy mindenki számára hozzáférhető legyen a biztosításközvetítő természetes személyek és gazdálkodó szervezetek adatai. Ezt a nagyközönség a PSZÁF honlapján érheti el.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>