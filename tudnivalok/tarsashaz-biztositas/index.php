<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'tarsashaz-biztositas';
		include '../../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Társasház biztosítással kapcsolatos hasznos kérdések <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/tudnivalok/">Vissza >></a></h3>
				
				<h4>Mire nyújt fedezetet a társasház biztosítás?</h4>
				<p>A teljes körű társasház biztosítás a társasházak, a lakóparkok és a lakásszövetkezetek számára, az épületszerkezet egészére, a teljes épületre nyújt vagyon és felelősségbiztosítási fedezetet. Lényege, hogy a lakóközösség egyetlen épületbiztosítást köt, melynek révén mind a közös tulajdonú és használatú, mind a saját tulajdonú épületrészek biztosítottak a választott veszélynemek tekintetében. így egy szerződés keretén belül biztosítva van az egész épületet.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>Egyéni lakásbiztosítások kiváltják-e a társasház biztosítást?</h4>
				<p>Ha egy társasházban minden lakó rendelkezik egyéni lakásbiztosítással, akkor tulajdoni hányad arányában a közös tulajdonú épületrészek (pince, lépcsőház, padlás, tető, lift stb.) is biztosítva vannak. A károk ügyintézése viszont bonyolult, amennyiben több különböző biztosító társaságnál vannak a lakók szerződései, ami igen valószínű.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>Mit a teendő az egyéni lakásbiztosítással, ha a ház teljes körű társasház biztosítást kötött?</h4>
				<p>Amennyiben a lakástulajdonos rendelkezik olyan teljeskörű lakásbiztosítással, melyben az ingóságain kívül a lakásának az épületrésze is biztosítva van, akkor azt módosítani szükséges. Ki kell venni a szerződésből az épületbiztosítási részt, és a továbbiakban csak az egyéni ingóságaira fogja a díjat fizetni.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>Milyen egyéb más előnyökkel járhat a társasház biztosítás?</h4>
				<p>A közös tömbbiztosítás keretén belül, olyan speciális biztosítási eseményekre is megköthető a szerződés, amelyek az egyéni lakásbiztosításokkal nem fedhetőek le. így például: közös tulajdonú ingóságok, különleges üvegek törése, tető- és panelhézag beázás, kaputelefon rongálás, liftrongálás, kerti növényzetben vagy garázsban tárolt gépjárművekben esett feltétel szerinti károk, rongálási károk, lakóközösségek jogvédelem-biztosítása.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>A lakáshitel hogyan érinti a társasház biztosítást?</h4>
				<p>A bankok megkövetelik, hogy hitelfelvételkor fedezetként felkínált ingatlan biztosítási védelemben részesüljön és ezt a szerződést a bankra engedményezze a tulajdonos, ami annyit jelent, hogy épületkár esetén a biztosító fedezetigazolásán megjelölt kárösszeg feletti kárkifizetés a bankot illetné meg, ezért a banknak engedményeznie kell a kár kifizetését a biztosított félnek, hiszen a helyreállítást a biztosított végzi. Ha lakóközösség rendelkezik társasház biztosítással, a lakásra nem kell külön épületrészt is tartalmazó biztosítást kötni, a bank köteles biztosításként a társasház biztosítást elfogadni. A társasház biztosítás meglétéről a biztosító a lakásra eső fedezetigazolást állít ki, amit a banknak át kell adni. A biztosító banki engedményezése a társasház biztosítási kötvényén is feltüntetésre kerül.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>
				
				<h4>Káresemény esetén melyik biztosító rendezi a kárt?</h4>
				<p>Épületszerkezeti káresemény esetén a kárt a társasház biztosítás biztosítója felé kell bejelenteni a közös képviselő bevonásával. Ha sérül a biztosított ingóság is a kárt az ingóságbiztosítás biztosítója felé kell bejelenteni. Egyszerűsíti a kárrendezést és a kárral kapcsolatos ügyintézést, ha mindkét biztosítás egy biztosítónál van.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>Hogy kell fizetni a társasház biztosítást?</h4>
				<p>A lakóközösség tagjai a közös költségbe építve fizetik a biztosítást a lakás nagyságával arányosan négyzetméter, vagy tulajdoni hányad megosztásban.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>Lehet-e épületértékre is szóló egyéni lakásbiztosítást kötni társasház biztosítás megléte esetén?</h4>
				<p>Igen, de csak a társasház biztosításban szereplő lakásra eső értéket meghaladó értékre. A magyar törvények alapján egy vagyontárgyra csak egy biztosítást szabad kötni. A két biztosítás megléte esetén az a biztosító fizet, amelyik szerződés korábban készült. A második biztosító csak első biztosítást meghaladó értékre, vagy veszélynemre köteles fizetni. Ezért társasház biztosítás kötése után az egyéni lakásbiztosításokat módosítani szükséges a dupla biztosítás elkerülése miatt.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>Kiterjed-e a társasház biztosítás arra, ha a lakók kárt okoznak idegennek vagy egymásnak?</h4>
				<p>A felelősségbiztosítás fedezete a lakók által az idegen harmadik személynek okozott károkon túl kiterjedhet a lakóközösségen belül a lakók egymásnak okozott káraira is.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			</div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>