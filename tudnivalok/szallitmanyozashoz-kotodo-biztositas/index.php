<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'szallitmanyozashoz-kotodo-biztositas';
		include '../../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Szállításhoz, szállítmányozáshoz kötődő biztosításokról röviden <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/tudnivalok/">Vissza >></a></h3>
				
				<p style="line-height: 1.6em;"><a class="link2" href="#1">I. Belföldi szállítmánybiztosítás</a>
				<br><a class="link2" href="#2">II. Nemzetközi szállítmánybiztosítás</a>
				<br><a class="link2" href="#3">III. Nemzetközi közúti árufuvarozói felelősségbiztosítás (CMR)</a>
				<br><a class="link2" href="#4">IV. Belföldi közúti árufuvarozói felelősségbiztosítás</a>
				<br><a class="link2" href="#5">V. Szállítmányozói felelősségbiztosítás</a>
				<br><a class="link2" href="#6">VI. Közúti árufuvarozók kezesi biztosítása.</a>
				</p>
				
				<p><strong><a class="horgony" name="1"></a>I. Belföldi szállítmánybiztosítás</strong></p>
				<p>Számos vállalkozás tevékenységéhez kapcsolódóan szállít, vagy szállíttat különböző eszközöket, termékeket rendszeresen vagy esetenként. A továbbítás során előfordulhatnak olyan események, amelyek következtében az áru megsérül, vagy megsemmisül.</p>
				<p><strong>Kinek ajánljuk?</strong></p>
				<p>Minden vállalkozás számára, akinek érdekében áll a szállított árú megóvása, legyen az a sajátja, vagy idegen tulajdonú.</p>
				<p><strong>Mire vonatkozik a biztosítás?</strong></p>
				<p>A Magyarország területén szállított áruban keletkezett olyan károkra, melyek a fuvarozás teljes folyamata alatt, beleértve a fel-, le- és átrakodásokat keletkeznek. Lehet összkockázatú (allrisk) biztosítás véletlenül, váratlanul bekövetkezett közvetlen dologi kár, melyekkel összefüggésben a biztosító a kártérítési kötelezettségét nem zárta ki, vagy lehet veszélynem szerinti (megnevezett kockázatokra szóló) biztosítás.</p>
				<p>A biztosító megtéríti azokat a ráfordításokat is, melyeket a biztosított a szállítmány mentése, az azt közvetlenül fenyegető kár elhárítása és valamely bekövetkezett kár enyhítése érdekében ésszerűnek tarthatott, amennyiben a biztosítási feltételek alapján magát a kárt megtéríti, a kármegállapítás szükséges költségeit, amennyiben a biztosítási feltételek alapján magát a kárt megtéríti.</p>
				<p>Megköthető egy útra szóló biztosításként, illetve éves keretszerződésként is.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<p><strong><a class="horgony" name="2"></a>II. Nemzetközi szállítmánybiztosítás </strong></p>
				<p>Azok a vállalkozások, amelyek export-import tevékenységet folytatnak, folyamatosan szembe kell nézniük azzal a kockázattal, hogy a szállított árut valamilyen kár éri. Legyen szó szárazföldi, tengeri, vagy légi fuvarozásról a nemzetközi szállítmánybiztosítás segít a kockázatok következményeinek minimalizálásában.</p>
				<p><strong>Kinek ajánljuk?</strong></p>
				<p>Minden vállalkozás számára, akinek érdekében áll a szállított árú megóvása, legyen az a sajátja, vagy idegen tulajdonú.</p>
				<p><strong>Mire vonatkozik a biztosítás?</strong></p>
				<p>A világ területén szállított áruban keletkezett olyan károkra, melyek a fuvarozás teljes folyamata alatt, beleértve a fel-, le- és átrakodásokat keletkeznek. A fedezet kiterjed a kényszerátrakodásokra, továbbá azon tárolásokra, útvonaltól való eltérésekre és szállítási késedelem idejére, melyek a biztosított hatáskörén kívül esnek. Lehet összkockázatú (allrisk) biztosítás véletlenül, váratlanul bekövetkezett közvetlen dologi kár, melyekkel összefüggésben a biztosító a kártérítési kötelezettségét nem zárta ki, vagy lehet veszélynem szerinti (megnevezett kockázatokra szóló) biztosítás.</p>
				<p>A biztosító megtéríti továbbá a közöskár-kiadásokat és hozzájárulást, a közös kárban hozott áldozatokat és károkat, azokat a ráfordításokat, melyeket a biztosított a szállítmány mentése, az azt közvetlenül fenyegető kár elhárítása és valamely bekövetkezett kár enyhítése érdekében ésszerűnek tarthatott, amennyiben magát a kárt a biztosítási feltételek alapján megtéríti, a kármegállapítás szükséges költségeit, amennyiben magát a kárt a biztosítási feltételek alapján megtéríti.</p>
				<p>Megköthető egy útra szóló biztosításként, illetve éves keretszerződésként is.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<p><strong><a class="horgony" name="3"></a>III. Nemzetközi közúti árufuvarozói felelősségbiztosítás (CMR)</strong></p>
				<p>Nemzetközi fuvarozói tevékenység során a fuvarozót komoly felelősség terheli, amíg a rá bízott árut vagy eszközöket eljuttatja a címzettnek. A biztosítás az áru átvétele és a címzettnek való átadása közötti időszakra jelent védelmet a fuvarozók számára.</p>
				<p><strong>Kinek ajánljuk?</strong></p>
				<p>Fuvarozóknak, akik nemzetközi fuvarozást is végeznek.</p>
				<p><strong>Mire vonatkozik a biztosítás?</strong></p>
				<p>Az áru részleges vagy teljes elveszésére, valamint az áru megsérülésére, ha a biztosított a CMR Egyezmény alapján kártérítési felelősséggel tartozik. (CMR Egyezmény a &quot;Nemzetközi Közúti árufuvarozási Szerződésről&quot; szóló egyezmény. Az egyezmény minden olyan - az áruknak közúton díj ellenében történő szállítására szóló - szerződésre érvényes, amelyeknél az áru átvételének helye, illetve a kiszolgáltatásra megjelölt hely két különböző állam területén van, és ezek közül az egyik állam a CMR Egyezmény szerződője).</p>
				<p>A szállított áruban bekövetkező károk vonatkozásában a kártérítés összege nem haladhatja meg a CMR Egyezmény, illetve az azt kiegészítő 1978. évi Protokollban rögzített mértéket, azaz a fuvarozott küldemény hiányzó illetve sérült részének bruttó súlya szerinti kilogrammonkénti 8,33 SDR-t.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<p><strong><a class="horgony" name="4"></a>IV. Belföldi közúti árufuvarozói felelősségbiztosítás</strong></p>
				<p>A fuvarozó felelősséggel tartozik megbízójának, hogy épségben leszállítsa a rábízott küldeményt, azonban előfordulhat, hogy rakománya megsérül. A biztosítás védelmet nyújt a véletlen, balesetszerűen bekövetkezett károk anyagi következményei ellen.</p>
				<p><strong>Kinek ajánljuk?</strong></p>
				<p>Fuvarozóknak, akik fuvarozási szerződés alapján szolgáltatást nyújtanak belföldön.</p>
				<p><strong>Mire vonatkozik a biztosítás?</strong></p>
				<p>A biztosított által vállalkozói szerződés alapján végzett közúti árufuvarozói tevékenység során, ill. következtében - a szerződéses partnerének, vagy azoknak, akiknek a szolgáltatást nyújtja - okozott és balesetszerűen bekövetkező dologi károkra.</p>
				<p>A biztosító megtéríti továbbá a fuvarozás során, azzal összefüggésben igazoltan felmerült fuvardíj és egyéb költségek árukárral arányos részét, a kár megállapításához, a kár elhárításához és a kárenyhítéshez szükséges ráfordítások közvetlen költségeit, valamint azon többletköltségeket, melyek a szerződés szerinti rendeltetési helyre történő továbbfuvarozás során keletkeztek, feltéve, hogy azok e szerződés hatálya alá tartozó káreseménnyel kapcsolatosan merültek fel, indokoltak voltak és ésszerű keretek között mozogtak.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<p><strong><a class="horgony" name="5"></a>V. Szállítmányozói felelősségbiztosítás </strong></p>
				<p>A szállítmányozó felelősséggel tartozik a megbízója számára megkötött fuvarozási szerződésekért, a hozzájuk kapcsolódó különböző szolgáltatásokért, a vámszabályok megsértésért, melyek anyagi következménye ellen a biztosítás védelmet nyújt.</p>
				<p><strong>Kinek ajánljuk?</strong></p>
				<p>Szállítmányozóknak, akik rendelkeznek szállítmányozási tevékenységi engedéllyel.</p>
				<p><strong>Mire vonatkozik a biztosítás?</strong></p>
				<p>Azon balesetszerűen bekövetkező tisztán vagyoni kártérítési kötelezettségekre nyújt fedezetet, amelyek a biztosítottat a magyar jogszabályok, a Polgári Törvénykönyv, a Magyar Szállítmányozók Szövetsége által elfogadott Magyar általános Szállítmányozási Feltételek, valamint az adott fuvarozási ágra vonatkozó nemzetközi egyezmények alapján szállítmányozói tevékenysége folytán megbízóival szemben terhelik.</p>
				<p>Kiterjed a szállítmányozó szállítmányozási szerződésen alapuló felelősségére, beleértve a szokványos járulékos szállítmányozói szolgáltatások alapján fennálló felelősségét is. Szokványos járulékos szállítmányozói szolgáltatásnak minősül a raktározási tevékenység, a vámügynöki és vámközvetítői tevékenység, az árutovábbításhoz kapcsolódó szállítmányozói tevékenység, illetve ezeknek a szolgáltatásoknak a megbízók részére való teljesítése, mások igénybevételével.</p>
				<p>A raktározási tevékenységen alapuló felelősség csak abban az esetben biztosított, ha a kár a biztosított szerződésben feltüntetett saját vagy bérelt raktárában következett be.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<p><strong><a class="horgony" name="6"></a>VI. Közúti árufuvarozók kezesi biztosítása.</strong></p>
				<p><strong>Kinek ajánljuk?</strong></p>
				<p>Annak a jogi személynek, jogi személyiséggel nem rendelkező gazdálkodó szervezetnek, illetve természetes személynek, aki közúti árufuvarozói engedéllyel rendelkezik, vagy ilyen engedély iránt kérelmet nyújtott be és azt a későbbiekben megkapja.</p>
				<p><strong>Mire vonatkozik a biztosítás?</strong></p>
				<p>A biztosító a biztosítási összeg erejéig teljesíti azokat a fizetési kötelezettségeket, pénzügyi követeléseket, amelyek a biztosítottat a belföldi és a nemzetközi közúti árufuvarozás szakmai feltételeiről és engedélyezési eljárásáról szóló, 14/2001. (IV.20.) KöVIM sz. rendelet (továbbiakban: rendelet) alapján terhelik.</p>
				<p>A biztosítás a rendelet 5. &sect; (5) bekezdésében meghatározott, a biztosított megfelelő pénzügyi helyzetének meglétét tanúsító vagyoni biztosítékként szolgál.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			</div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>