<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'lakasbiztositas';
		include '../../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Lakásbiztosítással kapcsolatos hasznos kérdések <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/tudnivalok/">Vissza >></a></h3>
				
				<h4>Hogyan határozzuk meg vagyontárgyaink értékét?</h4>
				<p>A lakásbiztosítás szerződésben szereplő vagyontárgyak valóságnak megfelelő értéke nagyon fontos a kárrendezés szempontjából. Kisebb értékekre kötött szerződés igaz, hogy kevesebbe kerül, de kárrendezésnél a biztosító aránylagos kártérítése (a tényleges és biztosított érték aránya szerint) miatt &Ouml;n nem lesz elégedett, mert a kártérítés nem fogja fedezni a tényleges helyreállítást.</b></p>
				<p>A fentiek elkerülésére az ingatlanokat újraépítési értéken, az ingóságokat pedig újravásárlási értéken szükséges megadni.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>Mikor érdemes a szerződést módosítani?</h4>
				<p>Általában abban a hitben élünk, hogy az évekkel korábban megkötött lakásbiztosításunk teljes körű védelmet biztosít értékeink számára. Annak ellenére, hogy a biztosító a szerződést évről évre indexálja a KSH adatai alapján, ez mégsem igaz. Az évek során eladunk, cserélünk, új dolgokat vásárolunk, a különböző biztosítók pedig egyre korszerűbb konstrukciókkal jelennek meg a biztosítási piacon. Ne felejtsük el azt sem, hogy az évenkénti indexálások a kiindulási értékeinket növelve évek alatt jelentős eltorzulásokat okozhatnak a vagyontárgyak értékeiben a szerződésen belül, ami nem valószínű, hogy a valóságnak megfelelő és olyan értékekért fizetünk egyre magasabb, szintén indexálódó díjat, aminek már nincs értelme.</p>
				<p>Ezért szükséges legalább 2-3 évente felülvizsgáltatni szerződésünket, ha szükséges akár biztosítót is kell váltanunk. Mindez egy általában olcsóbb és korszerűbb, esetenként bővebb szolgáltatást tartalmazó lakásbiztosítást eredményezhet.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>Mi a teendő kár esetén?</h4>
				<p>Leghamarabb a lehetőségekhez képest meg kell akadályozni további károk keletkezését, úgy hogy a körülmények lényegesen ne változzanak meg. Tűz esetén először a Tűzoltóságot, betörés esetén először a Rendőrséget kell értesíteni. A kárt az észlelésétől számítva 2 munkanapon belül a biztosítónak be kell jelenteni. (telefonon, levélben, portálon, személyesen) A legkevesebb fáradsággal járó és leggyorsabb ügyintézési mód a telefonon történő bejelentés, mert ilyenkor a biztosító segítségét rögtön igénybe vehetjük és a kár azonnal regisztrálásra kerül.</p>
				<p>Ahhoz, hogy a kárt megfelelően nyomon tudjuk követni, segíteni tudjunk az ügyintézésében a biztosítóhoz való bejelentés előtt érdemes a képviselőnkkel felvenni a kapcsolatot, elmondani hogy mi történt. Így segíteni tudunk a jogos kárigény biztosítási feltétel szerinti érvényesítésében.</p>
				<p>A kárról érdemes azonnal digitális fotókat készíteni, amit egyes biztosítók fel is használhatnak, de jól jöhet vitás esetekben is.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>Biztosítóváltással kapcsolatos tudnivalók.</h4>
				<p>Ha úgy dönt, hogy a meglévő biztosítását felmondja, azt csak írásban teheti meg úgy, hogy azt a biztosító legkésőbb az évforduló előtt 30 nappal átvegye. A felmondásnak tartalmaznia kell a nevét, címét, kötvényszámát, az évfordulót, hogy melyik napra mondja fel, a készítés dátumát, aláírást.</p>
				<p>A biztosítás megszűnhet díj nemfizetéssel is, de ilyenkor számolnia kell azzal, hogy a biztosító az őt megillető éves díjat akár bírósági úton is érvényesítheti. A biztosítót megilleti ilyenkor az éves díj, attól függetlenül, hogy a díj fizetésében kisebb részletekben állapodtak meg a lakásbiztosítási szerződésben.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>Mi a teendő, ha az ingatlan hitelhez szolgál fedezetként?</h4>
				<p>A bankok megkövetelik, hogy a fedezetként felkínált ingatlanra legyen kötve lakásbiztosítás. A biztosításban kedvezményezettként a bankot meg kell jelölni, ami annyit jelent, hogy épületkár esetén a biztosító által kiállított fedezetigazoláson megjelölt kárösszeg feletti kártérítés a bankot illeti meg, ezért ilyenkor a banknak engedményeznie kell a kár kifizetését a biztosított félnek, hiszen a helyreállítást a biztosított végzi. A kedvezményezett bejegyzés a biztosító által kiállított kötvényen is megjelenik.</p>
				<p>A bank nem határozhatja meg, hogy a lakásbiztosítás melyik biztosítónál legyen, a szabad biztosító és biztosítási ügyintéző választás minden esetben megilleti Önt.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			</div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>