<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'gepjarmukarral-kapcsolatos-teendok';
		include '../../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Gépjárműkárral kapcsolatos teendők <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/tudnivalok/">Vissza >></a></h3>
					
				<h4>I. A kár bejelentésére vonatkozó tudnivalók</h4>
				<p><b>Saját hibás (casco) kár:</b></p>
				<p>A káreseményt 2 munkanapon belül be kell jelenteni a biztosító telefonos ügyfélszolgálatán, honlapján, vagy személyesen az ügyfélszolgálatán.</p>
				<p><b>Idegenhibás, illetve egyéb kár</b> <b>esetén</b>:</p>
				<p> - Ki kell tölteni a másik féllel közösen az Európai baleseti bejelentőt, ami után mindkét fél rendelkezésére áll egy-egy eredeti példány. Ezen mindkét fél adatainak szerepelniük kell. A kitöltéshez a nyomtatvány hátoldalán található útmutató nyújt segítséget. Ügyelni kell a nyomtatvány teljes körű és pontos kitöltésére.</p>
				<p> - Ha nincs kéznél az Európai baleseti bejelentő, egy Gépjármű-kárbejelentési adatlapot töltsön ki!</p>
				<p>- Ha személyi sérülés is történt, vagy a másik féllel nem tudnak egyértelműen megegyezni, feltétlenül kérjen rendőri intézkedést!</p>
				<p> - Ha Ön a károkozó, kérjük, jelentse be a kárt 5 munkanapon belül saját biztosítójánál, a lakhelye szerint illetékes egységnél. (Saját kárának megtérítésére a casco biztosítás alapján van lehetőség.)</p>
				<p> - Ha Ön a károsult, kárigényét a káreseményt követő maximum 30 napon belül be kell jelentenie a károkozó biztosítójánál.</p>
				<p> - Ha a károkozó külföldi állampolgár volt, a kárt a Mabisz bármelyik tagbiztosítójánál bejelentheti. Saját érdekében fontos feljegyezni a károkozó külföldi felelősségbiztosítójának nevét, a birtokában lévő zöldkártya számát és érvényességi idejét. Ebben az esetben is feltétlenül kérjen rendőri intézkedést!</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>II. Milyen iratokra van szükség a gépjárműkár ügyintézése során?</h4>
				<p><b>1.) Casco biztosításra történő kárrendezéshez:</b></p>
				<p> - Gépjármű-kárbejelentési adatlap,</p>
				<p> - Forgalmi engedély,</p>
				<p> - Törzskönyv (nagy értékű károk esetén)</p>
				<p> - Vezetői engedély</p>
				<p> - Rendőrségi jegyzőkönyv (ha történt rendőri intézkedés),</p>
				<p> - Kárbejelentési lap teljes gépjárműlopás esetén</p>
				<p> - Teljes lopáskár esetén a rendőrségi feljelentés másolata, a nyomozást megszüntető határozat, forgalmi engedély, a rendszámtáblát visszavonó határozat, az ellopott jármű összes kulcsai (beleértve a riasztó- és egyéb vagyonvédelmi berendezések kulcsait is),</p>
				<p>- Szabálysértési vagy büntetőeljárás esetén a jogerős határozat,</p>
				<p>- Személyi sérülés kárbejelentő adatlap (amennyiben történt személyi sérülés a baleset során). Orvosi iratok, és a káresemény kapcsán felmerült költségeit igazoló számlák, bizonylatok (gyógyszerek és gyógyászati segédeszközök számlái, utazás bizonylatai, stb.).</p>
				<p><b>- </b>A biztosító az eljáró hatósági, rendvédelmi szervektől nem kapja meg közvetlenül a szükséges iratokat, ezért ezek bemutatásával gyorsíthatja, segítheti a kárrendezést!</p>
				<p><b>2.) Kötelező gépjármű-felelősségbiztosítása történő kárrendezéshez:</b></p>
				<p> - Európai baleseti bejelentő,</p>
				<p> - Gépjármű-kárbejelentési adatlap (ha nem áll rendelkezésre kitöltött Európai baleseti bejelentő)</p>
				<p> - Gépjármű-kárbejelentési adatlap,</p>
				<p> - Forgalmi engedély,</p>
				<p> - Vezetői engedély</p>
				<p> - Személyi igazolvány, lakcímkártya,</p>
				<p> - Rendőrségi jegyzőkönyv (ha történt rendőri intézkedés),</p>
				<p> - Tárgyi-kárbejelentési adatlap (amennyiben a baleset során károsodott a károsult valamely vagyontárgya).</p>
				<p>- Szabálysértési vagy büntetőeljárás esetén a jogerős határozat,</p>
				<p>- Személyi sérülés kárbejelentő adatlap (amennyiben történt személyi sérülés a baleset során). Orvosi iratok, és a káresemény kapcsán felmerült költségeit igazoló számlák, bizonylatok (gyógyszerek és gyógyászati segédeszközök számlái, utazás bizonylatai, stb.).</p>
				<p><b>- </b>A biztosító az eljáró hatósági, rendvédelmi szervektől nem kapja meg közvetlenül a szükséges iratokat, ezért ezek bemutatásával gyorsíthatja, segítheti a kárrendezést!</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>III. Személyi sérülés esetén mi a teendő?</h4>
				<p>Ha személyi sérülés történt a balesetben, mindenképp kérjék rendőr segítségét, intézkedését! Személyi sérülést külön adatlapon kell bejelenteni, a káresemény után minél hamarabb, lehetőség szerint 30 munkanapon belül.</p>
				<p>Akkor is érdemes orvoshoz fordulni, ha a balesetet követő időszakban gyanús tünetet észlel magán. A látszólag jelentéktelen sérülések utólag súlyosbodhatnak.</p>
				<p>Az orvosi iratokat, gyógyszerek és gyógyászati segédeszközök számláit meg kell őrizni, s a kárbejelentéshez csatolni kell. Minden olyan kiadásról, mely a személyi sérüléssel kapcsolatosan merült fel, kérjen számlát vagy igazolást!</p>
				<p>Ha a károkozó ismeretlen vagy külföldiállampolgár, a kárt a MABISZ bármelyik tagbiztosítójánál bejelentheti, de elsősorban saját biztosítója köteles eljárni. Saját érdekében fontos feljegyezni a károkozó külföldi felelősségbiztosítójának nevét, a birtokában lévő zöldkártya számát és érvényességi idejét. Ebben az esetben is feltétlenül kérjen rendőri intézkedést.</p>
				<p>A személyi sérüléssel kapcsolatos kárrendezési eljárás különleges jellegénél fogva elkülönül a töréskár rendezésétől: összetettebb és hosszabb folyamat.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>IV. Mi a teendő, ha külföldön történt a baleset?</h4>
				<p> - Hívja a biztosító Call Centerét.</p>
				<p> - Ne írjon alá, és a felelősség bizonyítására ne fogadjon el olyan idegen nyelvű iratot, amelyet nem ért, vagy nem tud lefordítani! Ellenkező esetben mindenképp tűntesse fel a bizonylaton azt, hogy aláírását úgy tette, hogy a dokumentum nyelvét nem ismerte, s tartalmát nem értette.</p>
				<p>- Kérjen rendőri intézkedést! Rendőri intézkedés esetén a legtöbb országban költség ellenében a történtekről igazolást kaphat.</p>
				<p> - Hasznos a gépjárműben minden esetben Európai baleseti bejelentőt tartani, amelyet a baleset után a másik féllel közösen töltsön ki, és utána az egyik példányt tartsa meg. A kitöltéshez a nyomtatvány hátoldalán található útmutató nyújt segítséget. ügyelni kell a nyomtatvány adatainak teljes körű és pontos kitöltésére.</p>
				<p>- Lehetőség szerint készítsen minél több fényképfelvételt a baleset helyszínéről, a járművekről illetve az okmányokról.</p>
				<p> - Nyomtatvány hiányában a baleset részeseinek minden adatát fel kell jegyezni, és a baleset részeseinek közösen alá kell írniuk:</p>
				<ul>
				<li>A baleset helye (az ország megjelölésével),</li>
				<li>A baleseti részesek járműveinek felségjele, forgalmi rendszáma, típusa,</li>
				<li>A vezető/tulajdonos neve, címe, telefonszáma,</li>
				<li>A vezetői engedély száma,</li>
				<li>Felelősség biztosító(k) neve, címe,</li>
				<li>A biztosítási kötvény száma,</li>
				<li>A baleset vázlata, rövid leírása,</li>
				<li>A sérülések feltüntetése,</li>
				<li>Az esetleges tanú(k) neve, címe, elérhetősége</li>
				</ul>
				<p>- Lopáskár esetén kérjen rendőri igazolást a történtekről!</p>
				<p>- Hazaérkezése után az Európai baleseti bejelentőt &ndash; vagy ennek hiányában kárbejelentését 5 munkanapon belül, ha Ön a károkozó 30 napon belül, ha Ön a károsult, 2 munkanapon belül casco biztosítása terhére jelentse be a biztosítóba.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>V. Kárrendezési eljárások</h4>
				<p><b>1.) Egyezséges kárrendezés: </b></p>
				<p>Ha számla nélkül kívánja kárigényét érvényesíteni, az anyag- és alkatrész árak, valamint munkadíjak vonatkozásában a biztosító által megállapított javítási költséget ajánlják fel egyezségre.</p>
				<p><b>2.) Számlás kárrendezés:</b></p>
				<p>A kár kifizetése a károsult nevére kiállított &ndash; a kárfelvételi jegyzőkönyvben rögzített helyreállítási módnak megfelelő javítási számla alapján történik. Javítónál történő helyreállítás esetén a biztosító javítás közbeni és utáni alkatrész beépítés ellenőrzést végezhet, melynek során a javítási számlába állított gyári alkatrészek felhasználását ellenőrzi. A gyári alkatrészek eredetének igazolásához a felhasznált alkatrész beszerzésének bizonylatát is csatolni kell. A számlás kárrendezésen belül hitelleveles kárrendezést is választható.</p>
				<p>A biztosítókról és a biztosítási tevékenységről szóló 2003. évi LX. törvény 96.&sect; (6) szerint a javítási költség áfa tartalmának a megtérítésére csak olyan számla alapján van mód, amelyen feltüntetik az áfa összegét, vagy amelyből annak összege megállapítható.</p>
				<p><b>3.) Hitelleveles kárrendezés:</b></p>
				<p>Amennyiben a bejelentett kárügy fedezete és jogalapja tisztázott, úgy kérésre hitellevelet állíthat ki a biztosító megjelölt szerződött javító partner felé. Ebben az esetben a biztosítót terhelő javítási költséggel a biztosító és a javító közvetlenül számol el egymással. A jármű helyreállítását követően csak az önt terhelő költséget (pl.: önrészesedés, avulás, saját megrendelés, stb.) kell megfizetnie a javító részére.</p>
				<p><b>4.) Totálkár rendezése: </b></p>
				<p>Erre akkor kerül sor, amikor a gépjármű javítása meghaladja a gazdaságos javítási határt, illetve a javítás műszakilag nem javasolt vagy a gépjárművet ellopták. Ebben az esetben a gépjármű megállapított káridőponti értékének a maradvány értékkel csökkentett összege alapján történik a kifizetés. A maradvány piaci értékének meghatározása roncsbörze segítségével történik, a roncsért kínált legmagasabb vételi ajánlatra figyelemmel. Amennyiben a károsodott gépjárművet értékesíteni kívánja, úgy hozzájárulása esetén a biztosító segít a maradványért legmagasabb árat kínáló ajánlattevővel történő kapcsolatfelvételben.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>VI. Kártérítés kifizetése, elévülés</h4>
				<p><b>1.) Kötelező gépjármű-felelősségbiztosítás</b></p>
				<p>A biztosító a szolgáltatását a benyújtott számlák, ezek hiányában a károsulttal kötött egyezség alapján téríti meg, amennyiben a gépjármű sérülései alapján nem állapítható meg gazdasági vagy műszaki totálkár. A biztosító a szolgáltatását a kártérítési javaslat elfogadását, illetve a kifizetéshez szükséges utolsó irat beérkezését és feldolgozását követő munkanapon teljesíti.</p>
				<p>A biztosító a biztosítási szerződésből eredő kötelezettségével összefüggésben a károsító eseményt megelőző állapot visszaállításához vagy a bekövetkezett kár következményeinek megszüntetéséhez szükséges, általános forgalmi adó-köteles szolgáltatás ellenértéke (anyag-, javítási, illetve helyreállítási költség) után az általános forgalmi adó összegének megfelelő összeg megtérítésére csak olyan számla alapján vállalhat kötelezettséget, illetve térítheti meg azt az arra jogosultnak, amelyen feltüntetik az általános forgalmi adó összegét, vagy amelyből annak összege kiszámítható.</p>
				<p><b>Elévülés</b> <br />
				A kötelező gépjármű-felelősségbiztosítási szerződés alapján keletkezett kártérítési igények a Polgári Törvénykönyv elévülési szabályai szerint évülnek el. A Ptk. 324. &sect;. (1) bekezdése szerint a követelések öt év alatt elévülnek, ha jogszabály másként nem rendelkezik. A törvény az átlagosnál rövidebb elévülési határidőt szab az ún. veszélyes üzem működéséből eredő kár megtérítése iránti követelések érvényesítésére, e követelések elévülési ideje 3 év.</p>
				<p><b>2.) Casco biztosítás</b></p>
				<p>A kár kifizetését a biztosító elemi kár, részleges lopáskár &ndash; az ellopott és meg nem került gépjármű kivételével &ndash; továbbá töréskár esetén a kárbejelentéstől, illetve a kárigény elbíráláshoz és kifizetéséhez szükséges összes irat beérkezésétől számított 15 napon belül átutalással teljesíti. A gépjármű ellopása esetén a kár kifizetése legkorábban a rendőrségi feljelentés időpontjától számított 60. napon esedékes feltéve, hogy addig a biztosító társasághoz beérkezett a rendőrségi határozat, ami a nyomozás megszüntetéséről, vagy felfüggesztéséről rendelkezik. A biztosítónak a szolgáltatást az esedékességtől számított 15 napon belül kell teljesítenie. Ha a feljelentéstől számított 60. nap elteltéig a rendőrség nem adja ki az említett határozatot, akkor általában előleg igényelhető a  biztosítási feltételekben előírtak szerint.</p>
				<p><b>Elévülés</b> <br />
				A biztosítási szerződésből eredő igények általánosan a biztosítási esemény bekövetkezésétől számított egy év letelte után elévülnek.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>