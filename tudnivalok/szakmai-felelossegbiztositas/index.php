<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'szakmai-felelossegbiztositas';
		include '../../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Szakmai felelősségbiztosításokról röviden, közérthetően <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/tudnivalok/">Vissza >></a></h3>
				
<p><strong>Szakmai felelősségbiztosításokról röviden, közérthetően</strong></p>
<p><strong>Kockázatviselés tárgya:</strong> szakmai szabályszegés elkövetése miatt a biztosítottat terhelő kártérítési kötelezettség. Szerződésszegéssel okozott károkra nyújtanak fedezetet.</p>
<p><strong>Időbeli hatály</strong></p>
<p>A biztosítási védelem a biztosítási szerződés hatálya alatt okozott, bekövetkezett és a biztosító részére bejelentett károkra terjed ki.</p>

			<p style="line-height: 1.6em;"><a href="#1">I. Egészségügyi és szociális tevékenység felelősségbiztosítása</a>
			<br><a href="#2">II. Állatorvosi felelősségbiztosítás</a>
			<br><a href="#3">III/1. Könyvvizsgálói, adótanácsadói és könyvelői felelősségbiztosítás</a>
			<br><a href="#4">III/2. Munkavállalói felelősségbiztosítás</a>
			<br><a href="#5">IV. Építészeti és műszaki tervezés felelősségbiztosítás</a>
			<br><a href="#6">V. Vagyonvédelmi vállalkozások felelősségbiztosítása</a>
			<br><a href="#7">VI. Rendezvényszervezői felelősségbiztosítás</a>
			<br><a href="#8">VII. Szakképzést szervezők felelősségbiztosítása</a>
			<br><a href="#9">VIII. Társasházkezelők, lakásszövetkezetek felelősségbiztosítása</a>
			<br><a href="#10">IX. Hivatalos közbeszerzési tanácsadók felelősségbiztosítása</a>
			<br><a href="#11">X. Szakfordítók, tolmácsok felelősségbiztosítása</a>
			<br><a href="#12">XI. Őnálló bírósági végrehajtók felelősségbiztosítása</a>
			<br><a href="#13">XII. Hivatali felelősségbiztosítás</a>
			<br><a href="#14">XIII. Közjegyzők felelősségbiztosítása</a>
			<br><a href="#15">XIV. Ügyvédek felelősségbiztosítása</a>
			<br><a href="#16">XV. Felszámolók felelősségbiztosítása</a>
			<br><a href="#17">XVI. Szabadalmi ügyvivők felelősségbiztosítása</a>
			<br><a href="#18">XVII. Igazságügyi szakértők felelősségbiztosítása</a>
			</p>
			
			<p><strong><a name="1"></a>I. Egészségügyi és szociális tevékenység felelősségbiztosítása </strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>Egészségügyi szolgáltatást saját nevében nyújtó természetes személynek, jogi személynek, vagy jogi személyiség nélküli gazdasági társaságnak.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>Olyan eseményre, amikor:</p>
			<ul type="disc">
				<li>a kárt maga a biztosított vagy      olyan személy okozta, akinek a magatartásáért a biztosított a magyar jog      szabályai szerint kártérítési felelősséggel tartozik és</li>
				<li>a kárt az egészségügyi      szolgáltatásra vonatkozó foglalkozási/szakmai előírások és szabályok      felróható módon történő megszegésével/megsértésével okozták és</li>
				<li>a kárt az egészségügyi szolgáltatást      igénybe vevő, vagy abban részesülő személy vagyonában, életében, testi      épségében vagy egészségében okozták.</li>
			</ul>
			<p>Ahhoz, hogy a káresemény biztosítási eseménynek minősüljön, mindhárom feltételnek egyidejűleg teljesülnie kell!</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="2"></a>II. Állatorvosi felelősségbiztosítás </strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>állategészségügyi szolgáltatást az előírt (állatorvosi kamarai, hatósági) engedély birtokában, saját nevében folytató természetes személynek jogi személynek, vagy jogi személyiség nélküli gazdasági társaságnak.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>Mentesíti a biztosítottat olyan károk megtérítése alól, melyet a biztosított, a biztosított alkalmazásában vagy vele munkavégzésre irányuló egyéb jog-viszonyban álló állatorvos, állategészségügyi képesítéssel rendelkező személy, állategészségügyi képesítéssel nem rendelkező, de a biztosított állat-egészségügyi szakmai szolgáltató tevékenységében közvetlenül részt vevő személy (pl. istálló-személyzet, állatápoló), a tevékenységére irányadó foglalkozási szabályok megszegése folytán okoz, és amelyekért a biztosított kártérítési felelősséggel tartozik.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="3"></a>III/1. Könyvvizsgálói, adótanácsadói és könyvelői felelősségbiztosítás</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>a.) Könyvvizsgálói tevékenység tekintetében okleveles könyvvizsgálói képesítéssel rendelkező, tevékenységét a Magyar Könyvvizsgálói Kamara tagjaként folytató természetes személy, a Magyar Könyvvizsgálói Kamaránál nyilvántartásba vett könyvvizsgálói társaság.</p>
			<p>b.) Adótanácsadói tevékenység tekintetében adószakértői igazolvánnyal, vagy adótanácsadó szakképesítéssel rendelkező természetes személy, az a gazdasági társaság, melynek vezető tisztségviselője vagy cégjegyzésre jogosult alkalmazottja(i) adószakértői igazolvánnyal vagy adótanácsadó szakképesítéssel rendelkezik, a Magyar Könyvvizsgálói Kamaránál nyilvántartásba vett könyvvizsgálói társaság, amelynek a cégbíróságon bejegyzett tevékenységi körében az adótanácsadói tevékenység is szerepel.</p>
			<p>c.) Könyvvezetési tevékenység tekintetében mérlegképes könyvelő szakképesítéssel rendelkező természetes személy, az a gazdasági társaság, melynek vezető tisztségviselője vagy cégjegyzésre jogosult alkalmazottja(i) mérlegképes könyvelői szakképesítéssel rendelkezik, könyvvizsgáló és azon könyvvizsgálói társaság, amelynek a cégbíróságon bejegyzett tevékenységi körében a könyvvezetés is szerepel.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>A biztosító vállalja, hogy - a szerződésben megállapított mértékben és feltételek szerint - mentesíti a biztosítottat olyan tisztán vagyoni károk megtérítése alól, melyet a biztosított, a biztosítottal munkaviszonyban álló, a biztosított tevékenységet folytató személy a tevékenységére irányadó foglalkozási szabályok megszegése folytán okoz, és amelyért a biztosított kártérítési felelősséggel tartozik.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="4"></a>III/2. Munkavállalói felelősségbiztosítás</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>a.) A könyvvizsgálói tevékenység tekintetében okleveles könyvvizsgálói képesítéssel rendelkező, tevékenységét a Magyar Könyvvizsgálói Kamara tagjaként folytató munkavállaló.</p>
			<p>b.) Az adótanácsadói tevékenység tekintetében adószakértői igazolvánnyal, okleveles adószakértő vagy adótanácsadó szakképesítéssel rendelkező munkavállaló.</p>
			<p>c.) A könyvviteli szolgáltatási tevékenység keretében okleveles könyvvizsgálói vagy mérlegképes könyvelői szakképesítéssel rendelkező munkavállaló.</p>
			<p>d.) Az igazságügyi szakértői tevékenység keretében a névjegyzékben szereplő szakterületén az a munkavállaló, aki az igazságügyi szakértői névjegyzékben szerepel.</p>
			<p>e.) A bérszámfejtés keretében e tevékenységet végző munkavállaló.</p>
			<p>f.) A társadalombiztosítási ügyintézés keretében társadalombiztosítási ügyintéző vagy társadalombiztosítási szakelőadó szakképesítéssel rendelkező munkavállaló.</p>
			<p> </p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>A biztosító mentesíti a biztosítottat, mint munkavállalót olyan károk megtérítése alól, melyeket a munkaviszonyából eredő kötelezettségének vétkes megszegésével, gondatlanul okozott, amely kárért a munkajog szabályai szerint a munkáltatóval szemben kártérítési felelősséggel tartozik.</p>
			<p>A biztosítás kizárólag azokat a munkáltatói kártérítési igényeket fedezi, melyeket a munkáltató a biztosított által okozott tisztán vagyoni kár miatt érvényesít a munkavállalóval szemben.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="5"></a>IV. Építészeti és műszaki tervezés felelősségbiztosítás</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>Egyéni vállalkozó (természetes személy), társas gazdálkodó szervezet, aki az általa végzett, biztosított tevékenység - építészeti-műszaki tervezés, tervezői művezetés, tervellenőri tevékenység - folytatására a hatályos magyar jogszabályok szerint jogosult.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>A biztosított tevékenység ellátása során a biztosított által a szakmai szabályokmegszegésével (szakmai hiba) okozott dologi és személyi sérüléses károk és a káreseménnyel kapcsolatos költségek bekövetkezése.</p>
			<p>A biztosító vállalja, hogy - a szerződésben megállapított mértékben és feltételek szerint - mentesíti a biztosítottat olyan károk megtérítése alól, melyet a biztosított, a biztosítottal munkaviszonyban vagy munkavégzésre irányuló egyéb jogviszonyban álló személy a tevékenységére irányadó foglalkozási szabályok megszegése folytán okoz, és amelyért a biztosított kártérítési felelősséggel tartozik.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="6"></a>V. Vagyonvédelmi vállalkozások felelősségbiztosítása</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>A személy- és vagyonvédelmi, valamint magánnyomozói tevékenységet rendőrhatósági engedély birtokában a Személy-, Vagyonvédelmi és Magánnyomozói Szakmai Kamara tagjaként folytató egyéni és társas vállalkozásoknak.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>A személy és vagyonvédelmi tevékenység során a megbízónak okozott szerződéses, illetve a tevékenységgel összefüggésben harmadik személynek okozott károkra.</p>
			<p>A biztosító vállalja, hogy - a szerződésben megállapított mértékben és feltételek szerint - mentesíti a biztosítottat olyan károk megtérítése alól, melyet - biztosított - a biztosítottal munkaviszonyban vagy munkavégzésre irányuló egyéb jogviszonyban álló, a biztosított tevékenység folytatására jogosult személy a tevékenységére irányadó foglalkozási szabályok megszegése folytán a vagyonvédelmi szolgáltatás jogosultjának (megbízójának, megrendelőjének) okoz, amelyért a biztosított kártérítési felelősséggel tartozik.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="7"></a>VI. Rendezvényszervezői felelősségbiztosítás</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>Rendezvényszervező cégeknek.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>A biztosító megtéríti mindazt a személysérüléses és dologi kárt, melyet a biztosított, mint rendezvény szervezője a rendezvényen résztvevőknek (pl. versenyzők, rendezvényen fellépők, kiállítók), nézőknek és harmadik személyeknek a rendezvény időtartama alatt, annak helyszínén okozott, és amelyért a magyar polgári jog szabályai szerint kártérítési felelősséggel tartozik.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="8"></a>VII. Szakképzést szervezők felelősségbiztosítása</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>Szakképzést szervezőknek. A közoktatásról szóló törvény a szakképzésben részt vevő tanuló javára a gyakorlati képzés idejére felelősségbiztosítási szerződés megkötését írja elő. Amennyiben a szakképzést szervezőnek nincs érvényes és hatályos munkáltatói felelősségbiztosítása, akkor a szakképzést szervezők felelősségbiztosításának megkötése kötelező.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>Személyi sérüléses és dologi (tárgyrongálási) károkra, amelyeket a biztosított az általa szervezett gyakorlati szakképzés során, illetőleg következtében a szakközépiskola, a szakmunkásképző iskola és szakiskola tanulójának a gyakorlati képzés keretében szervezői és/vagy lebonyolítói minőségben okozott.</p>
			<p>A biztosító megtéríti mindazokat a személysérüléses és dologi károkat, amelyért a biztosított, mint gyermekellátási, oktatási, nevelési tevékenység folytatója a magyar jog szerint kártérítési felelősséggel tartozik.</p>
			<p>Fedezetet nyújt a biztosítás a biztosított szakmai szolgáltatásával összefüggésben a tanulót (hallgatót, gyermeket) ért károkból származó kártérítési kötelezettségekre, a biztosítottat, mint belátási képességgel nem vagy korlátozottan rendelkező személy gondozóját terhelő kártérítési kötelezettségekre.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="9"></a>VIII. Társasházkezelők, lakásszövetkezetek felelősségbiztosítása </strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>A társasház fenntartásával, működésével összefüggő gazdasági és műszaki tevékenységet végző gazdasági társaság, vagy természetes személy.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>Megtéríti biztosító a biztosított által nyújtott szolgáltatás teljesítése során, illetve hibás teljesítése következtében a biztosított szerződéses partnereit ért azon személysérüléses és dologi károkat, amelyekért a biztosított a magyar magánjog szabályai szerint kártérítési felelősséggel tartozik.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="10"></a>IX. Hivatalos közbeszerzési tanácsadók felelősségbiztosítása</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>Megbízási jogviszony keretében tevékenykedő hivatalos közbeszerzési tanácsadóknak, illetve csak a közbeszerzési tanácsadó tevékenységre vonatkozóan ügyvédeknek.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>A hivatalos közbeszerzési tanácsadásra irányuló tevékenységből eredő valamely kötelezettség megszegésével a megbízója vagyonában okozott kárra.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="11"></a>X. Szakfordítók, tolmácsok felelősségbiztosítása</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>Szakfordítói tevékenységet (szakfordítás és a nyelvi lektorálás) és tolmácsolási tevékenységet (szinkrontolmácsolás és a konszekutív tolmácsolás) végző gazdasági társaság, vagy természetes személy.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>Biztosítási esemény olyan másnak okozott kár miatti kártérítési kötelezettség bekövetkezése, amelyet a magyar jog szerint a szerződés biztosítottjának kell teljesítenie, és amelynek térítését jelen szerződés alapján a biztosítótól követelheti.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="12"></a>XI. Önálló bírósági végrehajtók felelősségbiztosítása</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>Önálló bírósági végrehajtó, végrehajtójelölt, végrehajtó-helyettes, az önálló bírósági végrehajtó helyettesítésére tartós helyettesként kirendelt végrehajtó-helyettes, aki a hatályos jogszabályok alapján a biztosított tevékenység folytatására jogosult.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>Biztosítási esemény olyan másnak okozott kár miatti kártérítési kötelezettség bekövetkezése, amelyet a magyar jog szerint a szerződés biztosítottjának kell teljesítenie, és amelynek térítését jelen szerződés alapján a biztosítótól követelheti.</p>
			<p>Mentesíti a biztosítottat olyan tisztán vagyoni károk megtérítése alól, melyet a biztosított, a biztosítottal munkaviszonyban álló végrehajtó-helyettes, végrehajtójelölt a tevékenységére irányadó foglalkozási szabályok megszegése folytán okoz, és amelyért a biztosított kártérítési felelősséggel tartozik.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="13"></a>XII. Hivatali felelősségbiztosítás</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>A közhatalom gyakorlására feljogosított hivatalok.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>Biztosítási esemény az olyan kártérítési igény keletkezése, amelyet a szerződés biztosítottjával szemben, a biztosított közhatalom gyakorlására feljogosított munkavállalója e tevékenysége során kifejtett szervező-intézkedő tevékenysége, illetve mulasztása miatt a Polgári Törvénykönyv 349. &sect;-a alapján érvényesítenek.</p>
			<p>Biztosítási eseménynek kizárólag az olyan - államigazgatási jogkörben történő - károkozás minősül, mely rendes jogorvoslattal nem hárítható el, illetve ezen jogorvoslatot a károsult kimerítette.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="14"></a>XIII. Közjegyzők felelősségbiztosítása</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>Közjegyzők és <span>a közjegyző helyettesítésére kirendelt közjegyző helyettes, közjegyző jelölt, nyugalmazott közjegyző.</span></p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>Biztosítási esemény olyan másnak okozott kár miatti kártérítési kötelezettség bekövetkezése, amelyet a magyar jog szerint a szerződés biztosítottjának kell teljesítenie, és amelynek térítését jelen szerződés alapján a biztosítótól követelheti.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="15"></a>XIV. Ügyvédek felelősségbiztosítása</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>Ügyvéd, ügyvédi iroda, aki a hatályos jogszabályok szerint ügyvédi tevékenység folytatására jogosult.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>Biztosítási esemény olyan másnak okozott kár miatti kártérítési kötelezettség bekövetkezése, amelyet a magyar jog szerint a szerződés biztosítottjának kell teljesítenie, és amelynek térítését jelen szerződés alapján a biztosítótól követelheti.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="16"></a>XV. Felszámolók felelősségbiztosítása</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>Gazdálkodó szervezet felszámolására, vagyonkezelésére vagy végelszámolására, a csődeljárásra, felszámolási eljárásra és végelszámolási eljárásra vonatkozó hatályos jogszabályok alapján jogosult gazdasági társaság, vagy természetes személy.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>Biztosítási esemény olyan másnak okozott kár miatti kártérítési kötelezettség bekövetkezése, amelyet a magyar jog szerint a szerződés biztosítottjának kell teljesítenie, és amelynek térítését a biztosítási szerződés alapján a biztosítótól követelheti.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="17"></a>XVI. Szabadalmi ügyvivők felelősségbiztosítása</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>Egyéni szabadalmi ügyvivő, szabadalmi ügyvivői iroda,szabadalmi ügyvivői társaság, aki a hatályos jogszabályok szerint a szabadalmi ügyvivői tevékenység folytatására jogosult.</p>
			<p><strong>Mire vonatkozik a biztosítá?</strong></p>
			<p>Mentesíti a biztosítottat olyan tisztán vagyoni károk megtérítése alól, melyet a biztosított a biztosítottal munkaviszonyban vagy tagsági jogviszonyban álló szabadalmi ügyvivő vagy szabadalmi ügyvivőjelölt a szabadalmi ügyvivői tevékenységre irányadó foglalkozási szabályok megszegése folytán okoz, és amelyért a biztosított a magyar polgári jog szabályai szerint kártérítési felelősséggel tartozik.</p>
			<p><i><a href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<p><strong><a name="18"></a>XVII. Igazságügyi szakértők felelősségbiztosítása</strong></p>
			<p><strong>Kinek ajánljuk?</strong></p>
			<p>a.) Igazsági szakértő természetes személy, aki az igazságügyi szakértői kamara tagja és szerepel az Igazságügyi Minisztérium az igazságügyi szakértői névjegyzékében;</p>
			<p>b.) Gazdasági társaság, melyet az Igazságügyi Minisztérium az igazságügyi szakértői névjegyzékbe felvett;</p>
			<p>c.) Igazságügyi szakértői intézmény.</p>
			<p><strong>Mire vonatkozik a biztosítás?</strong></p>
			<p>Mentesíti a biztosítottat olyan károk (személyi sérüléses, dologi károk és tisztán vagyoni károk) megtérítése alól, melyet a biztosított, a biztosítottal munkaviszonyban vagy munkavégzésre irányuló egyéb jogviszonyban (pl. megbízási, vállalkozási jogviszonyban) álló, a biztosított tevékenységet folytató személy, tevékenységére irányadó foglalkozási szabályok megszegése folytán - hibás szakvélemény készítésével - okoz, és amelyért a biztosított kártérítési felelősséggel tartozik.</p>
			<p><i><a  class="link_bordo" href="elerhetoseg.php">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			</div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>