<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'felelossegbiztositas';
		include '../../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Felelősségbiztosításokról röviden, közérthetően <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/tudnivalok/">Vissza >></a></h3>
				
				<p><strong>Biztosítási esemény:</strong></p>
				<p>A biztosítási esemény olyan, másnak okozott kár miatti kártérítési kötelezettség, amelyet a magyar jog szerint a szerződés biztosítottjának kell teljesítenie, és amelynek a teljesítése alól a biztosítottat a biztosító a szerződésben meghatározottak szerint mentesíti.</p>

				<p><strong>Területi hatály:</strong></p>
				<p>Magyarország területén okozott, bekövetkezett és érvényesített károk &rarr; Területi hatály kiterjesztésére van lehetőség.</p>

				<p><strong>Időbeli hatály:</strong></p>
				<p>A biztosítási védelem a biztosítási szerződés hatálya alatt okozott, bekövetkezett és a biztosító részére bejelentett károkra terjed ki.&rarr; Utófedezetre van lehetőség.</p>

				<p><strong>A biztosító megtérítési igénye - regressz:</strong></p>
				<p>A Ptk. értelmében a biztosítót a károsulttal szemben a biztosított szándékos vagy súlyosan gondatlan magatartása sem mentesíti, de követelheti a biztosítottól a kifizetett biztosítási összeg megtérítését.</p>


				<h4>I. Mi a különbség a vagyon és a felelősségbiztosítás között?</h4>
				<p><strong><i>Vagyonbiztosítás:</i></strong></p>
				<p>- a biztosított saját vagyontárgyai károsodnak,</p>
				<p>- a károsult maga a biztosított,</p>
				<p>- a biztosítási összege a biztosított vagyontárgy értékéhez igazodik (újérték, káridőponti érték, túlbiztosítás, alulbiztosítás)</p>
				<p>- a biztosító mentesülhet, de ha fizet, akkor a kárért felelős személlyel szemben megtérítési jog illeti meg</p>
				<p><strong><i>Felelősségbiztosítás:</i></strong></p>
				<p>- a biztosító biztosított által okozott kárt téríti (a károsult nem a biztosított)</p>
				<p>- a károsult nem áll közvetlen jogviszonyban a biztosítóval</p>
				<p>- a biztosítási összeg a biztosítási szerződésben szereplő kártérítési limit</p>
				<p>- a biztosítót még a biztosított szándékos vagy súlyosan gondatlan magatartása sem mentesíti, csak visszakövetelési jog illeti meg</p>
				<p><strong>A polgárjogi felelősség és felelősségbiztosítás kapcsolata.</strong></p>
				<p>Felelősségbiztosítási szerződés alapján a biztosított követelheti, hogy a biztosító a szerződésben meghatározott mértékben és feltételek szerint mentesítse őt olyan kár megtérítése alól, amelyért jogszabály szerint felelős. (Ptk. 559.&sect;)</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>II. A biztosító kettős szolgáltatást nyújt a biztosítottnak:</h4>
				<p>- megtéríti a más által támasztott megalapozott kártérítési igényeket,</p>
				<p>- elhárítja a más által támasztott megalapozatlan kártérítési igényeket</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>IIII. A felelősségbiztosítás egy 3 pólusú jogviszony.</h4>
				<p>A biztosító jogviszonyban a biztosítottal van a felelősségbiztosítási szerződésen keresztül, de jogi kapcsolatba kerül a károsulttal is, mivel kártérítési kötelezettsége vele szemben van.</p>
				<p>A biztosítottnak, aki egyben a károkozó is, kártérítési kötelezettsége van a károsulttal szemben, de ezt a kötelezettséget (vagy egy részét) - a szerződés keretei között - átvállalja a biztosító.</p>
				<p>A károsult kárigényét a károkozó ellen jogosult érvényesíteni, a biztosító ellen nem fordulhat.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>IV. A biztosító visszakövetelési joga</h4>
				<p>A biztosítót a károsulttal szemben a biztosított (károkozó) szándékos vagy súlyos gondatlan magatartása sem mentesíti, azonban követelheti a biztosítottól a kifizetett biztosítási összeg megtérítését, a biztosítási feltételekben megnevezett esetekben.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>V. Felelősségbiztosítás területei:</h4>
				<p>- polgári jogi felelősség</p>
				<p>- munkajogi felelősség</p>
				<p>- államigazgatási-szabálysértési felelősség</p>
				<p>- büntetőjogi felelősség&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>VI. Polgári jogi felelősség</h4>
				<p>A polgári jogi felelősség a károkozó kötelezettsége arra, hogy a számára felróhatóan másnak okozott kárt megtérítse.</p>
				<p><strong>A.) Következménye:</strong> kártérítési kötelezettség</p>
				<p><strong>B.) Elvei:</strong></p>
				<p>- teljes kártérítés elve</p>
				<p>- káron szerzés tilalma (&bdquo;meggazdagodással&rdquo; nem járhat)</p>
				<p>- a károsult magatartásának értékelése a kárenyhítés és kármegelőzés körében (eleget tett-e ennek?)</p>
				<p><strong>C.) Feltételei: </strong></p>
				<p>Ahhoz, hogy egy kártérítési igény megalapozott legyen, különböző feltételeknek kell együttesen teljesülniük:</p>
				<p><strong><i>1. Kár:</i></strong></p>
				<p>a.) <strong>Vagyoni kár</strong> az, amikor pénzben meghatározható nagyságú kár keletkezik</p>
				<p>&bull; <strong><i>felmerült (tényleges) kár</i></strong> - az az érték, amellyel a károsult vagyona valamely dolog sérülése, &nbsp;elpusztulása, megrongálódása folytán csökkent. Ide tartozik az az értékcsökkenés is, amely a megrongált dolog kijavítása ellenére is fennmarad. (Pl. egy eredetileg sértetlen gépjármű, javítás után már értékcsökkent lesz.)</p>
				<p>&bull; <strong><i>indokolt költségek</i></strong> - azon költségek, melyek a károsultat ért hátrány csökkentéséhez, vagy kiküszöböléséhez szükségesek (kárelhárítás, kárenyhítés, igényérvényesítés költségei)</p>
				<ul type="circle">
					<li>dologi kár esetén: a kár helyreállításával összefüggő szállítási, tervezési, javítási, előkészítő munkák költsége, amennyiben azt a károsult végzi, vagy végezteti.</li>
					<li>élet, testi épség, egészség sérelme esetén: ápolási költség, élelmezésfeljavítás költsége, többletfűtés, átképzési költség, gyógyítási-kezelési, mosatási költség, végtag sérülése esetén helyváltoztatást segítő eszközök beszerzési költsége, hozzátartozók költségei (Pl. látogatás, utazás, temetés, gyászruha stb.)</li>
				</ul>
				<p>&bull; <strong><i>elmaradt vagyoni előny</i> </strong>- az az érték, amellyel a károsult vagyona nyilvánvalóan gyarapodott volna, ha a károsító magatartás be nem következik.</p>
				<p>b.) <strong>Nem vagyoni kár</strong> az, amikor a kár csak jelképesen fejezhető ki pénzben. A károsult személyhez fűződő jogainak megsértése esetén jár, elsősorban személysérüléses károkhoz kötődik. (Pl. kar elvesztése, vakság okozása, szülők elvesztése stb.)</p>
				<p><strong><i>2. Jogellenes magatartás</i></strong></p>
				<p>Minden károkozás jogellenes, kivéve, ha</p>
				<p>- a kárt önvédelemből okozták - &bdquo;jogos védelem&rdquo; (Pl. valaki leüt egy rátámadó garázdát),</p>
				<p>- a kárt szükséghelyzetben okozták, vagyis a károkozással nagyobb kárt akadályoztak meg (Pl. egy égő épület oltása közben a tűzoltók letapossák a szomszéd kertjét. Ilyenkor a károsult nem kártérítést, hanem kártalanítást kap, de nem a károkozótól, hanem a károkozó felelősségbiztosítójától.)</p>
				<p>- a kárt a károsult beleegyezésével okozták (Pl. beteg beleegyezése az orvosi beavatkozásba, sportoló beleegyezése a sportág gyakorlásával együtt járó károsításba).</p>
				<p><strong><i>3. Okozati összefüggés</i></strong></p>
				<p>A károkozó magatartása és a kár bekövetkezése között okozati összefüggésnek kell fennállnia. Ez az okozati kapcsolat nem lehet túl távoli, csak olyan, amit az adott helyzetben előre lehet látni. (Pl. egy könyvelő elcsúszik a jeges járdán és eltöri a lábát. Két hónapig nem tud dolgozni. Ez idő alatt egy ügyfele más könyvelőhöz fordul. Mivel az ügyfél az új könyvelővel elégedett, nála is marad. Itt a kéthónapos jövedelem kiesés megalapozott kártérítési igény. Az ügyfél elvesztése miatti kárigény azonban ezen felül megalapozatlan, mert a jeges járdán való elcsúszás és az ügyfél elvesztése között nagyon távoli az okozati összefüggés.)</p>
				<p><strong><i>4. Felróhatóság</i></strong></p>
				<p>A kártérítési felelősség alóli mentesülés szempontjából különbséget tehetünk az általános (vétkességi) és az objektív (vétkesség nélküli) felelősség között:</p>
				<p>a.) <strong><i>Általános felelősség (vétkességi felelősség) esetén:</i></strong> mentesül a károkozó a kártérítési felelősség alól, ha bizonyítja, hogy úgy járt el, ahogy az az adott helyzetben általában elvárható, vagyis nem &bdquo;vétkes&rdquo;.</p>
				<p>Lényeges: csak annál a személynél állapítható meg a vétkesség, aki belátási képességgel rendelkezik, vagyis előre látja magatartásának következményeit és azt, hogy ezt a társadalom általában hogyan ítéli meg (vétőképesség)! általában ez 10-12 év.</p>
				<p>b.) <strong><i>Objektív (vétkesség nélküli) felelősség esetén:</i></strong> a károkozó nem mentesül a kártérítési felelősség alól annak a bizonyításával, hogy úgy járt el, ahogy az az adott helyzetben általában elvárható!</p>
				<p>- Veszélyes üzemi felelősség: akkor mentesül, ha bizonyítja, hogy a kárt olyan elháríthatatlan ok idézte elő, mely a fokozott veszéllyel járó tevékenység körén kívül esik.</p>
				<p>- Termékfelelősség: a termékgyártó szintén nem mentesül a hibás termék által okozott kár megtérítése alól annak bizonyításával, hogy úgy járt el, ahogy az az adott helyzetben általában elvárható.</p>
				<p><strong>D.) Bizonyítási teher</strong></p>
				<p><strong><i>A károsultnak kell bizonyítania:</i></strong></p>
				<p>- a kárt és annak mértékét</p>
				<p>- az okozati összefüggést</p>
				<p><strong><i>A károkozónak kell bizonyítani:</i></strong></p>
				<p>- hogy magatartása nem volt jogellenes vagy</p>
				<p>- nem volt vétkes, vagyis úgy járt el, ahogy az az adott helyzetben általában elvárható</p>
				<p><strong>E.) Többek általi károkozás</strong></p>
				<p>1.) <strong><i>Közös károkozás:</i></strong> a kárt a közreműködők közös ténykedéssel okozzák, bármelyik károkozó közrehatása nélkül a kár nem következett volna be &rarr; egyetemleges felelősség.</p>
				<p>(Mindegyik károkozó az egész kártérítéssel tartozik, de ha bármelyikük teljesít, a többi kötelezettsége megszűnik.)</p>
				<p>2.) <strong><i>A közreműködők egymástól függetlenül</i></strong>, de mégis egymással okozati kapcsolatban álló magatartással okoznak kárt &rarr; a károkozók önállóan felelnek.</p>
				<p>(Pl. a közlekedési baleset sérültjén végrehajtott orvosi beavatkozás során elkövetett &bdquo;műhiba&rdquo; következtében a károsult állapota még rosszabbá vált.)</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

				<h4>VII. A felelősségbiztosítás főbb kockázati csoportjai</h4>
				<p><strong>1. általános felelősségbiztosítás</strong></p>
				<p><strong>Mire vonatkozik, m</strong><strong>i a biztosítási esemény?</strong></p>
				<p>A biztosítottal szerződéses jogviszonyban nem álló harmadik személynek okozott, illetve a biztosítottal szerződéses kapcsolatban állóknak <strong><i>szerződésen kívül okozott</i></strong> személyi sérüléses vagyoni és nem vagyoni károk, illetve dologi károkozás kapcsán felmerülő vagyoni károk és elmaradt vagyoni előny miatti kártérítési kötelezettség, amelyet a magyar jog szerint a szerződés biztosítottjának kell teljesítenie, és amelynek a teljesítése alól a biztosítottat a biztosító a szerződésben meghatározottak szerint mentesíti.</p>
				<p><strong><i>Szerződésen kívül okozott kárnak csak az minősül</i></strong>, ha</p>
				<p>- a biztosított a biztosított tevékenység folytatása (az általa nyújtott szolgáltatás teljesítése) során, illetve azzal összefüggésben olyan személynek okoz kárt, akivel a biztosítási szerződésben meghatározott tevékenységével kapcsolatban nincs szerződéses jogviszonyban, vagy aki nem igénybevevője a biztosított által nyújtott szolgáltatásnak.</p>
				<p>- a biztosított, a biztosítási szerződésben megjelölt tevékenység folytatójaként ezen vállalkozás épületeinek,</p>
				<p>- ideértve az épület berendezési és épületgépészeti tárgyakat - helyiségeinek, építményeinek, reklámberendezéseinek fenntartója és üzemben tartója minőségben okoz kárt.</p>
				<p>Nem minősülnek szerződésen kívüli kárnak az épület, építmény tulajdonosának, bérbeadójának, bérlőjének, üzemeltetőjének egymásnak okozott kárai.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>
				<p><strong>2. Munkáltatói felelősségbiztosítás</strong></p>
				<p><strong>Mire vonatkozik, m</strong><strong>i a biztosítási esemény?</strong></p>
				<p>A biztosított munkavállalóját ért, munkabaleset miatt a biztosítottat (munkaadót) terhelő kártérítési kötelezettség.</p>
				<p><strong><i>Munkabaleset</i> </strong>az a baleset, amely a munkavállalót a szervezett munkavégzés során vagy azzal összefüggésben éri, annak helyétől és időpontjától és a munkavállaló közrehatásának mértékétől függetlenül. A munkavégzéssel összefüggésben következik be a baleset, ha a munkavállalót a foglalkozás körében végzett munkához kapcsolódó közlekedés, anyagvételezés, anyagmozgatás, tisztálkodás, szervezett üzemi étkeztetés, foglalkozás - egészségügyi szolgáltatás és a munkáltató által nyújtott egyéb szolgáltatás stb. igénybevétele során éri.</p>
				<p>Az erre vonatkozó felelősség <i>munkajogi felelősség, </i>vétkesség nélküli felelősség (Munka Törvénykönyve).</p>
				<p>A munkáltató a munkavállalónak munkaviszonyával összefüggésben okozott kárért vétkességre tekintet nélkül, teljes mértékben felel.</p>
				<p>Mentesül, ha bizonyítja, hogy</p>
				<p>- a kárt működési körén kívül álló elháríthatatlan ok vagy</p>
				<p>- kizárólag a károsult elháríthatatlan magatartása okozta.</p>
				<p>Kármegosztás - 2 együttes feltétel:</p>
				<p>- a munkavállaló maga is okozója a saját kárának, de nem kizárólagosan, csak részben és</p>
				<p>- ebben vétkesség terheli.</p>
				<p><strong><i>üzemi-, de nem munkabaleset:</i></strong> a munkavállalót a munkába menet vagy onnan lakására menet közben éri, kivéve, ha a baleset a munkáltató által üzemben tartott járművel történt.</p>
				<p><strong><i>Munka-, de nem üzemi baleset:</i></strong></p>
				<p>- a sérült ittassága miatti</p>
				<p>- munkahelyi feladatokhoz nem tartozó, engedély nélkül végzett munka,</p>
				<p>- engedély nélküli járműhasználat, munkahelyi rendbontás során bekövetkezett baleset.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>
				<p><strong>3. Szolgáltatói felelősségbiztosítás</strong></p>
				<p><strong>Mire vonatkozik, m</strong><strong>i a biztosítási esemény?</strong></p>
				<p>A biztosított által nyújtott szolgáltatás teljesítése során, illetve hibás teljesítése következtében a biztosított (szolgáltató) szerződéses partnereinek okozott dologi károk miatt a biztosítottal (szolgáltatóval) szemben támasztott kártérítési igények.</p>
				<p>Ezeknek a károknak a döntő része: szerződésszegéssel (hibás teljesítéssel) okozott kár.</p>
				<p>Amennyiben egy szolgáltatás nyújtója hibásan teljesít egy szerződést, a jogosult 2 igényt támaszthat:</p>
				<p>a.) <strong><i>szavatossági igényt </i></strong>- függetlenül attól, hogy a szolgáltatás nyújtója a teljesítés érdekében úgy járt-e el, ahogy az az adott helyzetben elvárható.</p>
				<p>A szavatosság keretében a szolgáltatás nyújtója köteles saját költségén a hibás dolgot kijavítani vagy kicserélni, illetve a munkát újból elvégezni &rarr; <strong><i>ezt nem fedezi a biztosítás</i></strong>.</p>
				<p>b<strong><i>.) szerződésszegéssel okozott egyéb károk (&bdquo;következménykárok&rdquo;) miatt kártérítési igényt</i></strong> - ha a szolgáltatás nyújtója a teljesítés érdekében nem úgy járt el, ahogy az az adott helyzetben általában elvárható &rarr; <strong><i>ezt fedezi a biztosítás.</i></strong></p>
				<p>A magyar jog szerint az általános (vétkességi) felelősség vonatkozik rá, vagyis a szolgáltatás nyújtója felel a szolgáltatás hibás teljesítése következtében okozott károkért, kivéve, ha bizonyítja, hogy úgy járt el, ahogy az az adott helyzetben általában elvárható.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>
				<p><strong>4. Termékfelelősség biztosítás</strong></p>
				<p><strong>Mire vonatkozik, m</strong><strong>i a biztosítási esemény?</strong></p>
				<p>A biztosított által gyártott hibás termék által okozott személysérüléses károk és dologi károk miatt a biztosítottal szemben támasztott kártérítési igények.</p>
				<p><strong><i>Kár:</i></strong></p>
				<p>- valakinek a személysérülése folytán bekövetkezett vagyoni és nem vagyoni kár</p>
				<p>- hibás termék által más dologban okozott 10 000 Ft-nál nagyobb összegű kár, ha az a más dolog szokásos rendeltetése szerint magánhasználat vagy magánfogyasztás tárgya és a károsult is erre használta.</p>
				<p>Ha a sérült dolog közhasználat tárgya, akkor a gyártó az általános szabályok szerint felel.</p>
				<p><strong><i>Külön feltétellel fedezetbe vonható kockázatok:</i></strong></p>
				<p>- azokat a károkat, amelyek a hibás terméknek más termékkel harmadik személy (felhasználó) által történő egyesítése, összevegyítése, e termékbe történő beépítése, feldolgozása folytán magában az új termékben keletkeznek vagy a felhasználónak egyéb kárt okoznak,</p>
				<p>- a termék harmadik személy (felhasználó) által történő tovább feldolgozása folytán az új termékben és a felhasználónál keletkező egyéb károkat,</p>
				<p>- azokat a károkat, melyek abból erednek, hogy a biztosított által gyártott - termék gyártására szolgáló - munkagép, a gép hibája miatt hibás terméket állít elő vagy a munkagéppel a terméket előállítani nem lehet és ez a hibás terméket előállító felhasználónak kárt okoz.</p>
				<p>Az ide vonatkozó termékfelelősségről szóló törvény (1993: X. tv.) - objektív (vétkesség nélküli) felelősség. A termék gyártója felel a termék hibája által okozott kárért. (10 évig terheli ez a szigorú felelősség a gyártót, utána az általános szabályok szerint felel.)</p>
				<p><strong><i>Kimentési lehetőségek:</i></strong> bizonyítania kell, hogy a terméket nem hozta forgalomba, a hiba oka később keletkezett,jogszabály vagy hatósági előírás okozta a hibát.</p>
				<p><strong><i>Termék:</i></strong> minden ingó dolog, kivéve a mezőgazdaság feldolgozatlan termékei és a villamos energia.</p>
				<p><strong><i>Gyártó:</i></strong> vég-, illetve résztermék, alapanyag gyártója, illetve, aki a terméken elhelyezett nevével magát gyártóként tünteti fel. Importált termék esetén az importáló. Ha a gyártó nem állapítható meg, a forgalmazót kell gyártónak tekinteni mindaddig, amíg a forgalmazó a gyártót meg nem nevezi - erre 30 napja van.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>
				<p><strong>5. Környezetszennyezési felelősségbiztosítás</strong></p>
				<p><strong>Mire vonatkozik, m</strong><strong>i a biztosítási esemény?</strong></p>
				<p>Környezetet veszélyeztető tevékenységgel okozott kár miatt a biztosítottat terhelő kártérítési kötelezettség, ha akár bekövetkezése</p>
				<p>- előre láthatatlan volt</p>
				<p>- hirtelen és váratlan volt</p>
				<p>- a normális üzemi folyamattól eltérő eseményre volt visszavezethető.</p>
				<p>Aki a környezet igénybevételével, illetőleg terhelésével járó tevékenységet (környezetveszélyeztető tevékenység) folytat, köteles az ebből eredő kárt megtéríteni.</p>
				<p>A felelősség alól csak akkor mentesül, ha bizonyítja, hogy a kár</p>
				<p>- elháríthatatlan volt és</p>
				<p>- a tevékenységén kívül esik.</p>
				<p>Az erre vonatkozó környezet védelmének általános szabályairól szóló törvény (1995. évi LIII. tv. 103. &sect;.) - veszélyes üzemi felelősség - objektív (vétkesség nélküli) felelősség.</p>
				<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			</div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>