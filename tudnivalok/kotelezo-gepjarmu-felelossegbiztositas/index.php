<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'kotelezo-gepjarmu-felelossegbiztositas';
		include '../../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Kötelező gépjármű felelősségbiztosítás - biztosító váltás <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/tudnivalok/">Vissza >></a></h3>
				
				<h4>Azok a gépjármű üzembentartók, akik 2010.01.01. <u>előtt</u> kötötték kötelező gépjármű felelősségbiztosítási szerződésüket, 2011.december 01.-ig mondhatják azt fel, mivel a szerződésük évfordulója a kötés napjától függetlenül december 31.</h4>
				<p>Azok a gépjármű üzembentartók, akik még 2010.01.01. előtt kötötték a biztosításukat, ameddig nem történik üzembentartó változás, vagy a gépjárművet el nem adják, és másikat nem vásárolnak addig december 31 a szerződésük évfordulója.</p>
				<p>A felmondásnak a kötelező adattartalommal (biztosító neve, címe, kötvényszám, rendszám, évfordulóval való felmondás - 2011.12.31.) 30 nappal évforduló előtt, azaz december 01.-ig az adott biztosítóhoz be kell érkeznie. Az új szerződést pedig legkésőbb december 31.-ig meg kell kötni. Későbbi kötés esetén az eltelt napokra a biztosító egységes szabály szerinti fedezetlenségi napidíjat számol fel. A fedezetlenségi időszak esetén számolnia kell a tarifális díjnál nagyobb, többszörös fedezetlenségi napidíjjal, valamint az esetleges károkozás anyagi és jogi következményeivel.</p>
				
				<h4>Azok a gépjármű üzembentartók, akik 2010.01.01. <u>után</u> kötötték kötelező gépjármű felelősségbiztosítási szerződésüket az évfordulójuk minden évben a kötés napja lesz, így a szerződést a kötés napja előtti 30. napig lehet felmondani.</h4>
				<p>Azok a gépjármű üzembentartók, akik 2010.01.02.-tól kötötték a biztosításukat a szerződés évfordulója a kockázatviselés kezdete - a kötés napja - lesz.)</p>
				<p>A felmondásnak a kötelező adattartalommal (biztosító neve, címe, kötvényszám, rendszám, évfordulóval való felmondás - év + a kötés napja) 30 nappal évforduló előtt, azaz az év közbeni kötés napja előtti 30. napig az adott biztosítóhoz be kell érkeznie. Az új szerződést legkésőbb az évforduló napjáig meg kell kötni. Későbbi kötés esetén az eltelt napokra a biztosító egységes szabály szerinti fedezetlenségi napidíjat számol fel. A fedezetlenségi időszak esetén számolnia kell a tarifális díjnál nagyobb, többszörös fedezetlenségi napidíjjal, valamint az esetleges károkozás anyagi és jogi következményeivel.</p>

				<h4>Az eddigi hengerűrtartalom helyett teljesítmény alapú besorolást vezettek bea 2011.01.01.-től a kötelező felelősségbiztosításban.</h4>
				<p>Személygépkocsik és motorkerékpárok esetében 2011.01.01.-től a hengerűrtartalom helyett a teljesítmény a meghatározó díjszámítási alap.</p>
				<p>Az új díjak kötelező megjelentetési időpontja minden évben október 30. Az érvényessége a következő naptári év, azaz 01.01.-12.31.</p>

				<h4>Tehergépjárműveknél és pótkocsiknál is változott 2011.01.01.-től a díjszámítás.</h4>
				<p>Tehergépjárműveknél eddig teherbírás volt a díjszámítási alap, ami 2011.01.01.-től össztömeg szerinti díjszámításra változott.</p>
				<p>Pótkocsik, félpótkocsik estében eddig is az össztömeg volt a díjszámítás alapja. Ez 2011.01.01.-.től 3 kategória szerint lett kialakítva és pontosítva.</p>

				<h4>Az adatbevitel során kérjük, hogy figyelmesen olvassa el a kérdéseket, mert a pontatlan adatközlés befolyásolhatja a díjszámítás eredményét. Minden kérdésre válaszoljon. A KGFB díja egyre inkább függ attól, hogy a különböző kedvezmények közül jogosult-e valamelyikre.</h4>
				<p>A kapcsolattartó kiválasztásával kiválaszthatja azt a személyt, aki segíthet a szerződéskötési folyamat lebonyolításában és a későbbiekben problémáival, ügyintézésekkel hozzá fordulhat.</p>

				<h4>A program által megjelenített díjajánlatok már az igénybe vehető, kedvezményekkel csökkentett díjakat tartalmazzák.</h4>
				<p>Amennyiben talál, az igényeinek megfelelő ajánlatot, akkor megkötheti biztosítását on-line, vagy kereshet bennünket elérhetőségeinken. A szerződés megkötését a választott biztosító neve mellett található <strong>MEGKÖTÖM</strong> gombra kattintással tudja kezdeményezni.</p>
				<p>A szerződéskötés regisztrációhoz kötött. A regisztrált felhasználók az e-mail címük és jelszavuk megadásával folytathatják a szerződés elkészítését. A rendszer a megadott e-mail címre visszaigazolást küld.</p>
				<p>A visszaigazolás tartalmaz egy alkuszi megbízást, ami feljogosít minket a szerződés véglegesítésére, aláírására és továbbítására. Az aláírt megbízást szíveskedjen, személyesen, postán, elektronikus levélben, vagy faxon elérhetőségünkre eljuttatni. Meglévő szerződésének felmondását is elvégezzük, amennyiben évforduló előtt 40 nappal eljuttatja hozzánk.</p>
				<p>Bármilyen probléma felmerülése esetén kérjen tőlünk segítséget elérhetőségeinken, vagy</p>

            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>