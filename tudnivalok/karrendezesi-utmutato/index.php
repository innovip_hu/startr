<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'karrendezesi-utmutato';
		include '../../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Kárrendezési útmutató <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/tudnivalok/">Vissza >></a></h3>
				
			 <h4>I. A kár bejelentése a biztosítónál</h4>
			<p>A kár bejelentését megteheti személyesen a biztosító ügyfélszolgálatán, írásban postai úton, telefonon, a biztosító honlapján és természetesen mi is segítünk a kárral kapcsolatos minden ügyintézésben, így a bejelentésben is.</p>
			<p>A kárbejelentéshez a következő adatok szükségesek:</p>
			<p> - A biztosítási szerződés száma (kötvényszám),</p>
			<p> - Kárbejelentő / biztosított neve, címe, telefonszáma,</p>
			<p> - A káresemény helye, időpontja, rövid leírása (a kár keletkezésének oka, körülményei, károsodott vagyontárgyak körének megnevezése - itt még nem szükséges a tételes felsorolás),</p>
			<p> - A kár becsült összege,</p>
			<p> - A hatóság részéről történt intézkedés megnevezése, ha volt ilyen (pl.: tűzoltóság, rendőrség)</p>
			<p>A kárbejelentés után a biztosító munkatársa ellenőrzi a biztosítási szerződés érvényességét és azt, hogy a káresemény a biztosítási feltételek alapján téríthető-e. Ha a kár rendezéséhez helyszíni szemle szükséges, akkor kárszakértő a szemle időpontjának egyeztetése érdekében telefonon vagy írásban felveszi Önnel a kapcsolatot.</p>
			<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<h4>II. A kárügyintézésre vonatkozó fontosabb határidők</h4>
			<p>A kárigényt a kár észlelésétől számított 2 munkanapon belül kell bejelenteni. (Halál és baleseti kár esetén, 8 munkanapon belül.)</p>
			<p>A kárbejelentést követően a kárszakértőnek a biztosítási feltételnek megfelelően általában 5 munkanapon belül jelentkeznie kell a helyszíni szemle időpontjának egyeztetése érdekében.</p>
			<p>A kár bejelentésétől számított 5. munkanapig hagyja a kár helyszínét változatlanul, a károsodott vagyontárgy állapotán ne változtasson, vagy csak a kárenyhítéshez szükséges mértékben tegye azt. Amennyiben a biztosító 5 munkanapon belül nem végzi el a helyszíni szemlét, és erről nem értesítette, akkor Ön intézkedhet a károsodott vagyontárgy helyreállításáról. A károsodott, kiselejtezett, kiszerelt alkatrészeket, berendezéseket őrizze meg a biztosító jelentkezéséig változatlan állapotban. A helyreállításról kérjen részletes árajánlatot, vagy részletes számlát. Készítsen a helyszínről dátumos fotókat a helyreállítás előtt.</p>
			<p>A minél gyorsabb kárrendezés érdekében a betöréses lopás- és rablás károkat a rendőrségre, tűzzel kapcsolatos károkat a tűzoltóságra a lehető legrövidebb időn belül be kell jelenteni. Mindez különösen azért fontos, mert a rendőrséget és a tűzoltóságot is érintő kárügyekben a kárrendezéshez a biztosítónak szüksége van a rendőrség és tűzoltóság által kiállított dokumentumokra, jegyzőkönyvekre is.</p>
			<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<h4>III. A kárrendezéshez általában szükséges dokumentumok</h4>
			<p>A biztosítási esemény, a szolgáltatásra való jogosultság és a szolgáltatás összegének megállapításához a káresettől függően a biztosítónak különböző iratokra és bizonylatokra van szüksége. Ezek mielőbbi átadásával gyorsítható a kárrendezés menete.</p>
			<p><b>1.) Tűz-, robbanás és nagyobb elemi károk</b></p>
			<p> - A károsodott ingóságok azonosítására alkalmas tételes lista,</p>
			<p> - Az építésügyi hatóság által kiadott bontási határozat (ha bontani kell),</p>
			<p> - Régebbi engedélyezési tervek a lakásról, családi házról,</p>
			<p> - Épület helyreállítási árajánlatok, számlák, alaprajzok, fotók.</p>
			<p> - Tűzhatósági bizonyítvány</p>
			<p><b> </b></p>
			<p><b>2.) Közvetlen villámcsapás</b></p>
			<p> - A károsodott ingóságok azonosítására alkalmas tételes lista,</p>
			<p> - épület helyreállítási árajánlatok, számlák.</p>
			<p><b>3.) Villámcsapás indukciós hatása</b></p>
			<p> - A károsodott ingóságok azonosítására alkalmas tételes lista,</p>
			<p> - Szakértői vélemény a károsodott készülékekről,</p>
			<p> - Tételes javítási árajánlat a készülékekről, tételes számlák.</p>
			<p><b>4.) Csőtörés- és beázás károk</b></p>
			<p> - A károsodott ingóságok azonosítására alkalmas tételes lista,</p>
			<p> - Tételes javítási árajánlat a készülékekről, tételes számlák.</p>
			<p> - (A csőtörés helye, oka feltárható de a szemléig nem állítsa véglegesen helyre, ne burkolja vissza.)</p>
			<p><b>5.) Betöréses lopás, rablás</b></p>
			<p> - Az eltulajdonított ingóságok azonosítására alkalmas tételes lista,</p>
			<p> - Rendőrségi feljelentés, határozat,</p>
			<p> - Számlák, garanciajegyek, adásvételi szerződés, fotók.</p>
			<p><b>6.) Baleset</b></p>
			<p> - Orvosi szakvélemény,</p>
			<p> - Halotti anyakönyvi kivonat (baleseti halál esetén),</p>
			<p> - Kórházi zárójelentés (kórházi kezelés esetén),</p>
			<p> - Táppénzes igazolás másolata,</p>
			<p> - Öröklési bizonyítvány, hagyatékátadó végzés (baleseti halál esetén).</p>
			<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<h4>IV. A biztosító által alkalmazott kárrendezési eljárások</h4>
			<p><b>1.) A Biztosító az alábbi esetekben eltekinthet a helyszíni szemlétől lakásbiztosításoknál &ndash; számla alapján térít.</b></p>
			<p>- Kisebb összegű üvegkár</p>
			<p>- Zárcsere költségtérítése</p>
			<p>- Bankkártya-letiltás</p>
			<p><b>2.) A helyszíni szemle menete </b></p>
			<p>A biztosító munkatársa, kárszakértője, vagy szerződött kárszakértője előzetesen telefonon egyeztetik Önnel a helyszíni szemle időpontját, illetve telefonos elérési lehetőség hiányában, írásban küldenek értesítést.</p>
			<p>A szemle során kárszakértő megtekinti a helyszint, fotókat készít a károsodott helyszínről és a károsodott vagyontárgyakról. Az általa írt jegyzőkönyvben tételesen rögzíti az Önnel egyeztetetett épületben és ingóságokban keletkezett károkat és ha ez azonnal megállapítható kiszámolja a kártérítési összeget. (Ha a kár a szerződési feltételek szerint biztosítási eseménynek minősül.)</p>
			<p>A helyszínen készült jegyzőkönyvből és kárszámításból kap egy másolatot.</p>
			<p><b>3.) Telefonos kárrendezés</b></p>
			<p>A kényelmesebb és gyorsabb ügyintézés érdekében a biztosító telefonos kárrendezési lehetőséget is felajánlhat a kisebb összegű károknál (általában naptári évenként egy alkalommal). Ilyen károk lehetnek:</p>
			<p> - Felhőszakadás, jégverés, vihar és villámcsapás,</p>
			<p> - Csőtörés és beázás</p>
			<p> - Villámcsapás indukciós hatása miatt bekövetkező károk.</p>
			<p>Amennyiben Ön nem kíván élni ezzel a lehetőséggel, vagy nem tudnak a kárszakértővel megegyezni, akkor a helyszíni szemlétől nem lehet eltekinteni.</p>
			<p>A telefonon rendezett károk esetén később helyszíni szemlére is sor kerülhet, ahol a szakértő ellenőrzi a telefonos kárrendezés során bediktált kárt.</p>
			<p><b>4.) Assistance szolgáltatás igénybevétele, ha a szerződés tartalmazza. (Társasház és lakásbiztosításoknál)</b></p>
			<p>Az év minden napján 0&ndash;24 óráig fogadja az Ön telefonhívását és a lehetőségek szerint minél hamarabb információt nyújt, iparos munkát szervez és átvállalja ennek költségeit.</p>
			<p><b>a.) Vészelhárítási szolgáltatás</b></p>
			<p>A biztosított épület gépészeti, műszaki berendezéseinek meghibásodása vagy egy váratlan külső mechanikai behatás következtében olyan helyzet alakul ki, mely sürgős beavatkozást kíván a további károk és balesetveszély megelőzése érdekében.</p>
			<p>A vészhelyzet elbírálása a biztosítóval szerződéses kapcsolatban lévő assistance szolgáltató koordinátorának jogosultsága az Ön helyzetismertetése alapján.</p>
			<p>Vészelhárítás esetén, ha a felmerülő költségek meghaladják a biztosítási ajánlaton megjelölt összeget, a pluszköltség minden esetben a biztosítottat terheli.</p>
			<p><b>b.) Szakiparosok ajánlása</b></p>
			<p>Vészhelyzetnek nem minősülő, valamint a biztosítási szerződés szolgáltatásain túli szakipari munkák elvégzéséhez szintén ajánl az assistance szolgáltató a fenti ágazatokhoz tartozó, előre meghatározott kiszállási és munkadíjjal dolgozó iparost, de a munka elvégzésének összes költsége (kiszállás, munkadíj, anyagköltség) önt terheli.</p>
			<p><b>c.) Információs szolgáltatás</b></p>
			<p>A biztosító feltételében leírtak szerint információt nyújthat egyéb utazáshoz, szabadidőhöz és egyéb életvitelhez kötődő dolgokhoz. (Pl. szállodacímek, színház, mozi, áruházak nyitva tartása, rovar és rágcsálóirtás, költöztetés stb.)</p>
			<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

			<h4>V. A kárigény elévülése</h4>
			<p>Miután a biztosító a felmérések és a kárszámítás alapján kölcsönösen elfogadott kártérítési összeget kifizeti, a kárügy lezárásra kerül. Amennyiben a kárügyének lezárását követően esetlegesen további kárigénye merülne fel, arra az esetre az alábbiak szerint nyílik lehetőség:</p>
			<p> - Vagyon- és balesetbiztosítási szerződéséből eredő igényei a biztosítási esemény bekövetkezésétől számított 2 év alatt,</p>
			<p> - Felelősségbiztosítási szerződéséből eredő igények a kárnak, ill. a kárigénynek Önnel való közlésétől számított 1 év alatt évülnek el.</p>
			<p><i><a class="link2" href="<?php print $domain;  ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></i></p>

            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>