<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'szolgaltatasok';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">SZOLGÁLTATÁSAINK</h3>
                	<div class="row">
						<div class="grid_6">
							<p>Megbízóink érdekeit szem előtt tartva a sokszínű biztosítási termékkínálatból segítünk kiválasztani a legmegfelelőbb megoldást. Szolgáltatásunk kiterjed egy adott biztosítási szerződés „életútja” folyamán felmerülő összes lehetséges eseménnyel kapcsolatos szakmai támogatásra. Szaktudásunkkal, teljes körű ügyintézéssel szolgáljuk az Ön kényelmét.</p>

							<p>
							<ul class="felsorolas">
								<li>Kockázatok felmérése</li>
								<li>Igényfelmérés</li>
								<li>Biztosítói ajánlatok összehasonlítása és javaslattétel</li>
								<li>Szerződéskötés - Szerződéskezelés</li>
								<li>Segítség a kárrendezésben</li>
							</ul>
							</p>						
						</div>
						<div class="grid_6">
							<img src="<?=$domain?>/images/business-camera-coffee-1509428.jpg" alt="SZOLGÁLTATÁSAINK">
						</div>
					</div>
            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>