<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'egyuttmukodo-partnereink';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
	<script>
		 $(window).on("load", function() {
			$('.sor').each(function(){  
				var highestBox = 0;
				$('.box_kep', this).each(function(){

					if($(this).height() > highestBox) 
					   highestBox = $(this).height(); 
				});  
				$('.box_kep',this).height(highestBox);
			});  
			$('.sor').each(function(){  
				var highestBox = 0;
				$('.logo_box', this).each(function(){

					if($(this).height() > highestBox) 
					   highestBox = $(this).height(); 
				});  
				$('.logo_box',this).height(highestBox);
			});  
		});
	</script>
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container">
                <h3 class="border">Együttműködő partnereink</h3>
				
					<div class="row sor">
						<div class="grid_2 wow zoomIn">
							<a href="http://www.bazisnet.hu/" target="_blank">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/bazisnet.jpg"></div>
								<div class="box1_cnt">
									<p>BázisNet Rendszerház Kft.</p>
								</div>
							</div>
							</a>
						</div>
						<div class="grid_2 wow zoomIn">
							<a href="http://www.coopszeged.hu/" target="_blank">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/coop.jpg"></div>
								<div class="box1_cnt">
									<p>COOP Szeged Zrt.</p>
								</div>
							</div>
							</a>
						</div>
						<div class="grid_2 wow zoomIn">
							<a href="http://www.hok-plastic.hu/" target="_blank">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/Hok-Plastic.png"></div>
								<div class="box1_cnt">
									<p>Hok-Plastic Kft.</p>
								</div>
							</div>
							</a>
						</div>
						<div class="grid_2 wow zoomIn">
							<a href="http://www.ocsipc.hu/" target="_blank">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/ocsipc.jpg"></div>
								<div class="box1_cnt">
									<p>Ocsi-Pc Bt.</p>
								</div>
							</div>
							</a>
						</div>
						<div class="grid_2 wow zoomIn">
							<a href="http://www.innovip.hu/" target="_blank">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/innovip.png"></div>
								<div class="box1_cnt">
									<p>Innovip.hu Kft.</p>
								</div>
							</div>
							</a>
						</div>
					</div>

            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>