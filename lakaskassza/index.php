<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'lakaskassza';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Lakáskassza <a class="link_bordo vissza_h3" href="<?php print $domain; ?>/biztositasi-termekek/">Vissza >></a></h3>
				
					<p>Lakás-takarékpénztár (LTP) olyan szakosított hitelintézet, mely kizárólagosan lakáscélú betétek gyűjtésével és lakáscélú hitelek nyújtásával foglalkozik. </p>

					<p>A lakáscélú megtakarítások felhasználhatósága nagyon széleskörű, pl.: építés, vásárlás, felújítás, korszerűsítés, festés, beépített bútorok, beépített műszaki cikkek vásárlása, stb.</p>

					<p>2018. december 1-jétől elérhető a Fundamenta új <strong>Gondoskodó Lakásszámla</strong> terméke.</p>

					<p>
					<strong>Egyedi jellemzők:</strong>
					<ul class="felsorolas">
						<li>Magánszemélyeknek, Társasházaknak és Lakásszövetkezeteknek is elérhető</li>
						<li>személyre szabható: választható futamidő és havi megtakarítási összeg</li>
						<li>Betéti kamat: 0,1 %</li>
						<li>egyszeri éves 5% kamatbónusz</li>
						<li>Számlanyitási díj: 1%</li>
						<li>A megtakarítási idő lejártakor fix és alacsony, 2,9% (THM: 4,93%)2 hitelkamatú lakáskölcsönnel egészíthető ki.</li>
						<li>Hitel kezelési költség: 0%</li>
						<li>Továbbra is lakáscélú a termék, mind a betéti részt, mind a hitel részt tekintve!</li>
					</ul>
					</p>

					<p><a class="link_bordo" href="<?php print $domain; ?>/kapcsolat/">A tájékoztatás nem teljes körű, további információkért keresse irodánkat!</a></p>


            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>