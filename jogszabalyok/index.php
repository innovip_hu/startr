<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'jogszabalyok';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container">
                <h3 class="border">Jogszabályok</h3>
				
				<p>190/2004. Korm. rendelet a gépjármű üzembentartójának kötelező felelősségbiztosításáról (hatályon kívül 2010.01.01.-től)</p>
				<p><a class="link_bordo" href="http://www.mabisz.hu/images/stories/docs/jogszabalyok/190-2004kormrend.pdf" target="_blank">http://www.mabisz.hu/images/stories/docs/jogszabalyok/190-2004kormrend.pdf</a></p>
				<p>2009. évi LXII. Törvény a kötelező gépjármű felelősségbiztosításról  (hatályos 2010.01.01.-től)</p>
				<p><a class="link_bordo" href="http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=A0900062.TV" target="_blank">http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=A0900062.TV</a></p>
				<p>19/2009. (X.9.) PM rendelet a BM igazolás és a kártörténeti igazolás kiadásának szabályairól (hatályon kívül 2011.06.15.-től)</p>
				<p><a class="link_bordo" href="https://www.mabiasz.hu/downloads/57" target="_blank">https://www.mabiasz.hu/downloads/57</a></p>
				<p>21/2011. (VI.10.) NGM rendelet a BM rendszer és a kártörténeti igazolás kiadásának szabályairól (hatályos 2011.06.15.-től)</p>
				<p><a class="link_bordo" href="http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=A1100021.NGM" target="_blank">http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=A1100021.NGM</a></p>
				<p>20/2009. (X.9.) PM rendelet a kötelező gépjármű felelősségbiztosításnál alkalmazott kategóriákról (hatályos 2010.01.01.-től)</p>
				<p><a class="link_bordo" href="http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=A0900020.PM" target="_blank">http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=A0900020.PM</a></p>
				<p>1959 évi IV. Törvény a Magyar Köztársaság Polgári Törvénykönyvéről XLV. Fejezet - A biztosítás (Hatályon kívül 2014.03.15.-től)</p>
				<p><a class="link_bordo" href="http://docplayer.hu/929544-1959-evi-iv-torveny-a-magyar-koztarsasag-polgari-torvenykonyverol.html" target="_blank">http://docplayer.hu/929544-1959-evi-iv-torveny-a-magyar-koztarsasag-polgari-torvenykonyverol.html</a></p>
				<p>2013. évi V. törvény a Polgári Törvénykönyvről (Hatályos 2014.03.15.-től)</p>
				<p><a class="link_bordo" href="http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=A1300005.TV" target="_blank">http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=A1300005.TV</a></p>
				<p>2003 évi LX. Törvény a biztosítókról és a biztosítási tevékenységről (hatályon kívül 2016.01.01.-től)</p>
				<p><a class="link_bordo" href="http://mkogy.jogtar.hu/?page=show&docid=a0300060.TV" target="_blank">http://mkogy.jogtar.hu/?page=show&docid=a0300060.TV</a></p>
				<p>2014. évi LXXXVIII. Törvény a biztosítási tevékenységről (440.§ a hatálybalépésről)</p>
				<p><a class="link_bordo" href="http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=A1400088.TV" target="_blank">http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=A1400088.TV</a></p>
				<p>2001 évi CVIII. Törvény az elektronikus kereskedelmi szolgáltatásokról, valamint az információs társadalommal összefüggő szolgáltatások egyes kérdéseiről</p>
				<p><a class="link_bordo" href="http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=a0100108.tv" target="_blank">http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=a0100108.tv</a></p>
				<p>2005 évi XXV. Törvény a távértékesítés kerestében kötött pénzügyi ágazati szolgáltatási szerződésekről (Hatályos 2015.07.01.-től)</p>
				<p><a class="link_bordo" href="http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=A0500025.TV" target="_blank">http://net.jogtar.hu/jr/gen/hjegy_doc.cgi?docid=A0500025.TV</a></p>
				
				<p>2015. évi CXLVII. Törvény:
					<ul class="felsorolas">
						<li>A biztosítókról és a biztosítási tevékenységről szóló 2003. évi LX. Törvény módosítása (hatályos: 2016.01.01.-től)</li>
						<li>A kötelező gépjármű-felelősségbiztosításról szóló 2009. évi LXII. Törvény módosítása (hatályos: 2016.01.01.-től.)</li>
						<li>A biztosítási tevékenységről szóló 2014. évi LXXXVIII. Törvény módosítása (hatályos: 2016.01.02.-től.)</li>
					</ul>
				</p>
				<p><a class="link_bordo" href="https://www.mnb.hu/felugyelet/szabalyozas/jogszabalyok/egyeb-magyar-jogszabalyok/torvenyek" target="_blank">https://www.mnb.hu/felugyelet/szabalyozas/jogszabalyok/egyeb-magyar-jogszabalyok/torvenyek</a></p>
				
				<p>2015. évi CLXII. Törvény:
					<ul class="felsorolas">
						<li>A biztosítókról és biztosítási tevékenységről szóló 2003. évi LX. törvény módosítása (hatálybalépések a törvényben)</li>
						<li>A biztosítási tevékenységről szóló 2014. évi LXXXVIII. Törvény módosítása (hatálybalépések a törvényben)</li>
					</ul>
				</p>
				<p><a class="link_bordo" href="https://www.mnb.hu/felugyelet/szabalyozas/jogszabalyok/egyeb-magyar-jogszabalyok/torvenyek" target="_blank">https://www.mnb.hu/felugyelet/szabalyozas/jogszabalyok/egyeb-magyar-jogszabalyok/torvenyek</a></p>


            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>