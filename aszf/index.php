<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'aszf';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container">
                <h3 class="border">Felhasználási feltételek</h3>
				
					<div class="row">
						<div class="grid_12">

							<p><strong>Kérjük, figyelmesen olvassa el az alábbiakat!</strong></p>

							<p><br />
							<strong>Általunk közvetített Biztosítási termékek listája&nbsp;</strong><u><strong><a href="kozvetitett-biztositasi-termekek-listaja.pdf" target="_blank">elérhető itt.</a></strong></u></p>

							<ul>
								<li>
								<p><strong>Ezen dokumentumot kérésére térítés mentesen nyomtatott formában is rendelkezésére bocsájtjuk.</strong></p>
								</li>
							</ul>

							<p><br />
							Adatkezelési tájékoztatónk elérhető&nbsp;<u><a href="Adatkezelesi_tajekoztato.pdf" target="_blank">itt</a></u>.</p>

							<p>Adatkezelési szabályzatunk elérhető&nbsp;<u><a href="Adatkezelesiszabalyzat201907.pdf" target="_blank">itt</a></u>.</p>

							<p>SFDR (fenntarthatósági) tájékoztatónk elérhető&nbsp;<u><a href="SFDRtjkoztat202106.pdf" target="_blank">itt.</a></u></p>

							<p>Alkuszi megbízási&nbsp;<u><a href="TeljeskoruMegbizas202112honlapra.pdf" target="_blank">minta</a></u>.</p>

							<p>Távértékesítési tájékoztatónk elérhető&nbsp;<u><a href="tvrtkestsrevonatkozszablyok2022KB.pdf" target="_blank">itt.</a></u></p>

							<p>Honlapunk megnyitásával Ön elfogadja az itt felsorolt feltételeket. Kérjük, amennyiben nem ért egyet az alábbiakkal, ne nyissa meg weboldalainkat!</p>

							<p>Online tarifálás, ajánlattétel, szerződéskötés esetén a helytelen, vagy téves adatmegadásból adódó károkért, veszteségekért, költségekért, felelősséget nem vállalunk. Nyomtatáskor kérjük ellenőrizze az adatok pontosságát. Eltérés esetén azonnal jelezze a problémát, hogy még időben segíthessünk a hiba kijavításában.</p>

							<p>Felhasználóink hozzászólásaikban, blog bejegyzéseikben, fórum témáikban, vagy más, nyilvánosan beküldött információikban nem tehetnek közzé szerzői joggal védett információkat, mások személyes és különleges adatait, reklámokat, kéretlen üzeneteket (spam), másokat szándékosan sértő, ellenséges, bántó, uszító, vagy gyűlöletet szító, káromló, obszcén, vagy illetlen kifejezéseket tartalmazó, politikai, vallási, vagy egyéb, ide nem tartozó, vitát keltő (flame) írásokat. Az ilyen anyagokat észleléskor haladéktalanul töröljük. Felkérünk minden látogatónkat, hogy aki ilyet észlel, feltétlenül jelezze számunkra.</p>

							<p>Fenntartjuk magunknak azt a jogot, hogy az ilyen, vagy ehhez hasonló jellegű tartalmakat közlő látogatókat - az eredménytelen figyelmeztetést követően - a felhasználásból kizárjuk, regisztrációjukat megszüntessük, oldalaink látogatásától eltiltsuk.</p>

							<p>A látogatóink által beküldött hozzászólások, blog bejegyzések, fórumtémák tartalmáért felelősséget nem vállalunk, attól elhatárolódunk, azok nem képviselik a cég álláspontját.</p>

							<p>A Kőrös-Blank Biztosítási Alkusz Kft. weboldalain megjelenő információk, adatok, feltételek kizárólag tájékoztató jellegűek, azok teljességéért, pontosságáért a cég felelősséget nem vállal. Az aktuális feltételrendszereket az adott biztosító vagy a pénztári szolgáltató honlapján, vagy fiókjaiban közzétett mindenkor hatályos Általános és Üzletági Üzletszabályzatok és Hirdetmények tartalmazzák.</p>

							<p>A Kőrös-Blank Biztosítási Alkusz Kft. nem tartozik felelősséggel azokért az esetlegesen bekövetkező károkért, veszteségekért, költségekért, amelyek a weboldalak használatából, azok használatra képtelen állapotából, nem megfelelő működéséből, üzemzavarából, az adatok bárki által történő illetéktelen megváltoztatásából keletkeznek, illetve amelyek az információtovábbítási késedelemből, számítógépes vírusból, vonal- vagy rendszerhibából, vagy más hasonló okból származnak.</p>

							<p>Jelen honlap a Kőrös-Blank Biztosítási Alkusz Kft. tulajdona, szellemi termék. Az itt megjelenő információk részben, vagy egészben csak és kizárólag a Kőrös-Blank Biztosítási Alkusz Kft. írásos engedélyével használhatók fel, vagy idézhetők.</p>

						</div>

					</div>

            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>