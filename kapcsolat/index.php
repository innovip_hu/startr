<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'kapcsolat';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
	<script>
		function level_ellenorzes() {
			document.getElementById('riaszt_nev').style.height = '0';
			document.getElementById('riaszt_email').style.height = '0';
			document.getElementById('riaszt_uzenet').style.height = '0';
			document.getElementById('riaszt_telefon').style.height = '0';
			var hirl_oke = 0;
			if(document.getElementById("nev").value == '')
			{
				document.getElementById('riaszt_nev').style.height = '14px';
				var hirl_oke = 1;
			}

			if(document.getElementById("email").value == '')
			{
				document.getElementById('riaszt_email').style.height = '14px';
				var hirl_oke = 1;
			}

			if(document.getElementById("telefon").value == '')
			{
				document.getElementById('riaszt_telefon').style.height = '14px';
				var hirl_oke = 1;
			}

			if(document.getElementById("uzenet").value == '')
			{
				document.getElementById('riaszt_uzenet').style.height = '14px';
				var hirl_oke = 1;
			}

			
			if(grecaptcha.getResponse().length !== 0){
				// alert('checked');
				// document.getElementById("kapcs_alert_recaptcha").style.opacity = '0';
			}
			else
			{
				// alert('no checked');
				// document.getElementById("kapcs_alert_recaptcha").style.opacity = '1';
				var hirl_oke = 1;
			}

			if (!$('#adatv').is(':checked'))
			{
				var hirl_oke = 1;
				$('#adatv_alert').show(400);
			}
			else
			{
				$('#adatv_alert').hide(400);
			}
			
			if(hirl_oke == 0)
			{
				$('#ell_div').html('<input type="hidden" name="ell_input" value="ok" />')
				document.getElementById('contact-form').submit();
			}
		}
		$(window).on("load", function() {
			$('.close').click(function() {
				$('.modal').css('display', 'none');
			});
		});
	</script>
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container">
                <div class="row">
                    <div class="grid_4">
                        <h3 class="border">Elérhetőség</h3>
                        <div class="box02">
                        	<h4>Irodánk</h4>
                            <h5 style="font-weight: bold;">6724 Szeged<br>Csemegi utca 11. fsz. 3.<br/><span style="font-weight: normal;">(bejárat a Vasas Szent Péter utca felől)</span></h5>
                            <div class="addr-wrap">
								<address class="addr">
									<dl>
										<dt>Telefon:</dt>
										<dd><a class="link2" href="tel:+36 62 548 696">+36 62 548 696</a></dd>
									</dl>
									<dl>
										<dt>E-mail:</dt>
										<dd><a class="link2" href="mailto:info@startr.hu">info@startr.hu</a></dd>
									</dl>
									<dl>
										<dt>Kövessen minket:</dt>
										<dd><a href="https://www.facebook.com/startrkft/" target="_blank" class="fa-facebook link" style="color: #3B5998;"></a></dd>
									</dl>																	
								</address>
                            </div>
                            <h4>Nyitva tartás</h4>
                            <div class="addr-wrap">
								<address class="addr">
									<dl>
										<dt>Hétfő:</dt>
										<dd>9-16</dd>
									</dl>
									<dl>
										<dt>Kedd:</dt>
										<dd>9-16</dd>
									</dl>
									<dl>
										<dt>Szerda:</dt>
										<dd>9-16</dd>
									</dl>
									<dl>
										<dt>Csütörtök:</dt>
										<dd>9-16</dd>
									</dl>
									<dl>
										<dt>Péntek:</dt>
										<dd>9-12</dd>
									</dl>																											
									<dl>
										<dt>Szombat:</dt>
										<dd>zárva</dd>
									</dl>	
									<dl>
										<dt>Vasárnap:</dt>
										<dd>zárva</dd>
									</dl>																		
								</address> 								
                            </div>                            
                        </div>
                    </div>
                    <div class="grid_8">
                        <h3 class="border">Ajánlatkérés</h3>
                        <form id="contact-form" class='contact-form' method="POST" action="">
                            <div class="contact-form-loader"></div>
                            <fieldset>
                                <label class="name">
                                    <input type="text" name="nev" id="nev" placeholder="Név:" value=""/>
                                    <span id="riaszt_nev" class="empty-message">*Kitöltés kötelező.</span>
                                </label>
                                <label class="email">
                                    <input type="text" name="email" id="email" placeholder="Email:" value="" />
                                    <span id="riaszt_email" class="empty-message">*Kitöltés kötelező.</span>
                                </label>
                                <label class="phone">
                                    <input type="text" name="telefon" id="telefon" placeholder="Telefon:" value=""/>
                                    <span id="riaszt_telefon" class="empty-message">*Kitöltés kötelező.</span>
                                </label>
                                <label class="message">
                                    <textarea name="uzenet" id="uzenet" placeholder="Üzenet*:"></textarea>
                                    <span id="riaszt_uzenet" class="empty-message">*Kitöltés kötelező.</span>
                                </label>
								<div style="float: right; width:100%; margin-bottom:20px;"><div style="float: right;" class="g-recaptcha" data-sitekey="6LdOVyIUAAAAAK-6ztfLCkWYchKK_kqtrxcDMuLs"></div></div>
								<div id="ell_div"></div>
                                <a class="btn" onClick="level_ellenorzes()" id="kuldes_gomb">Üzenet küldése</a>
                                <div>
                                	<input type="checkbox" name="adatv" id="adatv"> A Start-R Biztosítási Alkusz Kft. <a href="<?=$domain?>/aszf/Adatkezelesi_tajekoztato.pdf" class="link2" target="_blank">Adatvédelmi Tájékoztatóját</a> elolvastam, megértettem, és hozzájárulok ahhoz, hogy megadott adataimat a szolgáltató, mint adatkezelő a Tájékoztatóban foglaltaknak megfelelően kezelje.
                                </div>
                                <div class="box2_color4" style="margin-top: 10px; padding: 10px; display: none;" id="adatv_alert">
                                	<p style="margin-bottom: 0px; color: #fff;">Az Adatkezelési tájékoztató nem lett elfogadva!</p>
                                </div>
                            </fieldset>
							<?php		
								if(isset($_POST['nev']) && isset($_POST['ell_input']))
								{
									// Levélküldés
									$uzenet = '<p>Az alábbi üzenet érkezett a weboldalról:</p>
												<p><b>Név:</b> '.$_POST['nev'].'
												<br><b>E-mail:</b> '.$_POST['email'].'
												<br><b>Telefon:</b> '.$_POST['telefon'].'
												<p><b>Üzenet:</b><br>'.$_POST['uzenet'].'</p>';
									include $gyoker.'/module/mod_email_sajat.php';
									//phpMailer
									require_once($gyoker.'/PHPMailer-master/PHPMailerAutoload.php');
									$mail = new PHPMailer();
									$mail->isHTML(true);
									$mail->CharSet = 'UTF-8';
									$mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
									$mail->SetFrom('uzenet@startr.hu', $webnev);
									$mail->AddAddress('info@startr.hu', $webnev);
									$mail->Subject = "Üzenet a weboldalról";
									$htmlmsg = $mess;
									$mail->Body = $htmlmsg;
									if(!$mail->Send()) {
									  echo "Mailer Error: " . $mail->ErrorInfo;
									}

									?>
										<div class="modal fade response-message" style="display:block;">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close">
															&times;
														</button>
														<h4 class="modal-title">Üzenet elküldve</h4>
													</div>
													<div class="modal-body">
														Üzenetét köszönjük, munkatársunk hamarosan felveszi a kapcsolatot Önnel.
													</div>
												</div>
											</div>
										</div>
									<?php
								}
							?>
							
                        </form>
                    </div>
                </div>
            </div>
        </section>
		<?php
			include $gyoker.'/module/mod_terkep.php';
		?>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>