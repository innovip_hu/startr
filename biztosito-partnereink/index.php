<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'biztosito-partnereink';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
	<script>
		 $(window).on("load", function() {
			$('.sor').each(function(){  
				var highestBox = 0;
				$('.box_kep', this).each(function(){

					if($(this).height() > highestBox) 
					   highestBox = $(this).height(); 
				});  
				$('.box_kep',this).height(highestBox);
			});  
			$('.sor').each(function(){  
				var highestBox = 0;
				$('.logo_box', this).each(function(){

					if($(this).height() > highestBox) 
					   highestBox = $(this).height(); 
				});  
				$('.logo_box',this).height(highestBox);
			});  
		});
	</script>
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container">
                <h3 class="border">Biztosító partnereink</h3>
				
					<div class="row sor">
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/logo_aegon.jpg"></div>
								<div class="box1_cnt">
									<p>AEGON Magyarország Általános Biztosító Zrt.</p>
								</div>
							</div>
						</div>
						<?php /*
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/aig.png"></div>
								<div class="box1_cnt">
									<p>AIG Europe Limited Magyarországi Fióktelepe</p>
								</div>
							</div>
						</div>
						*/ ?>
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/allianz.jpg"></div>
								<div class="box1_cnt">
									<p>Allianz Hungária Biztosító Zrt.</p>
								</div>
							</div>
						</div>
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/pannonia_uj.jpg"></div>
								<div class="box1_cnt">
									<p>CIG Pannónia Első Magyar Általános Biztosító Zrt.</p>
								</div>
							</div>
						</div>
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/eub-logo.jpg"></div>
								<div class="box1_cnt">
									<p>EURÓPAI UTAZÁSI Biztosító Zrt.</p>
								</div>
							</div>
						</div>
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/generali.jpg"></div>
								<div class="box1_cnt">
									<p>Generali Biztosító Zrt.</p>
								</div>
							</div>
						</div>
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/genertel.png"></div>
								<div class="box1_cnt">
									<p>GENERTEL Biztosító Zrt.</p>
								</div>
							</div>
						</div>						
					</div>
				
					<div class="row sor">
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/Groupama_Logo.jpg"></div>
								<div class="box1_cnt">
									<p>Groupama Biztosító Zrt.</p>
								</div>
							</div>
						</div>
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/kh2.jpg"></div>
								<div class="box1_cnt">
									<p>K&H Biztosító Zrt.</p>
								</div>
							</div>
						</div>
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/kobe.jpg"></div>
								<div class="box1_cnt">
									<p>KÖBE Közép-európai Kölcsönös Biztosító Egyesület</p>
								</div>
							</div>
						</div>
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/posta.jpg"></div>
								<div class="box1_cnt">
									<p>Magyar Posta Biztosító Zrt.</p>
								</div>
							</div>
						</div>
						<?php /*
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/MKB_logo.jpg"></div>
								<div class="box1_cnt">
									<p>MKB Általános Biztosító Zrt</p>
								</div>
							</div>
						</div>
						*/ ?>
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/ColonnadeLogo.jpg"></div>
								<div class="box1_cnt">
									<p>Collonade Insurance S.A. Magyarországi Fióktelepe</p>
								</div>
							</div>
						</div>
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/signal_logo.jpg"></div>
								<div class="box1_cnt">
									<p>Signal Iduna Biztosító Zrt.</p>
								</div>
							</div>
						</div>						
					</div>
				
					<div class="row sor">
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/union_logo.jpg"></div>
								<div class="box1_cnt">
									<p>UNION Vienna Insurance Group Biztosító Zrt.</p>
								</div>
							</div>
						</div>
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/uniqua_logo.jpg"></div>
								<div class="box1_cnt">
									<p>UNIQA Biztosító Zrt.</p>
								</div>
							</div>
						</div>
						<?php /*
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/vienna_logo.jpg"></div>
								<div class="box1_cnt">
									<p>Vienna Life Vienna Insurance Group Biztosító Zrt.</p>
								</div>
							</div>
						</div>
						*/ ?>
						<div class="grid_2 wow zoomIn">
							<div class="box1 logo_box">
								<div class="box_kep"><img src="../images/logok/waberer_logo.png"></div>
								<div class="box1_cnt">
									<p>Wáberer Hungária Biztosító Zrt.</p>
								</div>
							</div>
						</div>
					</div>

            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>