<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'panaszkezeles';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Panaszkezelés</h3>
				
				<p>Köszönjük, hogy ügyfelünkként megtisztelt minket bizalmával! Számunkra kiemelt fontosságú, hogy Ön elégedett legyen velünk, és hosszú távon is minket válasszon. Együttműködésünk sikeréhez hozzájárul, ha&nbsp;megosztja velünk véleményét, észrevételeit és természetesen kifogásait, panaszait is, hiszen ezek segítségével tudjuk leginkább javítani folyamatainkat és fejleszteni szolgáltatásunk minőségét.</p>

				<p>Észrevételeivel, panaszával az alábbi csatornákon keresztül fordulhat hozzánk:</p>

				<p><br />
				<strong>Személyesen:</strong>&nbsp;Kőrös-Blank Kft., 6200 Kiskőrös, Petőfi tér 9.&nbsp;</p>

				<p>Nyitva tartás személyes panasz esetére:</p>

				<ul>
					<li>
					<p>Hétfő: 7:30-16:30</p>
					</li>
					<li>
					<p>Kedd - Csütörtök: 8:00-16:30</p>
					</li>
					<li>
					<p>Péntek: 8:00-16:00</p>
					</li>
				</ul>

				<p>Ebédidő: 12:00 - 12:30</p>

				<p><strong>Telefonon:</strong>&nbsp;36 21 333-2942</p>

				<p>Elérhetőségi idő telefonos panasz esetére:</p>

				<ul>
					<li>
					<p>Hétfő: 8:00-20:00</p>
					</li>
					<li>
					<p>Kedd - Péntek: 8:00-16:00</p>
					</li>
				</ul>

				<p><strong>Faxon:</strong>&nbsp;+36 78 514-083&nbsp;</p>

				<p><strong>E-mailben:</strong>&nbsp;<u><a href="mailto:panaszkezeles@korosblank.hu">panaszkezeles@korosblank.hu</a></u><br />
				&nbsp;</p>

				<p><strong>Panaszkezelés folyamata</strong></p>

				<p>Panaszkezelési szabályzatunk alapján történik, melyet&nbsp;<u><a href="https://www.korosblank.hu/upload/Panaszkezelsiszablyzat2022.08.15.pdf" target="_blank">elérhet itt</a></u>, illetve nyomtatva az ügyféltérben kihelyezve.&nbsp;Amennyiben panasza elutasításra kerül, kérheti tőlünk a felügyeletünket ellátó Magyar Nemzeti Bankhoz (MNB) beadható fogyasztói megkeresés vagy a Pénzügyi Békéltető Testülethez (PBT) intézhető, vitarendezésre irányuló eljárás megindítására vonatkozó kérelem formanyomtatványát. Minden szükséges nyomtatványt díjmentesen biztosítunk a kérelmezőnek. Kifogását, panaszát előterjesztheti a Magyar Nemzeti Bank (MNB) weboldalán elérhető formanyomtatványon is, mely honlapot&nbsp;<u><a href="https://www.mnb.hu/fogyasztovedelem/penzugyi-panasz" target="_blank">ide kattintva</a></u>&nbsp;érhet el, illetve az alábbi linkeken keresztül közvetlenül is letöltheti a dokumentumokat:<br />
				&nbsp;</p>

				<ul>
					<li>
					<p><u><a href="https://www.korosblank.hu/upload/150-fogyasztoi-kerelem-1-2.pdf" target="_blank">Általános Fogyasztói kérelem Békéltető testülethez</a></u></p>
					</li>
					<li>
					<p><u><a href="https://www.korosblank.hu/upload/fogyasztoi-kerelem-az-mnbhez-03-01.doc" target="_blank">Magyar Nemzeti Bankhoz címzett fogyasztói kérelem minta</a> (</u><u>letölthető pdf)</u></p>
					</li>
					<li>
					<p><u><a href="https://www.korosblank.hu/upload/fogyasztoi-panasz-szolgaltatonak-2.doc" target="_blank">Pénzügyi szervezethez benyújtandó panasz minta</a> (</u><u>letölthető pdf)</u><br />
					&nbsp;</p>
					</li>
				</ul>

				<p>Továbbá szíves figyelmébe ajánljuk a Magyar Nemzeti Bank (MNB) által közzétett&nbsp;<u><a href="https://www.korosblank.hu/upload/mittegyunkpenzugyinc.pdf" target="_blank">„Mit tegyünk, ha pénzügyi panaszunk van?”</a></u>&nbsp;és&nbsp;<u><a href="https://www.korosblank.hu/upload/pbt-nc-2.pdf" target="_blank">„Pénzügyi Békéltető Testület”</a></u>&nbsp;témájú tájékoztatókat is, melyek további információkkal szolgálnak.<br />
				&nbsp;</p>

				<p><strong>Online vitarendezési fórum</strong>&nbsp;<u><a href="https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&amp;lng=HU" target="_blank">linkje elérhető itt</a></u>.</p>

				<p><strong>Pénzügyi navigátor füzetek</strong>&nbsp;<u><a href="https://www.mnb.hu/fogyasztovedelem/penzugyi-navigator-fuzetek" target="_blank">linkje elérhető itt.</a></u></p>

				<p>&nbsp;</p>

				<p><strong>Főbb adataink</strong></p>

				<p><strong>Cégnév:</strong>&nbsp;Kőrös-Blank Kft.<br />
				<strong>Székhely:</strong>&nbsp;6200 Kiskőrös, Petőfi tér 9.<br />
				<strong>Cégjegyzékszám:</strong>&nbsp;Cg. 03-09-107592<br />
				<strong>Adószám:</strong>&nbsp;11863234-1-03<br />
				<strong>Bejegyezve:</strong>&nbsp;1999<br />
				<strong>Számlaszám:</strong>&nbsp;52500075-11035123-00000000<br />
				<strong>Számlavezető pénzintézet:</strong>&nbsp;Rónasági Takarékszövetkezet (6200 Kiskőrös, Petőfi tér 10-11. )<br />
				Minősített befolyással egy biztosítóban SEM rendelkezünk.<br />
				Biztosító társaság vagy annak anyavállalata NEM rendelkezik minősített befolyással a Kőrös-Blank Kft-ben.<br />
				<br />
				Szakmai tevékenységünkkel kapcsolatos kár esetére alkuszi felelősség biztosítással rendelkezünk.<br />
				<strong>Biztosító:</strong>&nbsp;Generali Biztosító Zrt.&nbsp;<br />
				<strong>Szerződésszám:</strong>&nbsp;95610980921978300<br />
				Alkuszi megbízásunk alapján FÜGGETLEN BIZTOSÍTÁSKÖZVETÍTŐ-ként végezzük munkánkat.<br />
				<strong>MNB tevékenységi engedély:</strong>&nbsp;aktív státuszú független közvetítő<br />
				Ellenőrizhető:&nbsp;<u><a href="https://intezmenykereso.mnb.hu/" target="_blank">https://intezmenykereso.mnb.hu/</a></u></p>

				<p><strong>MNB regisztrációs szám:</strong>&nbsp;204121402624<br />
				Ellenőrizhető:&nbsp;<u><a href="https://intezmenykereso.mnb.hu/" target="_blank">https://intezmenykereso.mnb.hu/</a></u></p>

				<p><strong>FBAMSZ Etika Kódexe:</strong>&nbsp;<u><a href="https://fbamsz.hu/fbamsz/uzleti-etikai-kodex" target="_blank">https://fbamsz.hu/fbamsz/uzleti-etikai-kodex</a></u><br />
				&nbsp;</p>

				<p><strong>Adatkezelés</strong><br />
				Nemzeti Adatvédelmi és Információszabadság Hatósági (NAIH) nyilvántartási szám:&nbsp;NAIH-50973/2012.<br />
				Adatkezelési tájékoztatónk&nbsp;elérhető&nbsp;<u><a href="https://www.korosblank.hu/uploadfiles/doc/Adatkezelesi_tajekoztato.pdf" target="_blank">itt</a></u>.</p>

				<p>Adatkezelési szabályzatunk elérhető&nbsp;<u><a href="https://www.korosblank.hu/upload/Adatkezelesiszabalyzat201907.pdf">itt</a></u>.</p>

				<p><strong>Magyar Nemzeti Bank Pénzügyi Fogyasztóvédelmi Központ elérhetőségei</strong></p>

				<p>Postacím: 1534 Budapest BKKP Postafiók: 777.<br />
				E- mail:&nbsp;<u><a href="mailto:ugyfelszolgalat@mnb.hu">ugyfelszolgalat@mnb.hu</a></u><br />
				Telefon: +36&nbsp;40&nbsp;203-776&nbsp;<br />
				Személyes&nbsp;ügyfélszolgálat: 1013 Budapest, Krisztina krt. 39.<br />
				Weboldal:&nbsp;<u><a href="http://www.mnb.hu/bekeltetes">http://www.mnb.hu/</a><a href="http://www.mnb.hu/fogyasztovedelem">fogyasztovedelem</a></u></p>

				<p>&nbsp;&nbsp; &nbsp;<br />
				<strong>Pénzügyi Békéltető Testület elérhetőségei</strong><br />
				Postacím: 1525 Budapest BKKP Pf. 172.<br />
				E- mail:&nbsp;<u><a href="mailto:ugyfelszolgalat@mnb.hu">ugyfelszolgalat@mnb.hu</a></u><br />
				Telefon: +36&nbsp;40&nbsp;203-776&nbsp;<br />
				Személyes&nbsp;ügyfélszolgálat: 1133 Budapest, Váci út 76.<br />
				Weboldal:&nbsp;<u><a href="http://www.mnb.hu/bekeltetes">http://www.mnb.hu/bekeltetes</a></u><br />
				&nbsp;</p>

				<p><strong>Európai online vitarendezési platform</strong></p>

				<p>Az Európai Parlament és a Tanács fogyasztói jogviták online rendezéséről szóló, 2013. május 21-i 524/2013/EU rendelete (Rendelet) alapján az Európai Bizottság létrehozott egy online vitarendezési platformot.<br />
				<br />
				A Rendelet szerint az Európai Unióban tartózkodási hellyel rendelkező fogyasztók és az Európai Unióban letelepedett szolgáltatók közötti, online szolgáltatási szerződésekből eredő kötelezettségekkel kapcsolatban felmerülő jogviták, így az online megkötött szerződésekkel összefüggő pénzügyi fogyasztói jogviták bírósági eljáráson kívüli rendezésére irányuló kommunikációt ezen a platformon keresztül végezhetik a fogyasztók.<br />
				&nbsp;</p>

				<p>Az online vitarendezési platform&nbsp;<u><a href="http://ec.europa.eu/odr" target="_blank">elérhető&nbsp;</a><a href="http://http/ec.europa.eu/odr">itt</a></u>.</p>

				<p><u><a href="https://webgate.ec.europa.eu/odr/userguide/" target="_blank">További információk (felhasználói útmutatók)</a></u>&nbsp;az online vitarendezési platformról.</p>



            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>