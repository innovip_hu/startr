<?php
	// Hírek
	$conf_hirek = 1;
	// Hírlevél
	$conf_hirlevel = 0;
	// Banner
	$conf_banner = 0;
	$conf_banner_meret = ' (Méret: 300 x 250 pixel)';
	// Slider
	$conf_slider = 1;
	$conf_slider_meret = ' (Méret: 1905 x 585 pixel)';
	// Video Slider
	$conf_video_slider = 0;
	$conf_video_slider_kikapcsolt_mezok = array(/*'nev', 'link', 'szoveg', */'sorrend');
	// Galéria
	$conf_galeria = 0;
	// Kívánságlista
	$conf_kivansaglista = 0;
	// Árak megosztása
	$conf_arak_megosztasa = 0;
	
	// Webshop
	$conf_webshop = 0;
	// Kupon
	$conf_kupon = 1;
	// Több kategóriához való rendelés
	$conf_tobb_kategoria = 1;
	// Készlet
	$conf_keszlet = 1;
	// Paraméterek
	$conf_parameterek = 1;
	// Szállítás
		// Egyedi szállítás
		$conf_szall_egyedi = 1;
		// PostaPont
		$conf_szall_postapont = 1;
		// GLS CsomagPont
		$conf_szall_gls_cspont = 1;
?>  