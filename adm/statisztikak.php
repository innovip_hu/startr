﻿<?php
	session_start();
	ob_start();
	
	include '../config.php';
	
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	$oldal = 'statisztikak';
	
	$honapok = array("nulla", "Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December");
	$szinek = array("#00c0ef", "#3c8dbc", "#d2d6de","#f56954","#f39c12",  "#605CA8", "#00A65A", "#D81B60", "#39CCCC");
	$szinek_2 = array("#f56954", "#f39c12", "#605CA8", "#39CCCC", "#00c0ef", "#3c8dbc", "#d2d6de", "#00A65A", "#D81B60");
	shuffle($szinek);
	shuffle($szinek_2);
	
	$datum_kezd = date("Y-m-d", strtotime("-3 months"));
	$datum_vege = date('Y-m-d');
	if(isset($_POST['datum_kezd']))
	{
		$datum_kezd = $_POST['datum_kezd'];
		$datum_vege = $_POST['datum_vege'];
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Statisztikák | Admin</title>
		<?php
			include 'module/head.php';
		?>
		<script src="scripts/statisztikak.js"></script>
	</head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
		<?php
			include 'module/header.php';
			include 'module/menu.php';
		?>
		
		<div id="munkaablak">
			<div class="content-wrapper">
				<section class="content-header">
				  <h1 id="myModal">Statisztikák <small><form style="display: inline;" method="POST" action="" id="datumKuldes"><input onChange="datumMod()" class="stat_datum datepicker" type="text" name="datum_kezd" id="datum_kezd" value="<?php print $datum_kezd; ?>" /> - <input onChange="datumMod()" class="stat_datum datepicker" type="text" name="datum_vege" id="datum_vege" value="<?php print $datum_vege; ?>" /></form> között</small></h1>
				  <ol class="breadcrumb">
					<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
					<li class="active">Statisztikák</li>
				  </ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<!-- AREA CHART -->
							<div class="box box-primary">
								<div class="box-header with-border">
								  <h3 class="box-title">Vásárlások értéke</h3>
								  <div class="box-tools pull-right">
									<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								  </div>
								</div>
								<div class="box-body">
								  <div class="chart">
									<canvas id="areaChart" style="height:250px"></canvas>
								  </div>
								</div><!-- /.box-body -->
							</div><!-- /.box -->

							<!-- DONUT CHART -->
							<div class="box box-danger">
								<div class="box-header with-border">
								  <h3 class="box-title">Szállítási módok</h3>
								  <div class="box-tools pull-right">
									<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								  </div>
								</div>
								<div class="box-body">
									<canvas id="pieChart" style="height:250px"></canvas>
									<?php
										$query = "SELECT *, COUNT(szall_mod) as db FROM ".$webjel."rendeles 
											WHERE datum >= '".$datum_kezd."' AND datum <= '".$datum_vege."' AND teljesitve = 1 
											GROUP BY szall_mod
											ORDER BY szall_mod ASC";
										$i = 0;
										foreach ($pdo->query($query) as $row)
										{
											print '<div><i class="fa fa-square" style="color:'.$szinek[$i].'"></i> '.$row['szall_mod'].': '.$row['db'].' db</div>';
											$i++;
										}
									?>
								</div><!-- /.box-body -->
							</div><!-- /.box -->

							<!-- DONUT CHART -->
							<div class="box box-info">
								<div class="box-header with-border">
								  <h3 class="box-title">Fizetési módok</h3>
								  <div class="box-tools pull-right">
									<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								  </div>
								</div>
								<div class="box-body">
									<canvas id="pieChart2" style="height:250px"></canvas>
									<?php
										$query = "SELECT *, COUNT(fiz_mod) as db FROM ".$webjel."rendeles 
											WHERE datum >= '".$datum_kezd."' AND datum <= '".$datum_vege."' AND teljesitve = 1 
											GROUP BY fiz_mod
											ORDER BY fiz_mod ASC";
										$i = 0;
										foreach ($pdo->query($query) as $row)
										{
											print '<div><i class="fa fa-square" style="color:'.$szinek_2[$i].'"></i> '.$row['fiz_mod'].': '.$row['db'].' db</div>';
											$i++;
										}
									?>
								</div><!-- /.box-body -->
							</div><!-- /.box -->
							
							<!--Napi vásárlások-->
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Napi vásárlások</h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="box-body no-padding with-border">
									<div class="table-responsive">
										<table class="table table-hover table-striped margbot0">
											<tbody>
												<tr>
													<th>ID</th>
													<th>Kategória</th>
													<th style="text-align:right;">Eladott mennyiség</th>
													<th style="text-align:right;">Összeg</th>
												</tr>
												<?php
													$query_rend = "SELECT *
																		, SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar=0, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db,".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db)) as sum_ar  
																		, SUM(".$webjel."rendeles_tetelek.term_db) as sum_db 
																FROM ".$webjel."rendeles_tetelek 
																INNER JOIN ".$webjel."rendeles 
																ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
																WHERE ".$webjel."rendeles.datum >= '".$datum_kezd."' AND ".$webjel."rendeles.datum <= '".$datum_vege."' AND ".$webjel."rendeles.teljesitve = 1 
																GROUP BY ".$webjel."rendeles.datum 
																ORDER BY ".$webjel."rendeles.datum DESC";
													$szamlalo = 0;
													foreach ($pdo->query($query_rend) as $row_rend)
													{
														print '<tr>
															<td>'.$row_rend['datum'].'</td>
															<td>'.$row_rend['datum'].'</td>
															<td style="text-align:right;">'.$row_rend['sum_db'].' db</td>
															<td style="text-align:right;">'.number_format($row_rend['sum_ar'], 0, ',', ' ').' Ft</td>
														</tr>';
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>

						</div><!-- /.col (LEFT) -->
						<div class="col-md-6">

						  <!-- BAR CHART -->
						  <div class="box box-warning">
							<div class="box-header with-border">
								<h3 class="box-title">Regisztrállt-, és nem regisztrált vásárlók</h3>
								<div class="box-tools pull-right">
									<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								</div>
							</div>
							<div class="box-body">
								<div class="chart">
									<canvas id="barChart" style="height:230px"></canvas>
								</div>
								<i class="fa fa-square" style="color:#39CCCC"></i> Regisztráltak <i class="fa fa-square" style="color:#3C8DBC"></i> Nem regisztráltak
							</div>
						  </div>

						  <!-- BAR CHART -->
						  <div class="box box-success">
							<div class="box-header with-border">
								<h3 class="box-title">Akciós-, és nem akciós termékek</h3>
								<div class="box-tools pull-right">
									<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								</div>
							</div>
							<div class="box-body">
								<div class="chart">
									<canvas id="barChart2" style="height:230px"></canvas>
								</div>
								<i class="fa fa-square" style="color:#D2D6DE"></i> Akciós <i class="fa fa-square" style="color:#00C0EF"></i> Nem akciós
							</div>
						  </div>
						  
							<!--Legnépszerűbb kategóriák-->
							<div class="box box-warning">
								<div class="box-header with-border">
									<h3 class="box-title">Legnépszerűbb kategóriák</h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="box-body no-padding with-border">
									<div class="table-responsive">
										<table class="table table-hover table-striped margbot0">
											<tbody>
												<tr>
													<th>ID</th>
													<th>Kategória</th>
													<th style="text-align:right;">Eladott mennyiség</th>
													<th style="text-align:right;">Összeg</th>
												</tr>
												<?php
													$query_rend = "SELECT *
																		, SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar=0, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db,".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db)) as sum_ar  
																		, SUM(".$webjel."rendeles_tetelek.term_db) as sum_db 
																		, ".$webjel."term_csoportok.nev_url as csop_url
																		, ".$webjel."termekek.nev_url as term_url
																		, ".$webjel."termekek.nev as termek_nev
																		, ".$webjel."term_csoportok.nev as kategoria_nev
																		, ".$webjel."term_csoportok.id as csoport_id
																FROM ".$webjel."rendeles_tetelek 
																INNER JOIN ".$webjel."termekek 
																ON ".$webjel."rendeles_tetelek.term_id = ".$webjel."termekek.id 
																INNER JOIN ".$webjel."term_csoportok 
																ON ".$webjel."termekek.csop_id = ".$webjel."term_csoportok.id 
																INNER JOIN ".$webjel."rendeles 
																ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
																WHERE ".$webjel."rendeles.datum >= '".$datum_kezd."' AND ".$webjel."rendeles.datum <= '".$datum_vege."' AND ".$webjel."rendeles.teljesitve = 1 
																GROUP BY ".$webjel."term_csoportok.id 
																ORDER BY sum_db DESC LIMIT 20";
													$szamlalo = 0;
													foreach ($pdo->query($query_rend) as $row_rend)
													{
														print '<tr>
															<td>'.$row_rend['csoport_id'].'</td>
															<td><a href="'.$domain.'/termekek/'.$row_rend['csop_url'].'" style="color:#000; text-decoration:none;" target="_blank">'.$row_rend['kategoria_nev'].'</a></td>
															<td style="text-align:right;">'.$row_rend['sum_db'].' db</td>
															<td style="text-align:right;">'.number_format($row_rend['sum_ar'], 0, ',', ' ').' Ft</td>
														</tr>';
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						  
							<!--Legnépszerűbb termékek-->
							<div class="box box-lila">
								<div class="box-header with-border">
									<h3 class="box-title">Legnépszerűbb termékek</h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="box-body no-padding with-border">
									<div class="table-responsive">
										<table class="table table-hover table-striped margbot0">
											<tbody>
												<tr>
													<th>ID</th>
													<th>Termék</th>
													<th style="text-align:right;">Eladott mennyiség</th>
													<th style="text-align:right;">Összeg</th>
												</tr>
												<?php
													$query_rend = "SELECT ".$webjel."rendeles_tetelek.term_id, 
																		".$webjel."rendeles_tetelek.rendeles_id,
																		SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar=0, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db,".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db)) as sum_ar, 
																		SUM(".$webjel."rendeles_tetelek.term_db) as sum_db, 
																		".$webjel."rendeles.id,
																		".$webjel."rendeles.teljesitve,
																		".$webjel."rendeles.datum
																FROM ".$webjel."rendeles_tetelek 
																INNER JOIN ".$webjel."rendeles 
																ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
																WHERE ".$webjel."rendeles.datum >= '".$datum_kezd."' AND ".$webjel."rendeles.datum <= '".$datum_vege."' AND ".$webjel."rendeles.teljesitve = 1 
																GROUP BY ".$webjel."rendeles_tetelek.term_id 
																ORDER BY sum_db DESC LIMIT 20";
													$szamlalo = 0;
													foreach ($pdo->query($query_rend) as $row_rend)
													{
														$query_ell = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$row_rend['term_id'];
														$res = $pdo->prepare($query_ell);
														$res->execute();
														$row_ell  = $res -> fetch();
														if (isset($row_ell['lathato']) && $row_ell['lathato'] == 0) // Megvan-e még a termék, és látható-e
														{
															$query = 'SELECT '.$webjel.'termekek.nev, 
																			'.$webjel.'termekek.nev_url, 
																			'.$webjel.'termekek.id as termek_id, 
																			'.$webjel.'termekek.csop_id, 
																			'.$webjel.'term_csoportok.id, 
																			'.$webjel.'term_csoportok.nev_url as csop_url 
																		FROM '.$webjel.'termekek 
																		INNER JOIN '.$webjel.'term_csoportok 
																		ON '.$webjel.'termekek.csop_id = '.$webjel.'term_csoportok.id 
																		WHERE '.$webjel.'termekek.id='.$row_rend['term_id'];
															$res = $pdo->prepare($query);
															$res->execute();
															$row  = $res -> fetch();
															print '<tr>
																<td>'.$row['termek_id'].'</td>
																<td><a href="'.$domain.'/termekek/'.$row['csop_url'].'/'.$row['nev_url'].'" style="color:#000; text-decoration:none;" target="_blank">'.$row['nev'].'</a></td>
																<td style="text-align:right;">'.$row_rend['sum_db'].' db</td>
																<td style="text-align:right;">'.number_format($row_rend['sum_ar'], 0, ',', ' ').' Ft</td>
															</tr>';
														}
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						  
							<!--Top regisztrált vásárlók-->
							<div class="box box-bibor">
								<div class="box-header with-border">
									<h3 class="box-title">Top regisztrált vásárlók</h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="box-body no-padding with-border">
									<div class="table-responsive">
										<table class="table table-hover table-striped margbot0">
											<tbody>
												<tr>
													<th>ID</th>
													<th>Termék</th>
													<th style="text-align:right;">Vásárolt termékek</th>
													<th style="text-align:right;">Összeg</th>
												</tr>
												<?php
													$query_rend = "SELECT ".$webjel."rendeles_tetelek.id, 
																		".$webjel."rendeles_tetelek.rendeles_id,
																		SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar=0, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db,".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db)) as sum_ar, 
																		SUM(".$webjel."rendeles_tetelek.term_db) as sum_db, 
																		".$webjel."rendeles.id,
																		".$webjel."rendeles.user_id,
																		".$webjel."rendeles.teljesitve,
																		".$webjel."rendeles.datum,
																		".$webjel."rendeles.noreg
																FROM ".$webjel."rendeles_tetelek 
																INNER JOIN ".$webjel."rendeles 
																ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
																WHERE ".$webjel."rendeles.datum >= '".$datum_kezd."' AND ".$webjel."rendeles.datum <= '".$datum_vege."' AND ".$webjel."rendeles.teljesitve = 1 AND ".$webjel."rendeles.noreg=0 
																GROUP BY ".$webjel."rendeles.user_id 
																ORDER BY sum_ar DESC LIMIT 20";
													$szamlalo = 0;
													foreach ($pdo->query($query_rend) as $row_rend)
													{
														// Felhasználó adatai
														$query = 'SELECT * FROM '.$webjel.'users WHERE id='.$row_rend['user_id'];
														$res = $pdo->prepare($query);
														$res->execute();
														$row  = $res -> fetch();
														print '<tr>';
														print '<td>'.$row['id'].'</td>';
														print '<td>'.$row['vezeteknev'].'</td>';
														print '<td style="text-align:right;">'.$row_rend['sum_db'].' db</td>';
														print '<td style="text-align:right;">'.number_format($row_rend['sum_ar'], 0, ',', ' ').' Ft</td>';
														print '</tr>';
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<?php
			include 'module/footer.php';
		?>
    </div>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="plugins/chartjs/Chart.min.js"></script>
	<!-- Datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Datepicker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- CK Editor -->
    <script src="plugins/ckeditor/ckeditor.js"></script>
    <!-- Dropzone -->
	<script src="scripts/dropzone.js"></script>
    <!-- Page Script -->
    <script>
      $(function () {
        /* ChartJS
         * -------
         * Here we will create a few charts using ChartJS
         */

        //--------------
        //- AREA CHART -
        //--------------

        // Get context with jQuery - using jQuery's .get() method.
        var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var areaChart = new Chart(areaChartCanvas);
			<?php
				$chart_honapok = array();
				$chart_ertekek = array();
				$query = "SELECT *, MONTH(".$webjel."rendeles.datum) as honap, SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar=0, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db,".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db)) as sum_ar  
							FROM ".$webjel."rendeles_tetelek 
							INNER JOIN ".$webjel."rendeles 
							ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
							WHERE ".$webjel."rendeles.datum >= '".$datum_kezd."' AND ".$webjel."rendeles.datum <= '".$datum_vege."' AND ".$webjel."rendeles.teljesitve = 1 
							GROUP BY YEAR(".$webjel."rendeles.datum), MONTH(".$webjel."rendeles.datum)";
				foreach ($pdo->query($query) as $row)
				{
					// print $honapok[$row['honap']].' : '.$row['sum_ar'].'<br/>';
					$chart_honapok[] = $honapok[$row['honap']];
					$chart_ertekek[] = $row['sum_ar'];
				}
			?>
        var areaChartData = {
          labels: [
				<?php
					foreach($chart_honapok as $x)
					{
						print '"'.$x.'", ';
					}
				?>
		  ],
          datasets: [
            {
              label: "Digital Goods",
              fillColor: "rgba(60,141,188,0.9)",
              strokeColor: "rgba(60,141,188,0.8)",
              pointColor: "#3b8bba",
              pointStrokeColor: "rgba(60,141,188,1)",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(60,141,188,1)",
              data: [
			  	<?php
					foreach($chart_ertekek as $x)
					{
						print '"'.$x.'", ';
					}
				?>
			  ]
            }
          ]
        };

        var areaChartOptions = {
          //Boolean - If we should show the scale at all
          showScale: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: false,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - Whether the line is curved between points
          bezierCurve: true,
          //Number - Tension of the bezier curve between points
          bezierCurveTension: 0.3,
          //Boolean - Whether to show a dot for each point
          pointDot: false,
          //Number - Radius of each point dot in pixels
          pointDotRadius: 4,
          //Number - Pixel width of point dot stroke
          pointDotStrokeWidth: 1,
          //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
          pointHitDetectionRadius: 20,
          //Boolean - Whether to show a stroke for datasets
          datasetStroke: true,
          //Number - Pixel width of dataset stroke
          datasetStrokeWidth: 2,
          //Boolean - Whether to fill the dataset with a color
          datasetFill: true,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio: true,
          //Boolean - whether to make the chart responsive to window resizing
          responsive: true
        };

        //Create the line chart
        areaChart.Line(areaChartData, areaChartOptions);


        //-------------
        //- PIE CHART -
        //-------------
        // Szállítási módok
        var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
        var pieChart = new Chart(pieChartCanvas);
        var PieData = [
			<?php
				$query = "SELECT *, COUNT(szall_mod) as db FROM ".$webjel."rendeles 
					WHERE datum >= '".$datum_kezd."' AND datum <= '".$datum_vege."' AND teljesitve = 1 
					GROUP BY szall_mod
					ORDER BY szall_mod ASC";
				$i = 0;
				foreach ($pdo->query($query) as $row)
				{
					print '{
						value: '.$row['db'].',
						color: "'.$szinek[$i].'",
						highlight: "'.$szinek[$i].'",
						label: "'.$row['szall_mod'].'"
					},';
					$i++;
				}
			?>
        ];
        var pieOptions = {
          //Boolean - Whether we should show a stroke on each segment
          segmentShowStroke: true,
          //String - The colour of each segment stroke
          segmentStrokeColor: "#fff",
          //Number - The width of each segment stroke
          segmentStrokeWidth: 2,
          //Number - The percentage of the chart that we cut out of the middle
          percentageInnerCutout: 50, // This is 0 for Pie charts
          //Number - Amount of animation steps
          animationSteps: 100,
          //String - Animation easing effect
          animationEasing: "easeOutBounce",
          //Boolean - Whether we animate the rotation of the Doughnut
          animateRotate: true,
          //Boolean - Whether we animate scaling the Doughnut from the centre
          animateScale: false,
          //Boolean - whether to make the chart responsive to window resizing
          responsive: true,
          // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio: true,
          //String - A legend template
          legendTemplate: "<ul><li>asd<li><li>asd<li><li>asd<li></ul>"
          // legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(PieData, pieOptions);
		// Másik - Fizetési módok
        var pieChartCanvas = $("#pieChart2").get(0).getContext("2d");
        var pieChart = new Chart(pieChartCanvas);
        var PieData = [
			<?php
				$query = "SELECT *, COUNT(fiz_mod) as db FROM ".$webjel."rendeles 
					WHERE datum >= '".$datum_kezd."' AND datum <= '".$datum_vege."' AND teljesitve = 1 
					GROUP BY fiz_mod
					ORDER BY fiz_mod ASC";
				$i = 0;
				foreach ($pdo->query($query) as $row)
				{
					print '{
						value: '.$row['db'].',
						color: "'.$szinek_2[$i].'",
						highlight: "'.$szinek_2[$i].'",
						label: "'.$row['fiz_mod'].'"
					},';
					$i++;
				}
			?>
        ];
        pieChart.Doughnut(PieData, pieOptions);

        //-------------
        //- BAR CHART - Regisztrált és nem regisztrált vásárlók
        //-------------
			<?php
				$chart_honapok_regnoreg = array();
				$chart_ertekek_reg = array();
				$chart_ertekek_noreg = array();
				$query = "SELECT *, SUM(IF(".$webjel."rendeles.noreg = 1, 1, 0)) as noreg_db, SUM(IF(".$webjel."rendeles.noreg = 0, 1, 0)) as reg_db, MONTH(".$webjel."rendeles.datum) as honap  
							FROM ".$webjel."rendeles 
							WHERE ".$webjel."rendeles.datum >= '".$datum_kezd."' AND ".$webjel."rendeles.datum <= '".$datum_vege."' AND ".$webjel."rendeles.teljesitve = 1 
							GROUP BY YEAR(".$webjel."rendeles.datum), MONTH(".$webjel."rendeles.datum)";
				foreach ($pdo->query($query) as $row)
				{
					// print $honapok[$row['honap']].' : noreg: '.$row['noreg_db'].' | reg: '.$row['reg_db'].'<br/>';
					$chart_honapok_regnoreg[] = $honapok[$row['honap']];
					$chart_ertekek_reg[] = $row['reg_db'];
					$chart_ertekek_noreg[] = $row['noreg_db'];
				}
			?>
        var barChartCanvas = $("#barChart").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = {
          labels: [
			  	<?php
					foreach($chart_honapok_regnoreg as $x)
					{
						print '"'.$x.'", ';
					}
				?>
		  ],
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              data: [
			  	<?php
					foreach($chart_ertekek_reg as $x)
					{
						print '"'.$x.'", ';
					}
				?>
			  ]
            },
            {
			  label: "Digital Goods",
              fillColor: "rgba(60,141,188,0.9)",
              strokeColor: "rgba(60,141,188,0.8)",
              data: [
			  	<?php
					foreach($chart_ertekek_noreg as $x)
					{
						print '"'.$x.'", ';
					}
				?>
			  ]
            }
          ]
        };
        barChartData.datasets[0].fillColor = "#39CCCC";
        barChartData.datasets[0].strokeColor = "#39CCCC";
        barChartData.datasets[0].pointColor = "#39CCCC";
        barChartData.datasets[1].fillColor = "#3C8DBC";
        barChartData.datasets[1].strokeColor = "#3C8DBC";
        barChartData.datasets[1].pointColor = "#3C8DBC";
		/* 
        barChartData.datasets[0].fillColor = "<?php print $szinek[4]; ?>";
        barChartData.datasets[0].strokeColor = "<?php print $szinek[4]; ?>";
        barChartData.datasets[0].pointColor = "<?php print $szinek[4]; ?>";
        barChartData.datasets[1].fillColor = "<?php print $szinek[6]; ?>";
        barChartData.datasets[1].strokeColor = "<?php print $szinek[6]; ?>";
        barChartData.datasets[1].pointColor = "<?php print $szinek[6]; ?>";
		*/
        var barChartOptions = {
          //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
          scaleBeginAtZero: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: true,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - If there is a stroke on each bar
          barShowStroke: true,
          //Number - Pixel width of the bar stroke
          barStrokeWidth: 2,
          //Number - Spacing between each of the X value sets
          barValueSpacing: 5,
          //Number - Spacing between data sets within X values
          barDatasetSpacing: 1,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to make the chart responsive
          responsive: true,
          maintainAspectRatio: true
        };

        barChartOptions.datasetFill = false;
        barChart.Bar(barChartData, barChartOptions);
		// Második
			<?php
				$chart_honapok_akcios = array();
				$chart_ertekek_akcios = array();
				$chart_ertekek_noakcios = array();
				$query = "SELECT *, SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar = 0, 1, 0)) as nemakcios_db, 
								SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar > 0, 1, 0)) as akcios_db, 
								MONTH(".$webjel."rendeles.datum) as honap  
							FROM ".$webjel."rendeles_tetelek 
							INNER JOIN ".$webjel."rendeles 
							ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
							WHERE ".$webjel."rendeles.datum >= '".$datum_kezd."' AND ".$webjel."rendeles.datum <= '".$datum_vege."' AND ".$webjel."rendeles.teljesitve = 1 
							GROUP BY YEAR(".$webjel."rendeles.datum), MONTH(".$webjel."rendeles.datum)";
				foreach ($pdo->query($query) as $row)
				{
					// print $honapok[$row['honap']].' : nem akciós: '.$row['nemakcios_db'].' | akciós: '.$row['akcios_db'].'<br/>';
					$chart_honapok_akcios[] = $honapok[$row['honap']];
					$chart_ertekek_akcios[] = $row['akcios_db'];
					$chart_ertekek_noakcios[] = $row['nemakcios_db'];
				}
			?>
        var barChartCanvas = $("#barChart2").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = {
          labels: [
			  	<?php
					foreach($chart_honapok_akcios as $x)
					{
						print '"'.$x.'", ';
					}
				?>
		  ],
          datasets: [
            {
              label: "Electronics",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: [
			  	<?php
					foreach($chart_ertekek_akcios as $x)
					{
						print '"'.$x.'", ';
					}
				?>
			  ]
            },
            {
              label: "Digital Goods",
              fillColor: "rgba(60,141,188,0.9)",
              strokeColor: "rgba(60,141,188,0.8)",
              pointColor: "#3b8bba",
              pointStrokeColor: "rgba(60,141,188,1)",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(60,141,188,1)",
              data: [
			  	<?php
					foreach($chart_ertekek_noakcios as $x)
					{
						print '"'.$x.'", ';
					}
				?>
			  ]
            }
          ]
        };
        barChartData.datasets[1].fillColor = "#00C0EF";
        barChartData.datasets[1].strokeColor = "#00C0EF";
        barChartData.datasets[1].pointColor = "#00C0EF";
		/*
        barChartData.datasets[0].fillColor = "<?php print $szinek[3]; ?>";
        barChartData.datasets[0].strokeColor = "<?php print $szinek[3]; ?>";
        barChartData.datasets[0].pointColor = "<?php print $szinek[3]; ?>";
        barChartData.datasets[1].fillColor = "<?php print $szinek[5]; ?>";
        barChartData.datasets[1].strokeColor = "<?php print $szinek[5]; ?>";
        barChartData.datasets[1].pointColor = "<?php print $szinek[5]; ?>";
		*/
		barChart.Bar(barChartData, barChartOptions);
		// Chart vége
		
		// Datepicker
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			startDate: false,
			isRTL: false,
			autoclose:true,
			todayBtn: false,
			todayHighlight: true,
		});

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
          var clicks = $(this).data('clicks');
          if (clicks) {
            //Uncheck all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
          } else {
            //Check all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
          }
          $(this).data("clicks", !clicks);
        });

        //Handle starring for glyphicon and font awesome
        $(".mailbox-star").click(function (e) {
          e.preventDefault();
          //detect type
          var $this = $(this).find("a > i");
          var glyph = $this.hasClass("glyphicon");
          var fa = $this.hasClass("fa");

          //Switch states
          if (glyph) {
            $this.toggleClass("glyphicon-star");
            $this.toggleClass("glyphicon-star-empty");
          }

          if (fa) {
            $this.toggleClass("fa-star");
            $this.toggleClass("fa-star-o");
          }
        });
      });
    </script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
	<?php
		include 'module/body_end.php';
	?>
  </body>
</html>
