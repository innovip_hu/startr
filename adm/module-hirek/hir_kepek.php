<?php
	if (isset($_GET['script']))
	{
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	if(isset($_GET['torlendo_kep_id']))
	{
		// Képek törlése
		$query = "SELECT * FROM ".$webjel."hir_kepek WHERE id=".$_GET['torlendo_kep_id'];
		$res = $pdo->prepare($query);
		$res->execute();
		$row = $res -> fetch();
		$dir = $gyoker."/images/termekek/";
		unlink($dir.$row['kep']);
		// SQL
		$deletecommand = "DELETE FROM ".$webjel."hir_kepek WHERE id =".$_GET['torlendo_kep_id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}
	else if(isset($_GET['alap_kep_id']))
	{
		// Összes alap le
		$updatecommand = "UPDATE ".$webjel."hir_kepek SET alap=0 WHERE hir_id=".$_GET['id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
		// Alap
		$updatecommand = "UPDATE ".$webjel."hir_kepek SET alap=1 WHERE id=".$_GET['alap_kep_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}

	$query_kepek = "SELECT * FROM ".$webjel."hir_kepek WHERE hir_id=".$_GET['id']." ORDER BY id ASC ";
		foreach ($pdo->query($query_kepek) as $row_kepek)
		{
			print '<div class="row"><div class="col-md-4 col-sm-4">';
				print '<div class="kepek_termek_adatlap"><img src="../images/termekek/'.$row_kepek['kep'].'" />
				<img onClick="hirKepTorles('.$_GET['id'].', '.$row_kepek['id'].')" src="images/ikon_torles.png" class="kepek_termek_adatlap_torles" data-toggle="tooltip" title="Törlés" />';
				if($row_kepek['alap'] == 1) // Alap
				{
					print '<img src="images/ikon_alap_on.png" class="kepek_termek_adatlap_alap" style="cursor:default;"/>';
				}
				else
				{
					print '<img onClick="hirKepAlap('.$_GET['id'].', '.$row_kepek['id'].')" src="images/ikon_alap_off.png" class="kepek_termek_adatlap_alap" data-toggle="tooltip" title="Alap"/>';
				}
				print '</div>';
			print '</div>';
			print '<div class="col-md-6 col-sm-6">'.$domain.'/images/termekek/'.$row_kepek['kep'].'</div></div>';
		}
?>
