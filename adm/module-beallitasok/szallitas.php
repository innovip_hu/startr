<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		include '../config_adm.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	if(isset($_GET['ingyenes_szallitas']))
	{
		$updatecommand = "UPDATE ".$webjel."beallitasok SET ingyenes_szallitas=?, szall_kolts=?, szall_kolts_utanvet=?, szall_kolts_egyedi=?, szall_kolts_egyedi_utanvet=?, postapont=?, postapont_utanvet=?, gls_cspont=?, gls_cspont_utanvet=?, szallitas_afa=? WHERE id=1";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_GET['ingyenes_szallitas'],$_GET['szall_kolts'],$_GET['szall_kolts_utanvet'],$_GET['szall_kolts_egyedi'],$_GET['szall_kolts_egyedi_utanvet'],$_GET['postapont'],$_GET['postapont_utanvet'],$_GET['gls_cspont'],$_GET['gls_cspont_utanvet'],$_GET['szallitas_afa']));
	}
	
	$query = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
?>

	<div class="form-group">
		<label>Ingyenes szállítás <span style="font-size: 0.8em;">(Ha az érték 0, akkor nincs ingyenes szállítás)</span></label>
		<div class="input-group">
			<span class="input-group-addon input_jelolo_nsarga"><i class="fa fa-star"></i></span>
			<input type="text" class="form-control" id="ingyenes_szallitas" placeholder="Ingyenes szállítás" value="<?php print $row['ingyenes_szallitas']; ?>">
			<span class="input-group-addon">Ft</span>
		</div>
	</div>
	<div class="form-group">
		<label>Szállítási költség</label>
		<div class="input-group">
			<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
			<input type="text" class="form-control" id="szall_kolts" placeholder="Szállítási költség" value="<?php print $row['szall_kolts']; ?>">
			<span class="input-group-addon">Ft</span>
		</div>
	</div>
	<div class="form-group">
		<label>Szállítási költség utánvétel esetén</label>
		<div class="input-group">
			<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
			<input type="text" class="form-control" id="szall_kolts_utanvet" placeholder="Szállítási költség utánvétel esetén" value="<?php print $row['szall_kolts_utanvet']; ?>">
			<span class="input-group-addon">Ft</span>
		</div>
	</div>
	
	<!--Egyedi szállítás-->
	<?php
		if($conf_szall_egyedi == 1)
		{
			?>
				<div class="form-group">
					<label>Egyedi szállítási</label>
					<div class="input-group">
						<span class="input-group-addon input_jelolo_kek"><i class="fa fa-car"></i></span>
						<input type="text" class="form-control" id="szall_kolts_egyedi" placeholder="Egyedi szállítás" value="<?php print $row['szall_kolts_egyedi']; ?>">
						<span class="input-group-addon">Ft</span>
					</div>
				</div>
				<div class="form-group">
					<label>Egyedi szállítási utánvétel esetén</label>
					<div class="input-group">
						<span class="input-group-addon input_jelolo_kek"><i class="fa fa-car"></i></span>
						<input type="text" class="form-control" id="szall_kolts_egyedi_utanvet" placeholder="Egyedi szállítás utánvétel esetén" value="<?php print $row['szall_kolts_egyedi_utanvet']; ?>">
						<span class="input-group-addon">Ft</span>
					</div>
				</div>
			<?php
		}
		else
		{
			print '<input type="hidden" class="form-control" id="szall_kolts_egyedi" placeholder="Egyedi szállítás" value="'.$row['szall_kolts_egyedi'].'">';
			print '<input type="hidden" class="form-control" id="szall_kolts_egyedi_utanvet" placeholder="Egyedi szállítás" value="'.$row['szall_kolts_egyedi_utanvet'].'">';
		}
	?>
		
	<!--PostaPont szállítás-->
	<?php
		if($conf_szall_postapont == 1)
		{
			?>
				<div class="form-group">
					<label>PostaPont</label>
					<div class="input-group">
						<span class="input-group-addon input_jelolo_kek"><i class="fa fa-bus"></i></span>
						<input type="text" class="form-control" id="postapont" placeholder="PostaPont" value="<?php print $row['postapont']; ?>">
						<span class="input-group-addon">Ft</span>
					</div>
				</div>
				<div class="form-group">
					<label>PostaPont utánvétel esetén</label>
					<div class="input-group">
						<span class="input-group-addon input_jelolo_kek"><i class="fa fa-bus"></i></span>
						<input type="text" class="form-control" id="postapont_utanvet" placeholder="PostaPont utánvétel esetén" value="<?php print $row['postapont_utanvet']; ?>">
						<span class="input-group-addon">Ft</span>
					</div>
				</div>
			<?php
		}
		else
		{
			print '<input type="hidden" class="form-control" id="postapont" placeholder="Egyedi szállítás" value="'.$row['postapont'].'">';
			print '<input type="hidden" class="form-control" id="postapont_utanvet" placeholder="Egyedi szállítás" value="'.$row['postapont_utanvet'].'">';
		}
	?>
	
	<!--GLS CsomagPont szállítás-->
	<?php
		if($conf_szall_gls_cspont == 1)
		{
			?>
				<div class="form-group">
					<label>GLS CsomagPont</label>
					<div class="input-group">
						<span class="input-group-addon input_jelolo_kek"><i class="fa fa-plane"></i></span>
						<input type="text" class="form-control" id="gls_cspont" placeholder="GLS CsomagPont" value="<?php print $row['gls_cspont']; ?>">
						<span class="input-group-addon">Ft</span>
					</div>
				</div>
				<div class="form-group">
					<label>GLS CsomagPont utánvétel esetén</label>
					<div class="input-group">
						<span class="input-group-addon input_jelolo_kek"><i class="fa fa-plane"></i></span>
						<input type="text" class="form-control" id="gls_cspont_utanvet" placeholder="GLS CsomagPont utánvétel esetén" value="<?php print $row['gls_cspont_utanvet']; ?>">
						<span class="input-group-addon">Ft</span>
					</div>
				</div>
			<?php
		}
		else
		{
			print '<input type="hidden" class="form-control" id="gls_cspont" placeholder="Egyedi szállítás" value="'.$row['gls_cspont'].'">';
			print '<input type="hidden" class="form-control" id="gls_cspont_utanvet" placeholder="Egyedi szállítás" value="'.$row['gls_cspont_utanvet'].'">';
		}
	?>
	<div class="form-group">
		<label>Szállítási költség ÁFA</label>
		<div class="input-group">
			<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
			<input type="text" class="form-control" id="szallitas_afa" placeholder="Szállítási költség ÁFA" value="<?php print $row['szallitas_afa']; ?>">
			<span class="input-group-addon">%</span>
		</div>
	</div>
