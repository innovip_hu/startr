﻿<?php
	session_start();
	ob_start();
	
	include '../config.php';
	
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	if (isset($_POST['jelszo'])) // Belépés
	{
		
		$tipus = '';
		$username = addslashes($_POST['felhnev']);
		$userpw = md5($_POST['jelszo']);
		$query = "SELECT * FROM ".$webjel."users WHERE felhnev = '".$username."' AND jelszo = '".$userpw."'";
		$res = $pdo->prepare($query);
		$res->execute();
		$row  = $res -> fetch();
		$tipus = $row['tipus'];
		if ($tipus == 'admin') //Ha minden adat stimmel
		{
			$_SESSION['login_id'] = $row['id'];
			$_SESSION['login_nev'] = $row['felhnev'];
			$_SESSION['login_tipus'] = 'admin';
			//Kosár törlése
			$datum = date('Y-m-d');
			$datum = date('Y-m-d', strtotime($datum . ' - 2 day'));
			$sql = "DELETE FROM `".$webjel."kosar` WHERE `datum` < '".$datum."'";
			$stmt = $pdo->prepare($sql);
			$stmt->bindParam(':id', $_POST['id'], PDO::PARAM_INT);	
			$stmt->execute();
		}
		else
		{
			$uzenet = '<div id="note_c" class="alert alert-danger alert-dismissable">Hibás azonosító!</div>';
		}
	}
	else if (isset($_POST['command']) && $_POST['command'] == 'logout') // Kilépés
	{
		unset($_SESSION['login_id']);
		unset($_SESSION['login_nev']);
		unset($_SESSION['login_tipus']);
	}
	
	if(isset($_SESSION['login_id']))
	{
		include 'nyitooldal.php';
	}
	else
	{
		include 'login.php';
	}
?>
