<?php
	header("Access-Control-Allow-Origin: *"); 
	header('Content-type: application/json');
	
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsol�dni az adatb�zishoz!");
	}
	
	// require_once('simpleImage_class.php');
	$ds = DIRECTORY_SEPARATOR;
	// $dir= 'uploads/';   
	$dir = $gyoker.'/images/termekek/';
	
	// K�p m�sol�sa mapp�ba
	if (!empty($_FILES)) {
		/* $tempFile = $_FILES['file']['tmp_name'];                    
		$targetPath = $dir . $ds;
		$file = $_FILES['file']['name'];
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		$fn = 'hir2-'.time().rand(0,999999999999).'.'.$ext; // ez a j�
		$targetFile =  $targetPath. $fn; 
		move_uploaded_file($tempFile,$targetFile);
		$array = array("img"=>$targetPath.$fn);
		echo json_encode($array); */
		
		$image = @imagecreatefromstring(file_get_contents($_FILES['file']['tmp_name']));
		$exif = exif_read_data($_FILES['file']['tmp_name']);
		if(!empty($exif['Orientation'])) {
			switch($exif['Orientation']) {
				case 8:
					$image = imagerotate($image,90,0);
					break;
				case 3:
					$image = imagerotate($image,180,0);
					break;
				case 6:
					$image = imagerotate($image,-90,0);
					break;
			}
		}
		$tempFile = $_FILES['file']['tmp_name'];                   
		$targetPath = $dir . $ds;
		$file = $_FILES['file']['name'];
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		$fn = 'hir2-'.time().rand(0,999999999999).'.'.$ext; // ez a j�
		$targetFile =  $targetPath. $fn; 
		if($ext == 'png')
		{
			$black = imagecolorallocate($image, 0, 0, 0);
			imagecolortransparent($image, $black);
			imagepng($image, $targetFile);
		}
		else
		{
			imagejpeg($image, $targetFile);
		}
		imagedestroy($image);
		
		$array = array("img"=>$targetPath.$fn);
		echo json_encode($array);
		
		// Adatb�zidba t�lt�s
		$insertcommand = "INSERT INTO ".$webjel."hir2_kepek (hir_id,kep) VALUES (:hir_id,:kep)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':hir_id'=>$_GET['id'],
						  ':kep'=>$fn));
						  
	}
?>  