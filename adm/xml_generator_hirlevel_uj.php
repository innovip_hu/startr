﻿<?php
	
	$xml = NULL;
	include "../config.php";
	$query = "SHOW COLUMNS FROM ".$webjel."hirlevel WHERE lekerdezve=0";
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	$xml .= '<html>
			  <head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  </head>
			  <body>
				<div class="table-wrapper"><table class="sticky-enabled">
			 <thead><tr><th>Név</th><th>E-mail</th><th>Regisztráció dátuma</th><th>Korábban lekérdezve</th></tr></thead>
			<tbody>'; 	
	
	
	$query = "SELECT * FROM ".$webjel."hirlevel WHERE lekerdezve=0 ORDER BY id desc";
	$a = 0;
	foreach ($pdo->query($query) as $row)
	{
		if ($a == 0)
		{
			if ($row['lekerdezve'] == 0)
			{
				$lekerdezve = 'nem';
			}
			else
			{
				$lekerdezve = 'igen';
			}
			$xml .= '<tr class="odd"><td>'.$row['nev'].'</td><td>'.$row['email'].'</td><td>'.$row['datum'].'</td><td>'.$lekerdezve.'</td></tr>';
			$a = 1;
			$updatecommand = 'UPDATE '.$webjel.'hirlevel SET lekerdezve = 1 WHERE id = '.$row['id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
		}
		else
		{
			if ($row['lekerdezve'] == 0)
			{
				$lekerdezve = 'nem';
			}
			else
			{
				$lekerdezve = 'igen';
			}
			$xml .= '<tr class="odd"><td>'.$row['nev'].'</td><td>'.$row['email'].'</td><td>'.$row['datum'].'</td><td>'.$lekerdezve.'</td></tr>';
			$a = 0;
			$updatecommand = 'UPDATE '.$webjel.'hirlevel SET lekerdezve = 1 WHERE id = '.$row['id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
		}
		
	}
	
	
	header('content-type: application/xhtml+xml; charset=utf-8'); 
	header("Content-disposition: xml; filename=Hírlevél_regisztráció_újak_" . date("Y-m-d") . ".xls; size=".strlen($xml));
	echo $xml;
	exit;
	
?>