﻿<?php
	
	$xml = NULL;
	include "../config.php";
	$query = "SHOW COLUMNS FROM ".$webjel."hirlevel";
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	$xml .= '<html>
			  <head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  </head>
			  <body>
				<div class="table-wrapper"><table class="sticky-enabled">
			 <thead><tr>
			 <th>Név</th>
			 <th>Email</th>
			 <th>Telefon</th>
			 
			 <th>Város</th>
			 <th>Utca</th>
			 <th>Házszám</th>
			 <th>Irányítószám</th>
			 
			 <th>Szállítási Név</th>
			 <th>Szállítási Város</th>
			 <th>Szállítási Utca</th>
			 <th>Szállítási Házszám</th>
			 <th>Szállítási Irányítószám</th>
			 <th>Vásárlások száma</th>
			 <th>Vásárlások összege</th>
			 </tr></thead>
			<tbody>'; 	
	
	
	$vasarlok = $pdo->query(""
			. "SELECT * FROM ".$webjel."users  ORDER BY vezeteknev"
	)->fetchAll();
	foreach ($vasarlok as $row)
	{
		$xml .= '<tr class="odd">'
				. '<td>'.$row['vezeteknev'].'</td>'
				. '<td>'.$row['email'].'</td>'
				. '<td>'.$row['telefon'].'</td>'
				
				. '<td>'.$row['cim_varos'].'</td>'
				. '<td>'.$row['cim_utca'].'</td>'
				. '<td>'.$row['cim_hszam'].'</td>'
				. '<td>'.$row['cim_irszam'].'</td>'
				
				. '<td>'.$row['cim_szall_nev'].'</td>'
				. '<td>'.$row['cim_szall_varos'].'</td>'
				. '<td>'.$row['cim_szall_utca'].'</td>'
				. '<td>'.$row['cim_szall_hszam'].'</td>'
				. '<td>'.$row['cim_szall_irszam'].'</td>'
				. '<td>'.$pdo->query("SELECT COUNT(*) FROM {$webjel}rendeles WHERE user_id=$row[id] AND noreg=0")->fetchColumn().'</td>'
				// . '<td>'.$pdo->query("SELECT SUM(fizetendo) FROM {$webjel}rendeles WHERE user_id=$row[id] ")->fetchColumn().'</td>'
				. '<td>'.$pdo->query("SELECT SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar > 0, ".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db)) as rendelt_osszeg 
						FROM {$webjel}rendeles_tetelek 
						INNER JOIN ".$webjel."rendeles
						ON ".$webjel."rendeles_tetelek.rendeles_id = ".$webjel."rendeles.id
						WHERE ".$webjel."rendeles.user_id=$row[id] AND ".$webjel."rendeles.noreg=0")->fetchColumn().'</td>'
				. '</tr>';
	}
	
	
	header('content-type: application/xhtml+xml; charset=utf-8'); 
	header("Content-disposition: xml; filename=vasarlok_export_" . date("Y-m-d") . ".xls; size=".strlen($xml));
	echo $xml;
	exit;
	
?>