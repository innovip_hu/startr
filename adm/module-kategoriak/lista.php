<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	// Kategória törlése
	if(isset($_GET['torol_id']))
	{
		// Termékek
		$query = "SELECT * FROM ".$webjel."termekek WHERE csop_id=".$_GET['torol_id'];
		foreach ($pdo->query($query) as $row)
		{
			// Képek törlése
			$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id =".$row['id'];
			foreach ($pdo->query($query_kep) as $row_kep)
			{
				$dir = $gyoker."/images/termekek/";
				unlink($dir.$row_kep['kep']);
			}
			$deletecommand = "DELETE FROM ".$webjel."termek_kepek WHERE termek_id =".$row['id'];
			$result = $pdo->prepare($deletecommand);
			$result->execute();
			// Termék
			$deletecommand = 'DELETE FROM '.$webjel.'termekek WHERE id = '.$row['id'];
			$result = $pdo->prepare($deletecommand);
			$result->execute();
			// Jellemzők
			$pdo->exec("DELETE FROM ".$webjel."termek_termek_parameter_ertekek WHERE termek_id=".$row['id']."");
		}
		$pdo->exec("DELETE FROM ".$webjel."termek_termek_csoportok WHERE termek_csoport_id=".$_GET['torol_id']."");
		// Kategória képe
		if ($_GET['kep'] != "")
		{
			$dir = $gyoker."/images/termekek/";
			unlink($dir.$_POST['kep']);
		}
		// Kategória
		$deletecommand = "DELETE FROM ".$webjel."term_csoportok WHERE id =".$_GET['torol_id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}
	// Kategória sorrend FEL
	if(isset($_GET['sorrend_fel_id']))
	{
		$sorrend_minusz = $_GET['sorrend'] - 1;
		// Felette lévő módosítása
		$updatecommand = "UPDATE ".$webjel."term_csoportok SET sorrend=".$_GET['sorrend']." WHERE sorrend=".$sorrend_minusz;
		$result = $pdo->prepare($updatecommand);
		$result->execute();
		// Módosítandó módosítása
		$updatecommand = "UPDATE ".$webjel."term_csoportok SET sorrend=".$sorrend_minusz." WHERE id=".$_GET['sorrend_fel_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
		// Sorrend átfésülése
		$query = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id =".$_GET['csop_id']." ORDER BY sorrend ASC";
		$i = 1;
		foreach ($pdo->query($query) as $row)
		{
			$updatecommand = "UPDATE ".$webjel."term_csoportok SET sorrend=".$i." WHERE id=".$row['id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
			$i++;
		}
	}
	// Kategória sorrend LE
	if(isset($_GET['sorrend_le_id']))
	{
		$sorrend_minusz = $_GET['sorrend'] + 1;
		// Felette lévő módosítása
		$updatecommand = "UPDATE ".$webjel."term_csoportok SET sorrend=".$_GET['sorrend']." WHERE sorrend=".$sorrend_minusz;
		$result = $pdo->prepare($updatecommand);
		$result->execute();
		// Módosítandó módosítása
		$updatecommand = "UPDATE ".$webjel."term_csoportok SET sorrend=".$sorrend_minusz." WHERE id=".$_GET['sorrend_le_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
		// Sorrend átfésülése
		$query = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id =".$_GET['csop_id']." ORDER BY sorrend ASC";
		$i = 1;
		foreach ($pdo->query($query) as $row)
		{
			$updatecommand = "UPDATE ".$webjel."term_csoportok SET sorrend=".$i." WHERE id=".$row['id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
			$i++;
		}
	}
	// Új kategória - ajaxos
	if(isset($_GET['command']) && $_GET['command'] == 'uj_kategoria')
	{
		/* 
		// Sorrend átfésülése
		$query = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id =".$_GET['csop_id']." ORDER BY sorrend ASC";
		$i = 1;
		foreach ($pdo->query($query) as $row)
		{
			$updatecommand = "UPDATE ".$webjel."term_csoportok SET sorrend=".$i." WHERE id=".$row['id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
			$i++;
		}
		// Sorszám meghatározása
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id=".$_GET['csop_id']);
		$res->execute();
		$rownum = $res->fetchColumn();
		$rownum++;
		
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlnev.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$nev_url = $nev_url.'-'.rand(1,99999);
		}
		
		if($_POST['lathato'] == 'true') { $lathato = 0; } else { $lathato = 1; }
		$insertcommand = "INSERT INTO ".$webjel."term_csoportok (csop_id, nev, leiras, lathato, sorrend, nev_url) VALUES (:csop_id, :nev, :leiras, :lathato, :sorrend, :nev_url)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':csop_id'=>$_GET['csop_id'],
						  ':nev'=>$_POST['nev'],
						  ':leiras'=>$_POST['leiras'],
						  ':sorrend'=>$rownum,
						  ':nev_url'=>$nev_url,
						  ':lathato'=>$lathato));
		 */				  
	}
	// Új kategória 2
	if(isset($_POST['command']) && $_POST['command'] == 'uj_kategoria_mentese')
	{
		// Sorrend átfésülése
		$query = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id =".$_POST['csop_id']." ORDER BY sorrend ASC";
		$i = 1;
		foreach ($pdo->query($query) as $row)
		{
			$updatecommand = "UPDATE ".$webjel."term_csoportok SET sorrend=".$i." WHERE id=".$row['id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
			$i++;
		}
		// Sorszám meghatározása
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id=".$_POST['csop_id']);
		$res->execute();
		$rownum = $res->fetchColumn();
		$rownum++;
		
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlnev.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$nev_url = $nev_url.'-'.rand(1,99999);
		}
		
		if(isset($_POST['lathato']) && $_POST['lathato'] == 'ok') { $lathato = 0; } else { $lathato = 1; }
		$insertcommand = "INSERT INTO ".$webjel."term_csoportok (csop_id, nev, leiras, lathato, sorrend, nev_url, seo_title, seo_description) VALUES (:csop_id, :nev, :leiras, :lathato, :sorrend, :nev_url, :seo_title, :seo_description)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':csop_id'=>$_POST['csop_id'],
						  ':nev'=>$_POST['nev'],
						  ':leiras'=>$_POST['leiras'],
						  ':sorrend'=>$rownum,
						  ':nev_url'=>$nev_url,
						  ':seo_title'=>$_POST['seo_title'],
						  ':seo_description'=>$_POST['seo_description'],
						  ':lathato'=>$lathato));
		// KÉP
		if ($_FILES['kep']['name'] != '')
		{
			if (strstr($_FILES['kep']['name'], ' ') != true)
			{
				$id =$pdo->lastInsertId();
				
				// Kép kiterjesztése
				$path = $_FILES['kep']['name'];
				$kiterj = '.'.pathinfo($path, PATHINFO_EXTENSION);
				
				$file_size=$_FILES['kep']['size'];
				$limit_size=204800;
				if($file_size >= $limit_size)
				{
					
					$dir = $gyoker.'/images/termekek/';
					list($k_width, $k_height, $k_type, $k_attr) = getimagesize($_FILES['kep']['tmp_name']);
					
					include('SimpleImage.php');
					$image = new SimpleImage();
					$image->load($_FILES['kep']['tmp_name']);
					if ($k_width >= $k_height) //fekvő vagy négyzet
					{
						$image->resizeToWidth(800);
					}
					else //álló
					{
						$image->resizeToHeight(600);
					}
					$image->save($dir.$id.'csop_'.$nev_url.$kiterj);
				}
				else
				{
					$dir = $gyoker.'/images/termekek/';
					move_uploaded_file($_FILES['kep']['tmp_name'], $dir.$id.'csop_'.$nev_url.$kiterj);
				}
				$updatecommand = "UPDATE ".$webjel."term_csoportok SET kep = '".$id.'csop_'.$nev_url.$kiterj."' WHERE id = ".$id ;
				$result = $pdo->prepare($updatecommand);
				$result->execute();
			}
		}
	}
?>
<!--Modal-->
<div class="example-modal">
	<div id="rakerdez_torles" class="modal modal-danger">
	  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Törlés</h4>
			</div>
			<div class="modal-body">
				<p>Biztos törölni szeretnéd a(az) <span id="modal_torles_nev"></span> kategóriát?</p>
			</div>
			<div class="modal-footer">
				<input type="hidden" id="modal_torles_id" value="" />
				<input type="hidden" id="modal_torles_kep" value="" />
				<button onClick="torol()" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
				<button onClick="megsem('rakerdez_torles')" type="button" class="btn btn-outline">Mégsem</button>
			</div>
		</div>
	  </div>
	</div>
</div>

<div class="content-wrapper">
	<section class="content-header">
	  <h1 id="myModal">Kategóriák</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Kategóriák</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<span class="menu_gomb"><a onClick="ujKategoria(0, 'Főkategória')"><i class="fa fa-plus"></i> Új Főkategória</span></a>
					</div>
					<div class="box-body kategroia_kezelo">
						<ul class="tree">
							<?php
								$query = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id=0 ORDER BY sorrend ASC";
								foreach ($pdo->query($query) as $row)
								{
									if($row['lathato'] == 1) // nem látható
									{
										print '<li><span style="text-decoration: line-through;">'.$row['nev'].'</span>';
									}
									else
									{
										print '<li>'.$row['nev'];
									}
										?>
										<div class="btn-group">
											<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<li><a onClick="ujKategoria(<?php print $row['id']; ?>, '<?php print $row['nev']; ?>')"><i class="fa fa-plus"></i>Új alkategória</a></li>
												<li><a onClick="kategoriaMOD(<?php print $row['id']; ?>)"><i class="fa fa-pencil"></i>Módosítás</a></li>
												<li><a onClick="rakerdez(<?php print $row['id']; ?>, '<?php print $row['nev']; ?>', '<?php print $row['kep']; ?>')"><i class="fa fa-trash"></i>Törlés</a></li>
												<li><a onClick="egySzinttelFeljebb(<?php print $row['id']; ?>, <?php print $row['csop_id']; ?>, <?php print $row['sorrend']; ?>)"><i class="fa fa-chevron-up"></i>Egy szinttel feljebb</a></li>
												<li><a onClick="egySzinttelLejjebb(<?php print $row['id']; ?>, <?php print $row['csop_id']; ?>, <?php print $row['sorrend']; ?>)"><i class="fa fa-chevron-down"></i>Egy szinttel lejjebb</a></li>
											</ul>
										</div>
										<?php
										$query2 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id=".$row['id']." ORDER BY sorrend ASC";
										$szaml_2 = 0;
										foreach ($pdo->query($query2) as $row2)
										{
											$szaml_2 ++;
											if($szaml_2 == 1) { print '<ul>'; }
											print '<li>'.$row2['nev'];
												?>
												<div class="btn-group">
													<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
														<span class="caret"></span>
													</button>
													<ul class="dropdown-menu">
														<li><a onClick="ujKategoria(<?php print $row2['id']; ?>, '<?php print $row2['nev']; ?>')"><i class="fa fa-plus"></i>Új alkategória</a></li>
														<li><a onClick="kategoriaMOD(<?php print $row2['id']; ?>)"><i class="fa fa-pencil"></i>Módosítás</a></li>
														<li><a onClick="rakerdez(<?php print $row2['id']; ?>, '<?php print $row2['nev']; ?>', '<?php print $row2['kep']; ?>')"><i class="fa fa-trash"></i>Törlés</a></li>
														<li><a onClick="egySzinttelFeljebb(<?php print $row2['id']; ?>, <?php print $row2['csop_id']; ?>, <?php print $row2['sorrend']; ?>)"><i class="fa fa-chevron-up"></i>Egy szinttel feljebb</a></li>
														<li><a onClick="egySzinttelLejjebb(<?php print $row2['id']; ?>, <?php print $row2['csop_id']; ?>, <?php print $row2['sorrend']; ?>)"><i class="fa fa-chevron-down"></i>Egy szinttel lejjebb</a></li>
													</ul>
												</div>
												<?php
												$query3 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id=".$row2['id']." ORDER BY sorrend ASC";
												$szaml_3 = 0;
												foreach ($pdo->query($query3) as $row3)
												{
													$szaml_3 ++;
													if($szaml_3 == 1) { print '<ul>'; }
													print '<li>'.$row3['nev'];
													?>
													<div class="btn-group">
														<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
															<span class="caret"></span>
														</button>
														<ul class="dropdown-menu">
															<li><a onClick="kategoriaMOD(<?php print $row3['id']; ?>)"><i class="fa fa-pencil"></i>Módosítás</a></li>
															<li><a onClick="rakerdez(<?php print $row3['id']; ?>, '<?php print $row3['nev']; ?>', '<?php print $row3['kep']; ?>')"><i class="fa fa-trash"></i>Törlés</a></li>
															<li><a onClick="egySzinttelFeljebb(<?php print $row3['id']; ?>, <?php print $row3['csop_id']; ?>, <?php print $row3['sorrend']; ?>)"><i class="fa fa-chevron-up"></i>Egy szinttel feljebb</a></li>
															<li><a onClick="egySzinttelLejjebb(<?php print $row3['id']; ?>, <?php print $row3['csop_id']; ?>, <?php print $row3['sorrend']; ?>)"><i class="fa fa-chevron-down"></i>Egy szinttel lejjebb</a></li>
														</ul>
													</div>
													<?php
													print '</li>';
												}
												if($szaml_3 > 0) { print '</ul>'; }
											print '</li>';
										}
										if($szaml_2 > 0) { print '</ul>'; }
									print '</li>';
								}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

