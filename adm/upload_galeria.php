<?php
	header("Access-Control-Allow-Origin: *"); 
	header('Content-type: application/json');
	
	include '../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsol�dni az adatb�zishoz!");
	}
	
	// require_once('simpleImage_class.php');
	$ds = DIRECTORY_SEPARATOR;
	// $dir= 'uploads/';   
	$dir = $gyoker.'/images/galeria/';
	
	// K�p m�sol�sa mapp�ba
	if (!empty($_FILES)) {
		$tempFile = $_FILES['file']['tmp_name'];                    
		$targetPath = $dir . $ds;
		$file = $_FILES['file']['name'];
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		$fn = 'img-'.time().rand(0,999999999999).'.'.$ext; // ez a j�
		$targetFile =  $targetPath. $fn; 
		move_uploaded_file($tempFile,$targetFile);
		/*
		if(move_uploaded_file($tempFile,$targetFile))
		{
			$image = new SimpleImage();
			$image->load($targetPath. $fn);
			$image->resizeToWidth(200);
			// $image->save($targetPath.'thumb-'.$fn);	
		}
		*/
		// $array = array("img"=>$targetPath.$fn,"thumb"=>$targetPath.'thumb-'.$fn);
		$array = array("img"=>$targetPath.$fn);
		echo json_encode($array);
		
		// Adatb�zidba t�lt�s
		$insertcommand = "INSERT INTO ".$webjel."galeria_kepek (galeria_id,kep) VALUES (:galeria_id,:kep)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':galeria_id'=>$_GET['id'],
						  ':kep'=>$fn));
	}
?>  