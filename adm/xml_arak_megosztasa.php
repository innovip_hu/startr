<?php
	$datum = date("Y-m-d");
	
	$query_beall = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
	$res = $pdo->prepare($query_beall);
	$res->execute();
	$row_beall = $res -> fetch();

// ÁRUKERESŐ
	$xml = new DOMDocument('1.0', 'UTF-8');
	
	$root = $xml->createElement("products");
	$xml->appendChild($root);

	$query = "SELECT *, ".$webjel."afa.afa as afa_szaz 
					, ".$webjel."termekek.id as id 
					, ".$webjel."termekek.leiras as leiras 
					, ".$webjel."termekek.nev as nev 
					, ".$webjel."termekek.kep as kep 
					, ".$webjel."termekek.nev_url as nev_url 
					, ".$webjel."term_csoportok.nev as csop_nev 
					, ".$webjel."term_csoportok.csop_id as csop_id 
					, ".$webjel."term_csoportok.nev_url as kat_nev_url 
	FROM ".$webjel."termekek
	INNER JOIN ".$webjel."afa 
	ON ".$webjel."termekek.afa=".$webjel."afa.id 
	INNER JOIN ".$webjel."term_csoportok 
	ON ".$webjel."termekek.csop_id=".$webjel."term_csoportok.id 
	WHERE ".$webjel."termekek.lathato=0";
	foreach ($pdo->query($query) as $row)
	{
		if($row['csop_id'] > 0)
		{
			$query_szulkat = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row['csop_id'];
			$res = $pdo->prepare($query_szulkat);
			$res->execute();
			$row_szulkat  = $res -> fetch();
			$kategoria = $row_szulkat['nev'].' > '.$row['csop_nev'];
		}
		else
		{
			$kategoria = $row['csop_nev'];
		}
		
			$id = $xml->createElement("sku");
			$idText = $xml->createTextNode($row['id']);
			$id->appendChild($idText);
		
			// $delivery_time = $xml->createElement("delivery_time");
			// $delivery_timeText = $xml->createTextNode($row['aruk_szall_ido']);
			// $delivery_time->appendChild($delivery_timeText);

			// $delivery_cost = $xml->createElement("delivery_cost");
			// $delivery_costText = $xml->createTextNode($row_beall['szall_kolts_utanvet']);
			// $delivery_cost->appendChild($delivery_costText);

			// $manufacturer = $xml->createElement("manufacturer");
			// $manufacturerText = $xml->createTextNode($row['gyarto']);
			// $manufacturer->appendChild($manufacturerText);

			$name = $xml->createElement("name");
			$nameText = $xml->createTextNode($row['nev']);
			$name->appendChild($nameText);

			$category = $xml->createElement("category");
			$categoryText = $xml->createTextNode($kategoria);
			$category->appendChild($categoryText);

			$datum = date("Y-m-d");
			if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) // Ha akciós
			{
				$ws_ar = $row['akciosar'];
			}
			else
			{
				$ws_ar = $row['ar'];
			}
			$price = $xml->createElement("price");
			$priceText = $xml->createTextNode($ws_ar);
			$price->appendChild($priceText);

			$net_price = $xml->createElement("net_price");
			$net_priceText = $xml->createTextNode(round($ws_ar/(1+($row['afa_szaz']/100))));
			$net_price->appendChild($net_priceText);

			$identifier = $xml->createElement("identifier");
			$identifierText = $xml->createTextNode($row['id']);
			$identifier->appendChild($identifierText);

			// $warranty = $xml->createElement("warranty");
			// $warrantyText = $xml->createTextNode($row['aruk_garancia']);
			// $warranty->appendChild($warrantyText);

			$product_url = $xml->createElement("product_url");
			$product_urlText = $xml->createTextNode($domain.'/termekek/'.$row['kat_nev_url'].'/'.$row['nev_url']);
			$product_url->appendChild($product_urlText);
			
			if ($row['kep'] != '')
			{
				$image_url = $xml->createElement("image_url");
				$image_urlText = $xml->createTextNode($domain.'/images/termekek/'.$row['kep']);
				$image_url->appendChild($image_urlText);
			}

			$product = $xml->createElement("product");
			$product->appendChild($id);
			// $product->appendChild($manufacturer);
			$product->appendChild($category);
			$product->appendChild($name);
			// $product->appendChild($delivery_time);
			// $product->appendChild($delivery_cost);
			$product->appendChild($price);
			$product->appendChild($net_price);
			$product->appendChild($identifier);
			// $product->appendChild($warranty);
			$product->appendChild($product_url);
			if ($row['kep'] != '')
			{
				$product->appendChild($image_url);
			}

		$root->appendChild($product);
	}


	$xml->save("xml_arukereso.xml");

// Olcsóbbat.hu
	$xml = new DOMDocument('1.0', 'UTF-8');
	
	$root = $xml->createElement("products");
	$xml->appendChild($root);

	foreach ($pdo->query($query) as $row)
	{
		if($row['csop_id'] > 0)
		{
			$query_szulkat = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row['csop_id'];
			$res = $pdo->prepare($query_szulkat);
			$res->execute();
			$row_szulkat  = $res -> fetch();
			$kategoria = $row_szulkat['nev'].' > '.$row['csop_nev'];
		}
		else
		{
			$kategoria = $row['csop_nev'];
		}
		
		$itemid = $xml->createElement("itemid");
		$itemidText = $xml->createTextNode($row['id']);
		$itemid->appendChild($itemidText);
	
		$id = $xml->createElement("id");
		$idText = $xml->createTextNode($row['id']);
		$id->appendChild($idText);

		$name = $xml->createElement("name");
		$nameText = $xml->createTextNode($row['nev']);
		$name->appendChild($nameText);

		$category = $xml->createElement("category");
		$categoryText = $xml->createTextNode($kategoria);
		$category->appendChild($categoryText);

		$datum = date("Y-m-d");
		if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) // Ha akciós
		{
			$ws_ar = $row['akciosar'];
		}
		else
		{
			$ws_ar = $row['ar'];
		}
		$price = $xml->createElement("grossprice");
		$priceText = $xml->createTextNode($ws_ar);
		$price->appendChild($priceText);

		if ($row['raktaron'] >0) // Ha raktáron van
		{
			$raktaron = 'true';
		}
		else
		{
			$raktaron = 'false';
		}
	
		// $delivery_time = $xml->createElement("deliverytime");
		// $delivery_timeText = $xml->createTextNode($row['aruk_szall_ido']);
		// $delivery_time->appendChild($delivery_timeText);

		// $manufacturer = $xml->createElement("manufacturer");
		// $manufacturerText = $xml->createTextNode($row['gyarto']);
		// $manufacturer->appendChild($manufacturerText);
		
		// $stock = $xml->createElement("stock");
		// $stockText = $xml->createTextNode($raktaron);
		// $stock->appendChild($stockText);

		$product_url = $xml->createElement("urlsite");
		$product_urlText = $xml->createTextNode($domain.'/termekek/'.$row['nev_url'].'/'.$row['nev_url']);
		$product_url->appendChild($product_urlText);
		
		if ($row['kep'] != '')
		{
			$image_url = $xml->createElement("urlpicture");
			$image_urlText = $xml->createTextNode($domain.'/images/termekek/'.$row['kep']);
			$image_url->appendChild($image_urlText);
		}

		$product = $xml->createElement("product");
		$product->appendChild($id);
		$product->appendChild($itemid);
		$product->appendChild($category);
		$product->appendChild($name);
		// $product->appendChild($manufacturer);
		// $product->appendChild($delivery_time);
		// $product->appendChild($stock);
		
		$product->appendChild($price);
		$product->appendChild($product_url);
		if ($row['kep'] != '')
		{
			$product->appendChild($image_url);
		}

		$root->appendChild($product);
	}


	$xml->save("xml_olcsobbat.xml");
	
// ÁRGÉP

	$xml = new DOMDocument('1.0', 'UTF-8');
	
	$root = $xml->createElement("termeklista");
	$xml->appendChild($root);

	foreach ($pdo->query($query) as $row)
	{
			$nev = $xml->createElement("nev");
			$nevText = $xml->createCDATASection($row['nev']);
			$nev->appendChild($nevText);

			$leiras = $xml->createElement("leiras");
			$leirasText = $xml->createCDATASection($row['leiras']);
			$leiras->appendChild($leirasText);

			// $ido = $xml->createElement("ido");
			// $idoText = $xml->createCDATASection($row['aruk_szall_ido']);
			// $ido->appendChild($idoText);

			$datum = date("Y-m-d");
			if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) // Ha akciós
			{
				$ws_ar = $row['akciosar'];
			}
			else
			{
				$ws_ar = $row['ar'];
			}
			$ar = $xml->createElement("ar");
			$arText = $xml->createCDATASection($ws_ar);
			$ar->appendChild($arText);

			$termeklink = $xml->createElement("termeklink");
			$termeklinkText = $xml->createCDATASection($domain.'/termekek/'.$row['nev_url'].'/'.$row['nev_url']);
			$termeklink->appendChild($termeklinkText);
			
			if ($row['kep'] != '')
			{
				$fotolink = $xml->createElement("fotolink");
				$fotolinkText = $xml->createCDATASection($domain.'/images/termekek/'.$row['kep']);
				$fotolink->appendChild($fotolinkText);
			}

			$product = $xml->createElement("termek");
			$product->appendChild($nev);
			$product->appendChild($ar);
			$product->appendChild($termeklink);
			$product->appendChild($leiras);
			// $product->appendChild($ido);
			if ($row['kep'] != '')
			{
				$product->appendChild($fotolink);
			}

		$root->appendChild($product);
	}

	$xml->save("xml_argep.xml");

?>