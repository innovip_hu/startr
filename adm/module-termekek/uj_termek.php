<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE csop_id=".$_GET['csop_id']);
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'vezeteknev'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'ASC'; // Alap rendezési feltétel
	}
	// Adatok
	$query = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$_GET['csop_id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	$kat_nev = $row['nev'];
?>
		  
<div class="content-wrapper">
	<section class="content-header">
	  <h1>Új termék </small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a onClick="visszaTermekek('<?php print $_GET['fajl']; ?>', '<?php print $_GET['csop_id']; ?>')"><i class="fa fa-folder-open"></i> <?php print $kat_nev; ?></a></li>
		<li class="active">Új termék</li>
	  </ol>
	</section>

	<section class="content">
		<!-- Termék adatok -->
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Termék adatok</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Megnevezés</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="nev" placeholder="Megnevezés" value="">
									</div>
								</div>
								<div class="form-group">
									<label>Ár</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="ar" placeholder="Ár" value="" >
										<span class="input-group-addon">Ft</span>
									</div>
								</div>
								<div class="form-group">
									<label>ÁFA</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<select class="form-control" id="afa">
											<?php
												$query2 = "SELECT * FROM ".$webjel."afa ORDER BY id asc";
												foreach ($pdo->query($query2) as $row2)
												{
													print '<option value="'.$row2['id'].'">'.$row2['afa'].'</option>';
												}
											?>
										 </select>
										<span class="input-group-addon">%</span>
									</div>
								</div>
								<div class="form-group">
									<label>Akciós ár</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-star-o"></i></span>
										<input type="text" class="form-control" id="akciosar" placeholder="Akciós ár" value="" >
										<span class="input-group-addon">Ft</span>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>Akció kezdete</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control datepicker" id="akcio_tol" placeholder="Akció kezdete" value="" >
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Akció vége</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control datepicker" id="akcio_ig" placeholder="Akció vége" value="" >
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Rövid leírás</label>
									<textarea class="form-control" id="rovid_leiras" rows="3" placeholder="Rövid leírás"></textarea>
								</div>
								<div class="form-group">
									<label>SEO title</label>
									<div class="input-group">
										<input type="text" class="form-control" id="seo_title" placeholder="SEO title" value="" >
									</div>
								</div>
								<div class="form-group">
									<label>SEO description</label>
									<textarea class="form-control" id="seo_description" rows="3" placeholder="SEO description"></textarea>
								</div>
								<div class="row margtop10">
									<div class="col-sm-6">
										<div class="checkbox ">
											<label>
												<input type="checkbox" class="minimal" id="lathato" checked/> Látható
											</label>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="checkbox">
											<label>
												<input type="checkbox" class="minimal" id="kiemelt" /> Kiemelt
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Leírás</label>
									<textarea id="editor1" name="editor1" rows="10" cols="80"></textarea>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer">
						<button type="submit" onClick="mentesUjTermek('<?php print $_GET['fajl']; ?>', '<?php print $_GET['csop_id']; ?>')" class="btn btn-primary">Mentés</button>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<input type="hidden" name="oldalszam" id="oldalszam" value="<? print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<? print $kezd;?>"/>
<input type="hidden" name="kezd" id="oldalszam" value="<? print $oldalszam;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<? print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<? print $sorrend;?>"/>
