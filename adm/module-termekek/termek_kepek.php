<?php
	if (isset($_GET['script']))
	{
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	if(isset($_GET['torlendo_kep_id']))
	{
		// Képek törlése
		$query = "SELECT * FROM ".$webjel."termek_kepek WHERE id=".$_GET['torlendo_kep_id'];
		$res = $pdo->prepare($query);
		$res->execute();
		$row = $res -> fetch();
		$dir = $gyoker."/images/termekek/";
		unlink($dir.$row['kep']);
		// SQL
		$deletecommand = "DELETE FROM ".$webjel."termek_kepek WHERE id =".$_GET['torlendo_kep_id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}
	else if(isset($_GET['alap_kep_id']))
	{
		// Összes alap le
		$updatecommand = "UPDATE ".$webjel."termek_kepek SET alap=0 WHERE termek_id=".$_GET['id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
		// Alap
		$updatecommand = "UPDATE ".$webjel."termek_kepek SET alap=1 WHERE id=".$_GET['alap_kep_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}
	
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termek_kepek WHERE termek_id=".$_GET['id']);
	$res->execute();
	$rownum = $res->fetchColumn();
	$kep_1_oszlopban = floor($rownum / 3);
	$maradek = $rownum - ($kep_1_oszlopban * 3);
	
	$oszlop_1_ig = $kep_1_oszlopban;
	if($maradek > 0) { $oszlop_1_ig++; }

	$oszlop_2_tol = $oszlop_1_ig;
	$oszlop_2_ig = $kep_1_oszlopban;
	if($maradek > 1) { $oszlop_2_ig++; }

	$oszlop_3_tol = $kep_1_oszlopban * 2;
	if($maradek > 0) { $oszlop_3_tol = $kep_1_oszlopban + $oszlop_2_ig + 1; }
	$oszlop_3_ig = $kep_1_oszlopban;
	
	// 1. oszlop
	print '<div class="col-md-4 col-sm-4">';
		$query_kepek = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$_GET['id']." ORDER BY id ASC LIMIT 0, ".$oszlop_1_ig;
		foreach ($pdo->query($query_kepek) as $row_kepek)
		{
			print '<div class="kepek_termek_adatlap"><img src="../images/termekek/'.$row_kepek['kep'].'" />
			<img onClick="termekKepTorles('.$_GET['id'].', '.$row_kepek['id'].')" src="images/ikon_torles.png" class="kepek_termek_adatlap_torles" data-toggle="tooltip" title="Törlés" />';
			if($row_kepek['alap'] == 1) // Alap
			{
				print '<img src="images/ikon_alap_on.png" class="kepek_termek_adatlap_alap" style="cursor:default;"/>';
			}
			else
			{
				print '<img onClick="termekKepAlap('.$_GET['id'].', '.$row_kepek['id'].')" src="images/ikon_alap_off.png" class="kepek_termek_adatlap_alap" data-toggle="tooltip" title="Alap"/>';
			}
			print '</div>';
		}
	print '</div>';
	// 2. oszlop
	print '<div class="col-md-4 col-sm-4">';
		$query_kepek = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$_GET['id']." ORDER BY id ASC LIMIT ".$oszlop_2_tol.", ".$oszlop_2_ig;
		foreach ($pdo->query($query_kepek) as $row_kepek)
		{
			print '<div class="kepek_termek_adatlap"><img src="../images/termekek/'.$row_kepek['kep'].'" />
			<img onClick="termekKepTorles('.$_GET['id'].', '.$row_kepek['id'].')" src="images/ikon_torles.png" class="kepek_termek_adatlap_torles" data-toggle="tooltip" title="Törlés" />';
			if($row_kepek['alap'] == 1) // Alap
			{
				print '<img src="images/ikon_alap_on.png" class="kepek_termek_adatlap_alap" style="cursor:default;"/>';
			}
			else
			{
				print '<img onClick="termekKepAlap('.$_GET['id'].', '.$row_kepek['id'].')" src="images/ikon_alap_off.png" class="kepek_termek_adatlap_alap" data-toggle="tooltip" title="Alap"/>';
			}
			print '</div>';
		}
	print '</div>';
	// 3. oszlop
	print '<div class="col-md-4 col-sm-4">';
		$query_kepek = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$_GET['id']." ORDER BY id ASC LIMIT ".$oszlop_3_tol.", ".$oszlop_3_ig;
		foreach ($pdo->query($query_kepek) as $row_kepek)
		{
			print '<div class="kepek_termek_adatlap"><img src="../images/termekek/'.$row_kepek['kep'].'" />
			<img onClick="termekKepTorles('.$_GET['id'].', '.$row_kepek['id'].')" src="images/ikon_torles.png" class="kepek_termek_adatlap_torles" data-toggle="tooltip" title="Törlés" />';
			if($row_kepek['alap'] == 1) // Alap
			{
				print '<img src="images/ikon_alap_on.png" class="kepek_termek_adatlap_alap" style="cursor:default;"/>';
			}
			else
			{
				print '<img onClick="termekKepAlap('.$_GET['id'].', '.$row_kepek['id'].')" src="images/ikon_alap_off.png" class="kepek_termek_adatlap_alap" data-toggle="tooltip" title="Alap"/>';
			}
			print '</div>';
		}
	print '</div>';
?>
