<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
		
		include '../config_adm.php';
	}
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE csop_id=".$_GET['csop_id']);
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'vezeteknev'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'ASC'; // Alap rendezési feltétel
	}
	
	// Új termék mentése
	if(isset($_POST['command']) && $_POST['command'] == 'uj_termek')
	{
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlnev.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$query = "SELECT * FROM ".$webjel."termekek WHERE nev_url = '".$nev_url."'";
			$res = $pdo->prepare($query);
			$res->execute();
			$row = $res -> fetch();
			if($rownum2 > 1 || $row['id'] != $_GET['id']) // Ha nem saját maga
			{
				$nev_url = $nev_url.'-'.time().rand(1, 999);
			}
		}
		// Mentés
		if($_POST['lathato'] == 'true') { $lathato = 0; } else { $lathato = 1; }
		if($_POST['kiemelt'] == 'true') { $kiemelt = 1; } else { $kiemelt = 0; }
		$updatecommand = "UPDATE ".$webjel."termekek SET nev=?, ar=?, afa=?, akciosar=?, akcio_tol=?, akcio_ig=?, rovid_leiras=?, lathato=?, kiemelt=?, leiras=?, nev_url=?, seo_title=?, seo_description=?  WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_POST['nev'], $_POST['ar'], $_POST['afa'], $_POST['akciosar'], $_POST['akcio_tol'], $_POST['akcio_ig'], $_POST['rovid_leiras'], $lathato, $kiemelt, $_POST['leiras'], $nev_url, $_POST['seo_title'], $_POST['seo_description'],  $_GET['id']));
	}
	// Adatok mentése
	else if(isset($_POST['command']) && $_POST['command'] == 'adaok_mentese')
	{
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlnev.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$query = "SELECT * FROM ".$webjel."termekek WHERE nev_url = '".$nev_url."'";
			$res = $pdo->prepare($query);
			$res->execute();
			$row = $res -> fetch();
			if($rownum2 > 1 || $row['id'] != $_GET['id']) // Ha nem saját maga
			{
				$nev_url = $nev_url.'-'.$_GET['id'];
			}
		}
		// Mentés
		if($_POST['lathato'] == 'true') { $lathato = 0; } else { $lathato = 1; }
		if($_POST['kiemelt'] == 'true') { $kiemelt = 1; } else { $kiemelt = 0; }
		$updatecommand = "UPDATE ".$webjel."termekek SET nev=?, ar=?, afa=?, akciosar=?, akcio_tol=?, akcio_ig=?, rovid_leiras=?, lathato=?, kiemelt=?, leiras=?, csop_id=?, nev_url=?, seo_title=?, seo_description=? WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_POST['nev'], $_POST['ar'], $_POST['afa'], $_POST['akciosar'], $_POST['akcio_tol'], $_POST['akcio_ig'], $_POST['rovid_leiras'], $lathato, $kiemelt, $_POST['leiras'], $_POST['csop_id'], $nev_url, $_POST['seo_title'], $_POST['seo_description'], $_GET['id']));
	}
	// Klónozás
	else if(isset($_GET['command']) && $_GET['command'] == 'klonozas')
	{
		// Klón létrehozása
		$insertcommand = "INSERT INTO ".$webjel."termekek
		(csop_id, nev, nev_url, vonalkod, ar, afa, kep, leiras, lathato, akciosar, akcio_tol, akcio_ig, sorrend, raktaron, kod, yt_video, rovid_leiras, kiemelt, gyarto, garancia, cikkszam, seo_title, seo_description)
		SELECT csop_id, nev, nev_url, vonalkod, ar, afa, kep, leiras, lathato, akciosar, akcio_tol, akcio_ig, sorrend, raktaron, kod, yt_video, rovid_leiras, kiemelt, gyarto, garancia, cikkszam, seo_title, nev_url
		FROM ".$webjel."termekek
		WHERE id = ".$_GET['id'];
		$result = $pdo->prepare($insertcommand);
		$result->execute();
		$uj_id = $pdo->lastInsertId();
		// Név és url név átírása
		$updatecommand = "UPDATE ".$webjel."termekek SET nev=CONCAT(nev, ' (klón)'), nev_url=CONCAT(nev_url, '_".$uj_id."') WHERE id=".$uj_id;
		$result = $pdo->prepare($updatecommand);
		$result->execute();
		// Paraméterek
		$query = "SELECT * FROM ".$webjel."termek_termek_parameter_ertekek WHERE termek_id=".$_GET['id'];
		foreach ($pdo->query($query) as $row)
		{
			$insertcommand = "INSERT INTO ".$webjel."termek_termek_parameter_ertekek (termek_id, termek_parameter_id, ertek, felar) VALUES (:termek_id, :termek_parameter_id, :ertek, :felar)";
			$result = $pdo->prepare($insertcommand);
			$result->execute(array(':termek_id'=>$uj_id,
							  ':termek_parameter_id'=>$row['termek_parameter_id'],
							  ':ertek'=>$row['ertek'],
							  ':felar'=>$row['felar']));
		}
		// Képek
		$query = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$_GET['id'];
		foreach ($pdo->query($query) as $row)
		{
			$dir = $gyoker.'/images/termekek/';
			$regi_kep = $row['kep'];
			$uj_kep = $uj_id.'_'.$row['kep'];
			$insertcommand = "INSERT INTO ".$webjel."termek_kepek (termek_id, kep, alap) VALUES (:termek_id, :kep, :alap)";
			$result = $pdo->prepare($insertcommand);
			$result->execute(array(':termek_id'=>$uj_id,
							  ':kep'=>$uj_kep,
							  ':alap'=>$row['alap']));
			copy($dir.$regi_kep, $dir.$uj_kep);
		}
						  
		$_GET['id'] =  $uj_id;
	}
	
	// Adatok
	$query = "SELECT * FROM ".$webjel."termekek WHERE id=".$_GET['id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	if($_GET['csop_id'] == 'kereso') { $_GET['csop_id'] = $row['csop_id'];}
	$query_csop = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$_GET['csop_id'];
	$res = $pdo->prepare($query_csop);
	$res->execute();
	$row_csop = $res -> fetch();
	$kat_nev = $row_csop['nev'];
	if($_GET['csop_id'] == '-1') // Akciós termékek
	{
		$kat_nev = 'Akciós termékek';
	}
	else if($_GET['csop_id'] == '-2') // Kiemelt termékek
	{
		$kat_nev = 'Kiemelt termékek';
	}
	// Akciós-e
	if ($row['akcio_ig'] >= date("Y-m-d") && $row['akcio_tol'] <= date("Y-m-d")) // Ha akciós
	{
		$akcios = 1;
	}
	else
	{
		$akcios = 0;
	}
	
?>
<!--Modal-->
<div class="example-modal">
<div id="rakerdez_torles" class="modal modal-danger">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<h4 class="modal-title">Törlés</h4>
	  </div>
	  <div class="modal-body">
		<p>Biztos törölni szeretnéd a terméket?</p>
	  </div>
	  <div class="modal-footer">
		<button onClick="torolTermek('<?php print $_GET['id']; ?>', '<?php print $_GET['fajl']; ?>', '<?php print $_GET['csop_id']; ?>')" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
		<button onClick="megsem_termek('rakerdez_torles')" type="button" class="btn btn-outline">Mégsem</button>
	  </div>
	</div>
  </div>
</div>
</div>
		  
<div class="content-wrapper">
	<section class="content-header">
	  <h1>Termék <small>(ID = <?php print $row['id']; ?>)</small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a onClick="visszaTermekek('<?php print $_GET['fajl']; ?>', '<?php print $_GET['csop_id']; ?>', <?php print $row['id']; ?>)"><i class="fa fa-folder-open"></i> <?php print $kat_nev; ?></a></li>
		<li class="active"><?php print $row['nev']; ?></li>
	  </ol>
	</section>

	<section class="content">
		<!-- Termék adatok -->
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Termék adatok</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Megnevezés</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="nev" placeholder="Megnevezés" value="<?php print $row['nev']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Ár</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="ar" placeholder="Ár" value="<?php print $row['ar']; ?>" >
										<span class="input-group-addon">Ft</span>
									</div>
								</div>
								<div class="form-group">
									<label>ÁFA</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<select class="form-control" id="afa">
											<?php
												$query2 = "SELECT * FROM ".$webjel."afa ORDER BY id asc";
												foreach ($pdo->query($query2) as $row2)
												{
													if($row['afa'] == $row2['id'])
													{
														print '<option value="'.$row2['id'].'" selected>'.$row2['afa'].'</option>';
													}
													else
													{
														print '<option value="'.$row2['id'].'">'.$row2['afa'].'</option>';
													}
												}
											?>
										 </select>
										<span class="input-group-addon">%</span>
									</div>
								</div>
								<div class="form-group">
									<label>Akciós ár</label>
									<div class="input-group">
										<span class="input-group-addon <?php if($akcios==1){print 'input_jelolo_kek';} ?>"><i class="fa fa-star-o"></i></span>
										<input type="text" class="form-control" id="akciosar" placeholder="Akciós ár" value="<?php print $row['akciosar']; ?>" >
										<span class="input-group-addon">Ft</span>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>Akció kezdete</label>
											<div class="input-group">
												<span class="input-group-addon <?php if($akcios==1){print 'input_jelolo_kek';} ?>"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control datepicker" id="akcio_tol" placeholder="Akció kezdete" value="<?php print $row['akcio_tol']; ?>" >
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Akció vége</label>
											<div class="input-group">
												<span class="input-group-addon <?php if($akcios==1){print 'input_jelolo_kek';} ?>"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control datepicker" id="akcio_ig" placeholder="Akció vége" value="<?php print $row['akcio_ig']; ?>" >
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Kategória</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-folder-open"></i></span>
										<select class="form-control" id="csop_id">
											<?php
												$query2 = "SELECT * FROM ".$webjel."term_csoportok ORDER BY nev ASC";
												// $query2 = "SELECT * FROM `term_csoportok` as t1 WHERE NOT EXISTS (SELECT 1 FROM `term_csoportok` as t2 WHERE t1.`csop_id` = t2.`id`)";
												foreach ($pdo->query($query2) as $row2)
												{
													$rownum = 0;
													$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id=".$row2['id']);
													$res->execute();
													$rownum = $res->fetchColumn();
													if($rownum == 0) // nincs alkategóriája
													{
														if($row2['csop_id'] > 0)
														{
															$query_szul = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$row2['csop_id'];
															$res = $pdo->prepare($query_szul);
															$res->execute();
															$row_szul = $res -> fetch();
															$nev = $row2['nev'].' ('.$row_szul['nev'].')';
														}
														else
														{
															$nev = $row2['nev'];
														}
														
														if($row['csop_id'] == $row2['id'])
														{
															print '<option value="'.$row2['id'].'" selected>'.$nev.'</option>';
														}
														else
														{
															print '<option value="'.$row2['id'].'">'.$nev.'</option>';
														}
													}
												}
											?>
										 </select>
									</div>
								</div>
								<div class="form-group" <?php if($conf_tobb_kategoria == 0) { print 'style="display:none;"'; } ?>>
									<label>Több kategóriához való rendelés</label>
									<div class="input-group">
										<?php
											include('termek_urlap_csoportok.php');
										?>
									</div>
								</div>
								<div class="form-group">
									<label>Rövid leírás</label>
									<textarea class="form-control" id="rovid_leiras" rows="3" placeholder="Rövid leírás"><?php print $row['rovid_leiras']; ?></textarea>
								</div>
								<div class="form-group">
									<label>SEO title</label>
									<div class="input-group">
										<input type="text" class="form-control" id="seo_title" placeholder="SEO title" value="<?php print $row['seo_title']; ?>" >
									</div>
								</div>
								<div class="form-group">
									<label>SEO description</label>
									<textarea class="form-control" id="seo_description" rows="3" placeholder="SEO description"><?php print $row['seo_description']; ?></textarea>
								</div>
								<div class="row margtop10">
									<div class="col-sm-6">
										<div class="checkbox ">
											<label>
												<input type="checkbox" class="minimal" id="lathato" <?php if($row['lathato'] == 0)print 'checked'; ?>> Látható
											</label>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="checkbox">
											<label>
												<input type="checkbox" class="minimal" id="kiemelt" <?php if($row['kiemelt'] == 1)print 'checked'; ?>> Kiemelt
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Leírás</label>
									<textarea id="editor1" name="editor1" rows="10" cols="80">
										<?php print $row['leiras']; ?>
									</textarea>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer">
						<button type="submit" onClick="mentesTermek('<?php print $_GET['fajl']; ?>', <?php print $row['id']; ?>, '<?php print $_GET['csop_id']; ?>')" class="btn btn-primary">Mentés</button>
					</div>
				</div>
			</div>
			<!-- Képek -->
			<div class="col-md-6">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Képek feltöltése</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<form id="kepfeltoltes" action="/upload-target" class="dropzone"></form>
					</div>
					<div class="box-body" id="termek_kepek_div">
						<?php
							include('termek_kepek.php');
						?>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<!-- Gombok -->
				<div class="box box-danger">
					<div class="box-header">
						<h3 class="box-title">Vezérlőpult</h3>
					</div>
					<div class="box-body">
						<a onclick="rakerdez_termek('rakerdez_torles')" class="btn btn-app"><i class="fa fa-times"></i> Törlés</a>
						<a onClick="klonozas('<?php print $_GET['fajl']; ?>', '<?php print $_GET['csop_id']; ?>', <?php print $_GET['id']; ?>)" class="btn btn-app"><i class="fa fa-clone"></i> Klónozás</a>
					</div>
				</div>
				<!-- Paraméterek -->
				<div class="box box-warning" <?php if($conf_parameterek == 0) { print 'style="display:none;"'; } ?>>
					<div class="box-header">
						<h3 class="box-title">Termék paraméterek</h3>
					</div>
					<div class="box-body" id="parameterek">
						<?php
							include('termek_urlap_parameterek.php');
						?>
					</div>
					<div class="box-footer">
						<?php /*<button type="submit" onClick="mentesParameterek(<?php print $row['id']; ?>, <?php echo $_GET['csop_id']; ?>)" class="btn btn-primary">Mentés</button>*/ ?>
						A paramétereket egyesével lehet menteni vagy törölni!
					</div>
				</div>
				<!--RENDELÉSEK-->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Rendelések</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-hover table-striped margbot0">
							<tbody>
								<tr>
									<th>Azonosító</th>
									<th>Dátum</th>
									<th>Szállítás</th>
									<th>Fizetés</th>
									<th style="text-align:right;">Mennyiség</th>
									<th style="text-align:right;">Érték</th>
								</tr>
								<?php
									$query_rend = "SELECT *, ".$webjel."rendeles.id as id, 
												".$webjel."rendeles.rendeles_id as rendeles_id, 
												SUM(".$webjel."rendeles_tetelek.term_db) as db, 
												SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar > 0, ".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db)) as rendelt_osszeg 
											FROM ".$webjel."rendeles 
											INNER JOIN ".$webjel."rendeles_tetelek 
											ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
											WHERE ".$webjel."rendeles_tetelek.term_id=".$_GET['id']." 
											GROUP BY ".$webjel."rendeles.id
											ORDER BY ".$webjel."rendeles.id DESC";
									foreach ($pdo->query($query_rend) as $row_rend)
									{
										?>
											<tr class="kattintos_sor" onClick="window.location.href = '<?php print $domain; ?>/adm/rendelesek.php?id=<?php print $row_rend['id']; ?>&fajl=module-rendelesek/lista.php';">
										<?php
											print '<td>'.$row_rend['rendeles_id'].'</td>
											<td>'.$row_rend['datum'].'</td>
											<td>'.$row_rend['szall_mod'].'</td>
											<td>'.$row_rend['fiz_mod'].'</td>
											<td style="text-align:right;">'.$row_rend['db'].' db</td>
											<td style="text-align:right;">'.number_format($row_rend['rendelt_osszeg'], 0, ',', ' ').' Ft</td>
										</tr>';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<? print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<? print $kezd;?>"/>
<input type="hidden" name="kezd" id="oldalszam" value="<? print $oldalszam;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<? print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<? print $sorrend;?>"/>
