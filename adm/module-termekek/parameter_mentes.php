<?php
	session_start();
	ob_start();
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	// Módosítás (igen_nem)
	if(isset($_GET['command']) && $_GET['command'] == 'parameter_mod_egyedi')
	{
		$pdo->exec("UPDATE ".$webjel."termek_termek_parameter_ertekek SET ertek='".$_GET['ertek']."' WHERE termek_parameter_id =".$_GET['parameter_id']." AND termek_id=".$_GET['id']);
	}
	// Módosítás (lenyilo-egy_valaszthato)
	else if(isset($_GET['command']) && $_GET['command'] == 'parameter_mod_lenyilo_egy_valaszthato')
	{
		$pdo->exec("UPDATE ".$webjel."termek_termek_parameter_ertekek SET ertek='".$_GET['ertek']."' WHERE termek_parameter_id =".$_GET['parameter_id']." AND termek_id=".$_GET['id']);
	}
	// Felár módosítás (lenyilo-egy_valaszthato)
	else if(isset($_GET['command']) && $_GET['command'] == 'parameter_mod_felar')
	{
		$pdo->exec("UPDATE ".$webjel."termek_termek_parameter_ertekek SET felar='".$_GET['felar']."' WHERE id =".$_GET['id']);
	}
	
