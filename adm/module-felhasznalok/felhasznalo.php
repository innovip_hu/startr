<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	if (isset($_POST['command']))
	{
		if($_POST['command'] == 'mentes')
		{
			if($_POST['admin'] == 'true')
			{
				$admin = 'admin';
			}
			else
			{
				$admin = '';
			}
			
			// Ha nem módosult a jelszó
			if($_POST['jelszo1'] == '')
			{
				$updatecommand = "UPDATE ".$webjel."users  SET vezeteknev=?, email=?, telefon=?, tipus=?, cim_varos=?, cim_utca=?, cim_hszam=?, cim_irszam=?, cim_szall_nev=?, cim_szall_varos=?, cim_szall_utca=?, cim_szall_hszam=?, cim_szall_irszam=? WHERE id=?";
				$result = $pdo->prepare($updatecommand);
				$result->execute(array($_POST['nev'], $_POST['email'], $_POST['telefon'], $admin, $_POST['cim_varos'], $_POST['cim_utca'], $_POST['cim_hszam'], $_POST['cim_irszam'], $_POST['cim_szall_nev'], $_POST['cim_szall_varos'], $_POST['cim_szall_utca'], $_POST['cim_szall_hszam'], $_POST['cim_szall_irszam'], $_GET['id']));
			}
			else
			{
				$updatecommand = "UPDATE ".$webjel."users  SET vezeteknev=?, jelszo=?, email=?, telefon=?, tipus=?, cim_varos=?, cim_utca=?, cim_hszam=?, cim_irszam=?, cim_szall_nev=?, cim_szall_varos=?, cim_szall_utca=?, cim_szall_hszam=?, cim_szall_irszam=? WHERE id=?";
				$result = $pdo->prepare($updatecommand);
				$result->execute(array($_POST['nev'], md5($_POST['jelszo1']), $_POST['email'], $_POST['telefon'], $admin, $_POST['cim_varos'], $_POST['cim_utca'], $_POST['cim_hszam'], $_POST['cim_irszam'], $_POST['cim_szall_nev'], $_POST['cim_szall_varos'], $_POST['cim_szall_utca'], $_POST['cim_szall_hszam'], $_POST['cim_szall_irszam'], $_GET['id']));
			}
		}
	}
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."users ");
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'vezeteknev'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'ASC'; // Alap rendezési feltétel
	}
	
	// Adatok
	$query = "SELECT * FROM ".$webjel."users WHERE id=".$_GET['id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
?>
<div class="content-wrapper">
	<section class="content-header">
	  <h1>Felhasználók<small><?php print $row['vezeteknev']; ?></small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a onClick="vissza('<?php print $_GET['fajl']; ?>')"><i class="fa fa-user"></i> Felhasználók</a></li>
		<li class="active"><?php print $row['vezeteknev']; ?></li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- Személyes adatok -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Személyes adatok</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Név</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_nsarga"><i class="fa fa-user"></i></span>
										<input type="text" class="form-control" id="nev" placeholder="Felhasználó neve" value="<?php print $row['vezeteknev']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Felhasználónév</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="felhnev" placeholder="Felhasználónév" value="<?php print $row['felhnev']; ?>" readonly>
									</div>
								</div>
								<div id="riaszt_jelszo"></div>
								<div class="form-group">
									<label for="exampleInputPassword1">Jelszó</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-lock"></i></span>
										<input type="password" class="form-control" id="jelszo1" placeholder="Csak akkor töltsd ki, ha módosítani szeretnéd." value="">
									</div>
								</div>
								<div id="riaszt_jelszo2"></div>
								<div class="form-group">
									<label for="exampleInputPassword1">Jelszó ismét</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-lock"></i></span>
										<input type="password" class="form-control" id="jelszo2" placeholder="Csak akkor töltsd ki, ha módosítani szeretnéd." value="">
									</div>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Email cím</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-envelope"></i></span>
										<input type="email" class="form-control" id="email" placeholder="Felhasználó email címe" value="<?php print $row['email']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Telefonszám</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-phone"></i></span>
										<input type="text" class="form-control" id="telefon" placeholder="Felhasználó telefonszáma" value="<?php print $row['telefon']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Regisztráció dátuma</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-calendar"></i></span>
										<input type="text" class="form-control" id="regisztracio" placeholder="Regisztráció dátuma" value="<?php print $row['reg_datum']; ?>" readonly>
									</div>
								</div>
								<div class="checkbox margtop40">
									<label>
										<input type="checkbox" class="minimal" id="admin" <?php if($row['tipus'] == 'admin')print 'checked'; ?>> Adminisztrátor
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Város</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="cim_varos" placeholder="Város" value="<?php print $row['cim_varos']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Utca</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="cim_utca" placeholder="Utca" value="<?php print $row['cim_utca']; ?>" >
									</div>
								</div>
								<div class="form-group">
									<label>Házszám</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="cim_hszam" placeholder="Házszám" value="<?php print $row['cim_hszam']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Irányítószám</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="cim_irszam" placeholder="Irányítószám" value="<?php print $row['cim_irszam']; ?>" >
									</div>
								</div>
								<div class="form-group">
									<label>Szállítási név</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
										<input type="text" class="form-control" id="cim_szall_nev" placeholder="Szállítási név" value="<?php print $row['cim_szall_nev']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Város (szállítási cím)</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
										<input type="text" class="form-control" id="cim_szall_varos" placeholder="Város (szállítási cím)" value="<?php print $row['cim_szall_varos']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Utca (szállítási cím)</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
										<input type="text" class="form-control" id="cim_szall_utca" placeholder="Utca (szállítási cím)" value="<?php print $row['cim_szall_utca']; ?>" >
									</div>
								</div>
								<div class="form-group">
									<label>Házszám (szállítási cím)</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
										<input type="text" class="form-control" id="cim_szall_hszam" placeholder="Házszám (szállítási cím)" value="<?php print $row['cim_szall_hszam']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Irányítószám (szállítási cím)</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
										<input type="text" class="form-control" id="cim_szall_irszam" placeholder="Irányítószám (szállítási cím)" value="<?php print $row['cim_szall_irszam']; ?>" >
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer">
						<button type="submit" onClick="mentes('<?php print $_GET['fajl']; ?>', <?php print $row['id']; ?>)" class="btn btn-primary">Mentés</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Rendelések -->
		<div class="row">
			<div class="col-md-12">
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Rendelések</h3>
					</div>
					<div class="table-responsive">
						<table class="table table-hover table-striped margbot0">
							<tbody>
								<tr>
									<th>Azonosító</th>
									<th>Vásárlás dátuma</th>
									<th>Szállítás</th>
									<th>Fizetés</th>
									<th style="text-align:right;">Termékek mennyisége</th>
									<th style="text-align:right;">Vásárlás értéke</th>
								</tr>
								<?php
									$query_rend = "SELECT *, ".$webjel."rendeles.id as id, 
												".$webjel."rendeles.rendeles_id as rendeles_id, 
												SUM(".$webjel."rendeles_tetelek.term_db) as db, 
												SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar > 0, ".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db)) as rendelt_osszeg 
											FROM ".$webjel."rendeles 
											INNER JOIN ".$webjel."rendeles_tetelek 
											ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
											WHERE ".$webjel."rendeles.user_id=".$row['id']." AND ".$webjel."rendeles.noreg=0 
											GROUP BY ".$webjel."rendeles.id
											ORDER BY ".$webjel."rendeles.id DESC";
									foreach ($pdo->query($query_rend) as $row_rend)
									{
										?>
											<tr class="kattintos_sor" onClick="window.location.href = '<?php print $domain; ?>/adm/rendelesek.php?id=<?php print $row_rend['id']; ?>&fajl=module-rendelesek/lista.php';">
										<?php
											print '<td>'.$row_rend['rendeles_id'].'</td>
											<td>'.$row_rend['datum'].'</td>
											<td>'.$row_rend['szall_mod'].'</td>
											<td>'.$row_rend['fiz_mod'].'</td>
											<td style="text-align:right;">'.$row_rend['db'].' db</td>
											<td style="text-align:right;">'.number_format($row_rend['rendelt_osszeg'], 0, ',', ' ').' Ft</td>
										</tr>';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<? print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<? print $kezd;?>"/>
<input type="hidden" name="kezd" id="oldalszam" value="<? print $oldalszam;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<? print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<? print $sorrend;?>"/>
