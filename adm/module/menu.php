      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
              <a href="http://www.innovip.hu" target="_blank"><img src="images/innovip_logo_feher.png" style="width: 100%;"></a>
          </div>
		  
 		<?php if($conf_webshop == 1) { ?>
          <!-- Termék kereső -->
          <form action="#" id="kereso_form" method="get" class="sidebar-form">
            <div class="input-group">
              <input onKeyUp="termekKeresoAC()" type="text" id="termek_kereso_input_AC" value="" class="form-control" placeholder="Termék Kereső...">
              <span class="input-group-btn">
                <span onClick="termekKeresoAC()" type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></span>
              </span>
            </div>
          </form>
 		<?php } ?>
          <!-- Menü -->
          <ul class="sidebar-menu">
			<li <?php if($oldal=='nyitooldal'){print 'class="active"';} ?>><a href="index.php"><i class="fa fa-home"></i> <span>Nyitóoldal</span></a></li>
			
			<!-- Webshop -->
			<?php if($conf_webshop == 1) { ?>
				<li class="header">Webáruház</li>
				<li <?php if($oldal=='rendelesek'){print 'class="active"';} ?>>
				  <a href="rendelesek.php">
					<i class="fa fa-shopping-cart"></i>
					<span>Rendelések</span>
					<?php
						$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."rendeles WHERE teljesitve=0");
						$res->execute();
						$rownum_rendelesek = $res->fetchColumn();
						if($rownum_rendelesek > 0)
						{
							print '<span class="label label-primary pull-right">'.$rownum_rendelesek.'</span>';
						}
					?>
					
				  </a>
				</li>
				<!--<li <?php if($oldal=='termekek'){print 'class="active"';} ?>><a href="termekek.php"><i class="fa fa-dropbox"></i> <span>Termékek</span></a></li>-->
				<li class="treeview <?/*active*/?>">
					<a href="#">
						<i class="fa fa-dropbox"></i> <span>Termékek</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu <?/*active*/?>">
						<li><a class="menu_term_kat" data-csop_id="-1" ><i class="fa fa-circle-o"></i> Akciós termékek</a></li>
						<li><a class="menu_term_kat" data-csop_id="-2" ><i class="fa fa-circle-o"></i> Kiemelt termékek</a></li>
						<?php
						$query_csop1 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id=0 ORDER BY sorrend ASC";
						foreach ($pdo->query($query_csop1) as $row_csop1)
						{
							$rownum = 0;
							$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id=".$row_csop1['id']);
							$res->execute();
							$rownum = $res->fetchColumn();
							if($rownum > 0) // Ha van alkategória
							{
								print '<li><a href="#"><i class="fa fa-circle-o"></i> '.$row_csop1['nev'].' <i class="fa fa-angle-left pull-right"></i></a>
									<ul class="treeview-menu">';
									$query_csop2 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id=".$row_csop1['id']." ORDER BY sorrend ASC";
									foreach ($pdo->query($query_csop2) as $row_csop2)
									{
										$rownum = 0;
										$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id=".$row_csop2['id']);
										$res->execute();
										$rownum = $res->fetchColumn();
										if($rownum > 0) // Ha van alkategória
										{
											print '<li><a href="#"><i class="fa fa-circle-o"></i> '.$row_csop2['nev'].' <i class="fa fa-angle-left pull-right"></i></a>
												<ul class="treeview-menu">';
												$query_csop3 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id=".$row_csop2['id']." ORDER BY sorrend ASC";
												foreach ($pdo->query($query_csop3) as $row_csop3)
												{
													print '<li><a class="menu_term_kat" data-csop_id="'.$row_csop3['id'].'" ><i class="fa fa-circle-o"></i> '.$row_csop3['nev'].'</a></li>';
												}
											print '</ul></li>';
										}
										else // Ha nincs alkategória
										{
											print '<li><a class="menu_term_kat" data-csop_id="'.$row_csop2['id'].'" ><i class="fa fa-circle-o"></i> '.$row_csop2['nev'].'</a></li>';
										}
									}
								print '</ul></li>';
							}
							else // Ha nincs alkategória
							{
								print '<li><a class="menu_term_kat" data-csop_id="'.$row_csop1['id'].'" ><i class="fa fa-circle-o"></i> '.$row_csop1['nev'].'</a></li>';
							}
						}
						?>
					</ul>
				</li>
				<li <?php if($oldal=='kategoriak'){print 'class="active"';} ?>><a href="kategoriak.php"><i class="fa fa-folder-open"></i> <span>Kategóriák</span></a></li>
				
				<!-- Paraméterek -->
				<?php if($conf_parameterek == 1) { ?>
				<li <?php if($oldal=='parameterek'){print 'class="active"';} ?>><a href="parameterek.php"><i class="fa fa-puzzle-piece"></i> <span>Termék paraméterek</span></a></li>
				<?php } ?>
				
				<!-- Készlet -->
				<?php if($conf_keszlet == 1) { ?>
				<li <?php if($oldal=='keszlet'){print 'class="active"';} ?>><a href="keszlet.php"><i class="fa fa-cubes"></i> <span>Készlet</span></a></li>
				<?php } ?>
				
				<li <?php if($oldal=='statisztikak'){print 'class="active"';} ?>><a href="statisztikak.php"><i class="fa fa-bar-chart"></i> <span>Statisztikák</span></a></li>
				
				<!-- Kupon -->
				<?php if($conf_kupon == 1) { ?>
				<li <?php if($oldal=='kuponok'){print 'class="active"';} ?>><a href="kuponok.php"><i class="fa fa-tag"></i> <span>Kuponok</span></a></li>
				<?php } ?>
				
				<!-- Kívánságlista -->
				<?php if($conf_kivansaglista == 1) { ?>
				<li <?php if($oldal=='kivansaglista'){print 'class="active"';} ?>><a href="kivansaglista.php"><i class="fa fa-heart"></i> <span>Kívánságlista</span></a></li>
				<?php } ?>
				
				<!-- Beállítások -->
				<li <?php if($oldal=='beallitasok'){print 'class="active"';} ?>><a href="beallitasok.php"><i class="fa fa-cog"></i> <span>Beállítások</span></a></li>
				
				<!-- Árak megosztása -->
				<?php if($conf_arak_megosztasa == 1) { ?>
				<li <?php if($oldal=='arak_megosztasa'){print 'class="active"';} ?>><a href="arak_megosztasa.php"><i class="fa fa-share-alt"></i> <span>Árak megosztása</span></a></li>
				<?php } ?>
			<?php } ?>
			
			<li <?php if($oldal=='felhasznalok'){print 'class="active"';} ?>><a href="felhasznalok.php"><i class="fa fa-user"></i> <span>Felhasználók</span></a></li>
			
			<li class="header">Weboldal</li>
			
 			<!-- Hírek -->
			<?php if($conf_hirek == 1) { ?>
			<li <?php if($oldal=='hirek'){print 'class="active"';} ?>><a href="hirek.php"><i class="fa fa-newspaper-o"></i> <span>Hírek</span></a></li>
			<?php } ?>
			
 			<!-- Hírek 2 -->
			<?php if($conf_hirek == 1) { ?>
			<li <?php if($oldal=='hirek2'){print 'class="active"';} ?>><a href="hirek2.php"><i class="fa fa-question-circle"></i> <span>Tudta-e</span></a></li>
			<?php } ?>
			
			<!-- Hírlevél -->
			<?php if($conf_hirlevel == 1) { ?>
			<li <?php if($oldal=='hirlevel'){print 'class="active"';} ?>>
				<a href="hirlevel.php">
					<i class="fa fa-envelope"></i>
					<span>Hírlevél</span>
					<?php
						$rownum_hirl = 0;
						$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirlevel WHERE lekerdezve=0");
						$res->execute();
						$rownum_hirl = $res->fetchColumn();
						if($rownum_hirl > 0)
						{
							print '<span class="label pull-right bg-red">'.$rownum_hirl.'</span>';
						}
					?>
				</a>
            </li>
			<?php } ?>
			
			<!-- Banner -->
			<?php if($conf_banner == 1) { ?>
			<li <?php if($oldal=='bannerek'){print 'class="active"';} ?>><a href="bannerek.php"><i class="fa fa-picture-o"></i> <span>Bannerek</span></a></li>
			<?php } ?>
			
			<!-- Slider -->
			<?php if($conf_slider == 1) { ?>
				<li <?php if($oldal=='sliderek'){print 'class="active"';} ?>><a href="sliderek.php"><i class="fa fa-desktop"></i> <span>Sliderek</span></a></li>
			<?php } ?>
			
			<!-- Video Slider -->
			<?php if($conf_video_slider == 1) { ?>
				<li <?php if($oldal=='video_sliderek'){print 'class="active"';} ?>><a href="video_sliderek.php"><i class="fa fa-video-camera"></i> <span>Video Sliderek</span></a></li>
			<?php } ?>
			
			<!-- Galéria -->
			<?php if($conf_galeria == 1) { ?>
			<li <?php if($oldal=='galeria'){print 'class="active"';} ?>><a href="galeria.php"><i class="fa fa-camera"></i> <span>Galéria</span></a></li>
			<?php } ?>
			
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
