	// Új felhasználó
	function uj(belep_fajl,fajl) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				//iCheck for checkbox and radio inputs
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			}
		  }
		xmlhttp.open("GET",belep_fajl+"?script=ok&fajl="+fajl,true);
		xmlhttp.send();
	};
	// Új felhasználó - Visszalépés a listába
	function ujVissza(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				//iCheck for checkbox and radio inputs
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok",true);
		xmlhttp.send();
	};
	// Új felhasználó - Mentés (személyes adatok)
	function ujMentes(fajl) {
		var mehet = 'igen';
		document.getElementById("riaszt_jelszo").innerHTML = '';
		document.getElementById("riaszt_jelszo2").innerHTML = '';
		document.getElementById("riaszt_nev").innerHTML = '';
		document.getElementById("riaszt_felhnev").innerHTML = '';
		if(document.getElementById("nev").value == '')
		{
			document.getElementById("nev").focus();
			document.getElementById("riaszt_nev").innerHTML = '<div class="alert alert-danger margbot0">Add meg a nevet!</div>';
			var mehet = 'nem';
		}
		else if(document.getElementById("felhnev").value == '')
		{
			document.getElementById("felhnev").focus();
			document.getElementById("riaszt_felhnev").innerHTML = '<div class="alert alert-danger margbot0">Add meg a felhasználónevet!</div>';
			var mehet = 'nem';
		}
		else if(document.getElementById("jelszo1").value == '')
		{
			document.getElementById("jelszo1").focus();
			document.getElementById("riaszt_jelszo").innerHTML = '<div class="alert alert-danger margbot0">Add meg a jelszót!</div>';
			var mehet = 'nem';
		}
		else if(document.getElementById("jelszo2").value == '')
		{
			document.getElementById("jelszo2").focus();
			document.getElementById("riaszt_jelszo2").innerHTML = '<div class="alert alert-danger margbot0">Add meg ismét a jelszót!</div>';
			var mehet = 'nem';
		}
		else if(document.getElementById("jelszo1").value != document.getElementById("jelszo2").value)
		{
			document.getElementById("jelszo1").focus();
			document.getElementById("riaszt_jelszo").innerHTML = '<div class="alert alert-danger margbot0">A két jelszó nem egyezik!</div>';
			var mehet = 'nem';
		}
		if(mehet == 'igen')
		{
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
					//iCheck for checkbox and radio inputs
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Hover message
					$('<span>Adatok elmentve.</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("POST",fajl+"?script=ok",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("command=uj_felhasznalo&nev="+encodeURIComponent(document.getElementById("nev").value)
					+"&felhnev="+encodeURIComponent(document.getElementById("felhnev").value)
					+"&jelszo1="+encodeURIComponent(document.getElementById("jelszo1").value)
					+"&email="+encodeURIComponent(document.getElementById("email").value)
					+"&telefon="+encodeURIComponent(document.getElementById("telefon").value)
					+"&admin="+encodeURIComponent(document.getElementById("admin").checked)
					+"&cim_varos="+encodeURIComponent(document.getElementById("cim_varos").value)
					+"&cim_utca="+encodeURIComponent(document.getElementById("cim_utca").value)
					+"&cim_hszam="+encodeURIComponent(document.getElementById("cim_hszam").value)
					+"&cim_irszam="+encodeURIComponent(document.getElementById("cim_irszam").value)
					+"&cim_szall_nev="+encodeURIComponent(document.getElementById("cim_szall_nev").value)
					+"&cim_szall_varos="+encodeURIComponent(document.getElementById("cim_szall_varos").value)
					+"&cim_szall_utca="+encodeURIComponent(document.getElementById("cim_szall_utca").value)
					+"&cim_szall_hszam="+encodeURIComponent(document.getElementById("cim_szall_hszam").value)
					+"&cim_szall_irszam="+encodeURIComponent(document.getElementById("cim_szall_irszam").value));
		}
	};
	
	// Belépés
	function Belepes(belep_fajl,fajl,id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				//iCheck for checkbox and radio inputs
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			}
		  }
		xmlhttp.open("GET",belep_fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl,true);
		xmlhttp.send();
	};
	
	// Visszalépés a listába
	function vissza(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				//iCheck for checkbox and radio inputs
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+encodeURIComponent(document.getElementById("oldalszam").value)+"&sorr_tip="+encodeURIComponent(document.getElementById("sorr_tip").value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)+"&kezd="+encodeURIComponent(document.getElementById("kezd").value),true);
		xmlhttp.send();
	};
	
	// Mentés (személyes adatok)
	function mentes(fajl,id) {
		var mehet = 'igen';
		document.getElementById("riaszt_jelszo").innerHTML = '';
		document.getElementById("riaszt_jelszo2").innerHTML = '';
		if(document.getElementById("jelszo1").value != '' || document.getElementById("jelszo2").value != '')
		{
			if(document.getElementById("jelszo1").value == '')
			{
				document.getElementById("jelszo1").focus();
				document.getElementById("riaszt_jelszo").innerHTML = '<div class="alert alert-danger margbot0">Add meg a jelszót!</div>';
				var mehet = 'nem';
			}
			else if(document.getElementById("jelszo2").value == '')
			{
				document.getElementById("jelszo2").focus();
				document.getElementById("riaszt_jelszo2").innerHTML = '<div class="alert alert-danger margbot0">Add meg ismét a jelszót!</div>';
				var mehet = 'nem';
			}
			else if(document.getElementById("jelszo1").value != document.getElementById("jelszo2").value)
			{
				document.getElementById("jelszo1").focus();
				document.getElementById("riaszt_jelszo").innerHTML = '<div class="alert alert-danger margbot0">A két jelszó nem egyezik!</div>';
				var mehet = 'nem';
			}
		}
		if(mehet == 'igen')
		{
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
					//iCheck for checkbox and radio inputs
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Hover message
					$('<span>Adatok elmentve.</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("POST","module-felhasznalok/felhasznalo.php?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl,true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("command=mentes&nev="+encodeURIComponent(document.getElementById("nev").value)
					+"&jelszo1="+encodeURIComponent(document.getElementById("jelszo1").value)
					+"&email="+encodeURIComponent(document.getElementById("email").value)
					+"&telefon="+encodeURIComponent(document.getElementById("telefon").value)
					+"&admin="+encodeURIComponent(document.getElementById("admin").checked)
					+"&cim_varos="+encodeURIComponent(document.getElementById("cim_varos").value)
					+"&cim_utca="+encodeURIComponent(document.getElementById("cim_utca").value)
					+"&cim_hszam="+encodeURIComponent(document.getElementById("cim_hszam").value)
					+"&cim_irszam="+encodeURIComponent(document.getElementById("cim_irszam").value)
					+"&cim_szall_nev="+encodeURIComponent(document.getElementById("cim_szall_nev").value)
					+"&cim_szall_varos="+encodeURIComponent(document.getElementById("cim_szall_varos").value)
					+"&cim_szall_utca="+encodeURIComponent(document.getElementById("cim_szall_utca").value)
					+"&cim_szall_hszam="+encodeURIComponent(document.getElementById("cim_szall_hszam").value)
					+"&cim_szall_irszam="+encodeURIComponent(document.getElementById("cim_szall_irszam").value));
		}
	};
	// lapozás Tovább gomb
	function lapozasTovabb(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				//iCheck for checkbox and radio inputs
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)+Number(document.getElementById("oldalszam").value)),true);
		xmlhttp.send();
	};
	// lapozás Vissza gomb
	function lapozasVissza(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				//iCheck for checkbox and radio inputs
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)-Number(document.getElementById("oldalszam").value)),true);
		xmlhttp.send();
	};
	
	// Csak admin
	function csakAdmin(allapot)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				//iCheck for checkbox and radio inputs
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			}
		  }
		xmlhttp.open("GET","module-felhasznalok/lista.php?script=ok&command=csakadmin&oldalszam="+encodeURIComponent(document.getElementById("oldalszam").value)+"&sorr_tip="+encodeURIComponent(document.getElementById("sorr_tip").value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)+"&kezd="+encodeURIComponent(document.getElementById("kezd").value)+"&admin_szuro="+allapot,true);
		xmlhttp.send();
	};
// AutoComplete keresés
	function tablaAutoComplete()
	{
		var keresett_szoveg = document.getElementById("tabla_kereso").value;
		if(document.getElementById('tabla_kereso').value.length >= 2)
		{
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
					document.getElementById("tabla_kereso").value = keresett_szoveg;
					document.getElementById("tabla_kereso").focus();
					//iCheck for checkbox and radio inputs
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
				}
			  }
			xmlhttp.open("GET","module-felhasznalok/lista_AC.php?script=ok&tabla_kereso="+encodeURIComponent(document.getElementById("tabla_kereso").value),true);
			xmlhttp.send();
		}
	};

 	