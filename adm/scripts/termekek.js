	// lapozás Tovább gomb
	function lapozasTovabbTermekek(fajl, csop_id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)+Number(document.getElementById("oldalszam").value))+"&csop_id="+csop_id,true);
		xmlhttp.send();
	};
	// lapozás Vissza gomb
	function lapozasVisszaTermekek(fajl, csop_id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)-Number(document.getElementById("oldalszam").value))+"&csop_id="+csop_id,true);
		xmlhttp.send();
	};

	// Visszalépés a listába
	function visszaTermekek(fajl, csop_id, id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// $('#horgony_'+id).select();
				$('html, body').animate({
					scrollTop: $('#horgony_'+id).offset().top
				}, 500);
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+encodeURIComponent(document.getElementById("oldalszam").value)+"&sorr_tip="+encodeURIComponent(document.getElementById("sorr_tip").value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)+"&kezd="+encodeURIComponent(document.getElementById("kezd").value)+"&csop_id="+csop_id,true);
		xmlhttp.send();
	};
	
 	// Belépés
	function BelepesTermekbe(belep_fajl, fajl, id, csop_id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// iCheck for checkbox and radio inputs
				var scrpt = document.createElement('script');
				scrpt.src='plugins/iCheck/icheck.min.js';
				document.head.appendChild(scrpt);
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
				// Termék paraméterek
				parameter_mentes_egyedi ();
				/* $('.muliselectes.jellemzo').multipleSelect({
					onClick : function(view) {
						if (view.checked) {
							$('#termek_parameter_ertek_'+view.value).show();
						}
						else {
							$('#termek_parameter_ertek_'+view.value).hide();
							$('#termek_parameter_ertek_'+view.value+' input').val('');
						}
					}
				});		 */
				// Több kategóriához rendelés
				$('.muliselectes.csoportok').multipleSelect({
				});
				// Dropzone
				$("#kepfeltoltes").dropzone({
					dictDefaultMessage: "Húzd ide a feltöltendő képeket, vagy kattints a mezőbe",
					autoProcessQueue: true,
					// acceptedFiles: "image/jpeg",
					url: 'upload.php?id='+id,
					// maxFiles: 20, // Number of files at a time
					maxFilesize: 10, //in MB
					maxfilesexceeded: function(file)
					{
					alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
					},
					init: function () {
						this.on("complete", function (file) {
							setTimeout( function() {
								$('.dz-complete').remove();
							}, 2000); // feltöltés után az ikon törlése
							if (window.XMLHttpRequest)
							  {// code for IE7+, Firefox, Chrome, Opera, Safari
							  xmlhttp=new XMLHttpRequest();
							  }
							else
							  {// code for IE6, IE5
							  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							  }
							xmlhttp.onreadystatechange=function()
							  {
							  if (xmlhttp.readyState==4 && xmlhttp.status==200)
								{
									document.getElementById("termek_kepek_div").innerHTML=xmlhttp.responseText;
								}
							  }
							xmlhttp.open("GET","module-termekek/termek_kepek.php?script=ok&id="+id,true);
							xmlhttp.send();
						});
					}
				});
				// Datepicker
				$('.datepicker').datepicker({
					format: 'yyyy-mm-dd',
					startDate: false,
					isRTL: false,
					autoclose:true,
					todayBtn: true,
					todayHighlight: true,
				});
				// CKEDitor
				$(function () {
					var config = {
						htmlEncodeOutput: false,
						entities: false
					};
					CKEDITOR.replace( 'editor1', config );
					CKEDITOR.config.toolbar = [
					   ['Format','FontSize','Maximize','Source', 'Table'],
					   ['Bold','Italic','Underline','Strike','-','Outdent','Indent','-'],
					   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					   ['Link','Smiley','TextColor','BGColor']
					] ;
				});
			}
		  }
		xmlhttp.open("GET",belep_fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl+"&csop_id="+csop_id,true);
		xmlhttp.send();
	};
	
 	// Klónozás
	function klonozas(fajl, csop_id, id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// iCheck for checkbox and radio inputs
				var scrpt = document.createElement('script');
				scrpt.src='plugins/iCheck/icheck.min.js';
				document.head.appendChild(scrpt);
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
				// Termék paraméterek
				parameter_mentes_egyedi ();
				/* $('.muliselectes.jellemzo').multipleSelect({
					onClick : function(view) {
						if (view.checked) {
							$('#termek_parameter_ertek_'+view.value).show();
						}
						else {
							$('#termek_parameter_ertek_'+view.value).hide();
							$('#termek_parameter_ertek_'+view.value+' input').val('');
						}
					}
				});		 */
				// Több kategóriához rendelés
				$('.muliselectes.csoportok').multipleSelect({
				});
				// Dropzone
				$("#kepfeltoltes").dropzone({
					dictDefaultMessage: "Húzd ide a feltöltendő képeket, vagy kattints a mezőbe",
					autoProcessQueue: true,
					// acceptedFiles: "image/jpeg",
					url: 'upload.php?id='+id,
					// maxFiles: 20, // Number of files at a time
					maxFilesize: 10, //in MB
					maxfilesexceeded: function(file)
					{
					alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
					},
					init: function () {
						this.on("complete", function (file) {
							setTimeout( function() {
								$('.dz-complete').remove();
							}, 2000); // feltöltés után az ikon törlése
							if (window.XMLHttpRequest)
							  {// code for IE7+, Firefox, Chrome, Opera, Safari
							  xmlhttp=new XMLHttpRequest();
							  }
							else
							  {// code for IE6, IE5
							  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							  }
							xmlhttp.onreadystatechange=function()
							  {
							  if (xmlhttp.readyState==4 && xmlhttp.status==200)
								{
									document.getElementById("termek_kepek_div").innerHTML=xmlhttp.responseText;
								}
							  }
							xmlhttp.open("GET","module-termekek/termek_kepek.php?script=ok&id="+id,true);
							xmlhttp.send();
						});
					}
				});
				// Datepicker
				$('.datepicker').datepicker({
					format: 'yyyy-mm-dd',
					startDate: false,
					isRTL: false,
					autoclose:true,
					todayBtn: true,
					todayHighlight: true,
				});
				// CKEDitor
				$(function () {
					var config = {
						htmlEncodeOutput: false,
						entities: false
					};
					CKEDITOR.replace( 'editor1', config );
					CKEDITOR.config.toolbar = [
					   ['Format','FontSize','Maximize','Source', 'Table'],
					   ['Bold','Italic','Underline','Strike','-','Outdent','Indent','-'],
					   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					   ['Link','Smiley','TextColor','BGColor']
					] ;
				});
				// Hover message
				$('<span>Klón termék létrehozva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-termekek/termek.php?script=ok&command=klonozas&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl+"&csop_id="+csop_id,true);
		xmlhttp.send();
	};
	
	// Alapadatok mentése
	function mentesTermek(fajl, id, csop_id) {
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// iCheck for checkbox and radio inputs
				var scrpt = document.createElement('script');
				scrpt.src='plugins/iCheck/icheck.min.js';
				document.head.appendChild(scrpt);
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
				// Termék paraméterek
				parameter_mentes_egyedi ();
				/* $('.muliselectes.jellemzo').multipleSelect({
					onClick : function(view) {
						if (view.checked) {
							$('#termek_parameter_ertek_'+view.value).show();
						}
						else {
							$('#termek_parameter_ertek_'+view.value).hide();
							$('#termek_parameter_ertek_'+view.value+' input').val('');
						}
					}
				});	 */
				// Több kategóriához rendelés
				$('.muliselectes.csoportok').multipleSelect({
				});	
				// Dropzone
				$("#kepfeltoltes").dropzone({
					dictDefaultMessage: "Húzd ide a feltöltendő képeket, vagy kattints a mezőbe",
					autoProcessQueue: true,
					// acceptedFiles: "image/jpeg",
					url: 'upload.php?id='+id,
					// maxFiles: 20, // Number of files at a time
					maxFilesize: 10, //in MB
					maxfilesexceeded: function(file)
					{
					alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
					},
					init: function () {
						this.on("complete", function (file) {
							setTimeout( function() {
								$('.dz-complete').remove();
							}, 2000); // feltöltés után az ikon törlése
							if (window.XMLHttpRequest)
							  {// code for IE7+, Firefox, Chrome, Opera, Safari
							  xmlhttp=new XMLHttpRequest();
							  }
							else
							  {// code for IE6, IE5
							  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							  }
							xmlhttp.onreadystatechange=function()
							  {
							  if (xmlhttp.readyState==4 && xmlhttp.status==200)
								{
									document.getElementById("termek_kepek_div").innerHTML=xmlhttp.responseText;
								}
							  }
							xmlhttp.open("GET","module-termekek/termek_kepek.php?script=ok&id="+id,true);
							xmlhttp.send();
						});
					}
				});
				// Datepicker
				$('.datepicker').datepicker({
					format: 'yyyy-mm-dd',
					startDate: false,
					isRTL: false,
					autoclose:true,
					todayBtn: true,
					todayHighlight: true,
				});
				// CKEDitor
				$(function () {
					var config = {
						htmlEncodeOutput: false,
						entities: false
					};
					CKEDITOR.replace( 'editor1', config );
					CKEDITOR.config.toolbar = [
					   ['Format','FontSize','Maximize','Source', 'Table'],
					   ['Bold','Italic','Underline','Strike','-','Outdent','Indent','-'],
					   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					   ['Link','Smiley','TextColor','BGColor']
					] ;
				});
				// Hover message
				$('<span>Adatok elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("POST","module-termekek/termek.php?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl+"&csop_id="+csop_id,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("command=adaok_mentese&nev="+encodeURIComponent(document.getElementById("nev").value)
				+"&ar="+encodeURIComponent(document.getElementById("ar").value)
				+"&afa="+encodeURIComponent(document.getElementById("afa").value)
				+"&akciosar="+encodeURIComponent(document.getElementById("akciosar").value)
				+"&akcio_tol="+encodeURIComponent(document.getElementById("akcio_tol").value)
				+"&akcio_ig="+encodeURIComponent(document.getElementById("akcio_ig").value)
				+"&rovid_leiras="+encodeURIComponent(document.getElementById("rovid_leiras").value)
				+"&seo_title="+encodeURIComponent(document.getElementById("seo_title").value)
				+"&seo_description="+encodeURIComponent(document.getElementById("seo_description").value)
				+"&lathato="+encodeURIComponent(document.getElementById("lathato").checked)
				+"&kiemelt="+encodeURIComponent(document.getElementById("kiemelt").checked)
				+"&leiras="+leiras
				+"&csop_id="+encodeURIComponent(document.getElementById("csop_id").value)
				+"&termek_csoportok="+encodeURIComponent(getSelectValues(document.getElementById("termek_csoportok")))
				);
	}
	function getSelectValues(select) {
		var result = [];
		var options = select && select.options;
		var opt;
		for (var i=0, iLen=options.length; i<iLen; i++) {
			opt = options[i];
			if (opt.selected) {
				result.push(opt.value || opt.text);
			}
		}
		return result;
	}
	
	// Képek törlése
	function termekKepTorles(id, kep_id) {
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termek_kepek_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Kép törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-termekek/termek_kepek.php?script=ok&id="+id+"&torlendo_kep_id="+kep_id,true);
		xmlhttp.send();
	};

	// Képek alapértelmezése
	function termekKepAlap(id, kep_id) {
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termek_kepek_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Alapértelmezés beállítva</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-termekek/termek_kepek.php?script=ok&id="+id+"&alap_kep_id="+kep_id,true);
		xmlhttp.send();
	};

	 // Új termék
	function BelepesUjTermekbe(belep_fajl, fajl, csop_id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// iCheck for checkbox and radio inputs
				var scrpt = document.createElement('script');
				scrpt.src='plugins/iCheck/icheck.min.js';
				document.head.appendChild(scrpt);
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
				// Datepicker
				$('.datepicker').datepicker({
					format: 'yyyy-mm-dd',
					startDate: false,
					isRTL: false,
					autoclose:true,
					todayBtn: true,
					todayHighlight: true,
				});
				// CKEDitor
				$(function () {
					var config = {
						htmlEncodeOutput: false,
						entities: false
					};
					CKEDITOR.replace( 'editor1', config );
					CKEDITOR.config.toolbar = [
					   ['Format','FontSize','Maximize','Source', 'Table'],
					   ['Bold','Italic','Underline','Strike','-','Outdent','Indent','-'],
					   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					   ['Link','Smiley','TextColor','BGColor']
					] ;
				});
			}
		  }
		xmlhttp.open("GET",belep_fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&fajl="+fajl+"&csop_id="+csop_id,true);
		xmlhttp.send();
	};
	
	// Új termék
	function mentesUjTermek(fajl, csop_id) {
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		
		// ÚJ termék létrehozása ID miatt
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var id = parseFloat(xmlhttp.responseText); // Az új termék ID-je

				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
						// Hover message
						$('<span>Termék elmentve.</span>').hovermessage({
							autoclose : 3000,
							position : 'top-right',
						});
						$('html,body').scrollTop(0);
						// iCheck for checkbox and radio inputs
						var scrpt = document.createElement('script');
						scrpt.src='plugins/iCheck/icheck.min.js';
						document.head.appendChild(scrpt);
						$('.checkbox input[type="checkbox"]').iCheck({
						  checkboxClass: 'icheckbox_flat-blue',
						  radioClass: 'iradio_flat-blue'
						});
						// Termék paraméterek
						parameter_mentes_egyedi ();
						/* $('.muliselectes.jellemzo').multipleSelect({
							onClick : function(view) {
								if (view.checked) {
									$('#termek_parameter_ertek_'+view.value).show();
								}
								else {
									$('#termek_parameter_ertek_'+view.value).hide();
									$('#termek_parameter_ertek_'+view.value+' input').val('');
								}
							}
						});		 */
						// Több kategóriához rendelés
						$('.muliselectes.csoportok').multipleSelect({
						});
						// Dropzone
						$("#kepfeltoltes").dropzone({
							dictDefaultMessage: "Húzd ide a feltöltendő képeket, vagy kattints a mezőbe",
							autoProcessQueue: true,
							// acceptedFiles: "image/jpeg",
							url: 'upload.php?id='+id,
							// maxFiles: 20, // Number of files at a time
							maxFilesize: 10, //in MB
							maxfilesexceeded: function(file)
							{
							alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
							},
							init: function () {
								this.on("complete", function (file) {
									setTimeout( function() {
										$('.dz-complete').remove();
									}, 2000); // feltöltés után az ikon törlése
									if (window.XMLHttpRequest)
									  {// code for IE7+, Firefox, Chrome, Opera, Safari
									  xmlhttp=new XMLHttpRequest();
									  }
									else
									  {// code for IE6, IE5
									  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
									  }
									xmlhttp.onreadystatechange=function()
									  {
									  if (xmlhttp.readyState==4 && xmlhttp.status==200)
										{
											document.getElementById("termek_kepek_div").innerHTML=xmlhttp.responseText;
										}
									  }
									xmlhttp.open("GET","module-termekek/termek_kepek.php?script=ok&id="+id,true);
									xmlhttp.send();
								});
							}
						});
						// Datepicker
						$('.datepicker').datepicker({
							format: 'yyyy-mm-dd',
							startDate: false,
							isRTL: false,
							autoclose:true,
							todayBtn: true,
							todayHighlight: true,
						});
						// CKEDitor
						$(function () {
							var config = {
								htmlEncodeOutput: false,
								entities: false
							};
							CKEDITOR.replace( 'editor1', config );
							CKEDITOR.config.toolbar = [
							   ['Format','FontSize','Maximize','Source', 'Table'],
							   ['Bold','Italic','Underline','Strike','-','Outdent','Indent','-'],
							   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
							   ['Link','Smiley','TextColor','BGColor']
							] ;
						});
					}
				  }
				xmlhttp.open("POST","module-termekek/termek.php?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&fajl="+fajl+"&id="+id+"&csop_id="+csop_id,true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send("command=uj_termek&nev="+encodeURIComponent(document.getElementById("nev").value)
						+"&ar="+encodeURIComponent(document.getElementById("ar").value)
						+"&afa="+encodeURIComponent(document.getElementById("afa").value)
						+"&akciosar="+encodeURIComponent(document.getElementById("akciosar").value)
						+"&akcio_tol="+encodeURIComponent(document.getElementById("akcio_tol").value)
						+"&akcio_ig="+encodeURIComponent(document.getElementById("akcio_ig").value)
						+"&rovid_leiras="+encodeURIComponent(document.getElementById("rovid_leiras").value)
						+"&seo_title="+encodeURIComponent(document.getElementById("seo_title").value)
						+"&seo_description="+encodeURIComponent(document.getElementById("seo_description").value)
						+"&lathato="+encodeURIComponent(document.getElementById("lathato").checked)
						+"&kiemelt="+encodeURIComponent(document.getElementById("kiemelt").checked)
						+"&leiras="+leiras
					);
			}
		  }
		xmlhttp.open("GET","module-termekek/termek_letrehozas.php?csop_id="+csop_id,true);
		xmlhttp.send();
		
	};
	// Modal nyitás
	function rakerdez_termek(modal_id) {
		document.getElementById(modal_id).style.display = 'block';
	};
	function megsem_termek(modal_id) {
		document.getElementById(modal_id).style.display = 'none';
	};
	// Termék törlése
	function torolTermek(id, fajl, csop_id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&command=termek_torles&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)-Number(document.getElementById("oldalszam").value))+"&csop_id="+csop_id+"&id="+id,true);
		xmlhttp.send();
	};
	// Paramétermentése (Andor)
	/* function mentesParameterek(id, csop_id){
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("parameterek").innerHTML=xmlhttp.responseText;
				// IDE kell írni, ha valamit akarunk futtatni a művelet után
				// Hover message
				$('<span>Az adatok sikeresen elmentve!</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
				// Termék paraméterek
				$('.muliselectes.jellemzo').multipleSelect({
					onClick : function(view) {
						if (view.checked) {
							$('#termek_parameter_ertek_'+view.value).show();
						}
						else {
							$('#termek_parameter_ertek_'+view.value).hide();
							$('#termek_parameter_ertek_'+view.value+' input').val('');
						}
					}
				});
			}
		  }
		xmlhttp.open("POST","module-termekek/termek_urlap_parameterek.php?script2=ok&termek_id="+id+"&csop_id="+csop_id,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send($("#parameterek_form").serialize());
	}; */
	// Termék kereső
	function termekKeresoAC() {
			// alert($('#termek_kereso_input_AC').val());
			// alert(document.getElementById('termek_kereso_input_AC').value);
			// console.log(document.getElementById('kereso_form'));
			// console.log($('#termek_kereso_input_AC'));
			
			// alert('www');
			
		if(document.getElementById('termek_kereso_input_AC').value.length >= 2)
		{
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				}
			  }
			xmlhttp.open("GET","module-termekek/lista_AC.php?script=ok&tabla_kereso="+encodeURIComponent(document.getElementById("termek_kereso_input_AC").value),true);
			xmlhttp.send();
		}
	};
	// Nézet váltás
	function nezetValtas(fajl, csop_id, nezet)
	{
		// Cookie beállítása
		var expires;
		var days = 365;
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		expires = "; expires=" + date.toGMTString();
		document.cookie = "admin_termek_lista_nezet="+nezet + expires + "; path=/";
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&csop_id="+csop_id,true);
		xmlhttp.send();
	};

	// Paraméterek
	function parameter_mentes_egyedi () {
		// Törlés
		$('.parameter_torles_gomb').click(function() {
			// alert( "Paraméter: "+$(this).attr('attr_parameter_id') );
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("parameterek").innerHTML=xmlhttp.responseText;
					// iCheck for checkbox and radio inputs
					var scrpt = document.createElement('script');
					scrpt.src='plugins/iCheck/icheck.min.js';
					document.head.appendChild(scrpt);
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Paraméter újra hívása
					parameter_mentes_egyedi ();
					// Hover message
					$('<span>Paraméter törölve!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/termek_urlap_parameterek.php?script2=ok&command=parameter_torles&id="+$(this).attr('attr_termek_id')+"&parameter_id="+$(this).attr('attr_parameter_id'),true);
			xmlhttp.send();
		});
		// Módósítás előkészítése (egyedi paraméter)
		$('.parameter_input_egyedi').keyup(function() {
			$("#parameter_span_gomb_"+$(this).attr('attr_parameter_id')).addClass("input_jelolo_piros");
			$("#parameter_span_gomb_"+$(this).attr('attr_parameter_id')).addClass("input_jelolo_gomb");
			$("#parameter_span_gomb_"+$(this).attr('attr_parameter_id')).children("i").addClass("fa-floppy-o");
		});
		// Módósítás (egyedi paraméter)
		$('.parameter_mentes_gomb').click(function() {
			var ertek = $('#parameter_'+$(this).attr('attr_parameter_id')).val();
			var id = $(this).attr('attr_parameter_id');
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					$("#parameter_span_gomb_"+id).removeClass("input_jelolo_piros");
					$("#parameter_span_gomb_"+id).removeClass("input_jelolo_gomb");
					$("#parameter_span_gomb_"+id).children("i").removeClass("fa-floppy-o");
					// Hover message
					$('<span>Paraméter módosítva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/parameter_mentes.php?script2=ok&command=parameter_mod_egyedi&id="+$(this).attr('attr_termek_id')+"&parameter_id="+$(this).attr('attr_parameter_id')+"&ertek="+ertek,true);
			xmlhttp.send();
		});
		// Módósítás (lenyilo-egy_valaszthato)
		$('.parameter_select').change(function() {
			var ertek = $(this).val();
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					// Hover message
					$('<span>Paraméter módosítva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/parameter_mentes.php?script2=ok&command=parameter_mod_lenyilo_egy_valaszthato&id="+$(this).attr('attr_termek_id')+"&parameter_id="+$(this).attr('attr_parameter_id')+"&ertek="+ertek,true);
			xmlhttp.send();
		});
		// Módósítás (igen-nem paraméter)
		$('.parameter_igen_nem_gomb').on('ifClicked', function(event){
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("parameterek").innerHTML=xmlhttp.responseText;
					// iCheck for checkbox and radio inputs
					var scrpt = document.createElement('script');
					scrpt.src='plugins/iCheck/icheck.min.js';
					document.head.appendChild(scrpt);
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Paraméter újra hívása
					parameter_mentes_egyedi ();
					// Hover message
					$('<span>Paraméter módosítva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/termek_urlap_parameterek.php?script2=ok&command=parameter_mod_igen_nem&id="+$(this).attr('attr_termek_id')+"&parameter_id="+$(this).attr('attr_parameter_id')+"&ertek="+$(this).attr('attr_ertek'),true);
			xmlhttp.send();
		});
		// Új paraméter
		$('#parameter_uj_parameter').change(function() {
			var parameter_id = $(this).val();
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("parameterek").innerHTML=xmlhttp.responseText;
					// iCheck for checkbox and radio inputs
					var scrpt = document.createElement('script');
					scrpt.src='plugins/iCheck/icheck.min.js';
					document.head.appendChild(scrpt);
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Paraméter újra hívása
					parameter_mentes_egyedi ();
					// Hover message
					$('<span>Új paraméter hozzáadva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/termek_urlap_parameterek.php?script2=ok&command=uj_parameter&id="+$(this).attr('attr_termek_id')+"&parameter_id="+parameter_id,true);
			xmlhttp.send();
		});
		// Feláras paraméter árának mentésének előkészítése
		$('.param_felar_input').keyup(function() {
			var id = $(this).attr('attr_param_ertek_id');
			$("#param_felar_gomb_"+id).css('display', 'inline');
		});
		// Feláras paraméter árának mentése
		$('.param_felar_gomb').click(function() {
			var felar = $('#param_felar_input_'+$(this).attr('attr_param_ertek_id')).val();
			var id = $(this).attr('attr_param_ertek_id');
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					$("#param_felar_gomb_"+id).css('display', 'none');
					// Hover message
					$('<span>Felár módosítva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/parameter_mentes.php?script2=ok&command=parameter_mod_felar&id="+id+"&felar="+felar,true);
			xmlhttp.send();
		});
		// Feláras paraméter érték törlése
		$('.param_ertek_torles_gomb').click(function() {
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("parameterek").innerHTML=xmlhttp.responseText;
					// iCheck for checkbox and radio inputs
					var scrpt = document.createElement('script');
					scrpt.src='plugins/iCheck/icheck.min.js';
					document.head.appendChild(scrpt);
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Paraméter újra hívása
					parameter_mentes_egyedi ();
					// Hover message
					$('<span>Érték törölve!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/termek_urlap_parameterek.php?script2=ok&command=parameter_ertek_torlesi&id="+$(this).attr('attr_termek_id')+"&parameter_ertek_id="+$(this).attr('attr_param_ertek_id'),true);
			xmlhttp.send();
		});
		// Új több választós paraméter
		$('.parameter_select_tobb_valasztos').change(function() {
			var ertek_id = $(this).val();
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("parameterek").innerHTML=xmlhttp.responseText;
					// iCheck for checkbox and radio inputs
					var scrpt = document.createElement('script');
					scrpt.src='plugins/iCheck/icheck.min.js';
					document.head.appendChild(scrpt);
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Paraméter újra hívása
					parameter_mentes_egyedi ();
					// Hover message
					$('<span>Új paraméter hozzáadva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/termek_urlap_parameterek.php?script2=ok&command=uj_tobb_valasztos_parameter_ertek&id="+$(this).attr('attr_termek_id')+"&parameter_id="+$(this).attr('attr_parameter_id')+"&ertek="+ertek_id,true);
			xmlhttp.send();
		});
	};
	