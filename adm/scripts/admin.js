/* Kategória menü megnyitása */
	$(document).on("click", ".menu_term_kat", function() {
		
		$(".menu_term_kat").each(function(){
			$(this).attr('class', 'menu_term_kat');
			$(this).find('i').attr('class', 'fa fa-circle-o');
		});
		
		$(this).attr('class', 'menu_term_kat aktiv_menu_term_kat');
		$(this).find('i').attr('class', 'fa fa-circle');
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// js fájl meghívása
				var scrpt = document.createElement('script');
				scrpt.src='scripts/termekek.js';
				document.head.appendChild(scrpt);
			}
		  }
		xmlhttp.open("GET","module-termekek/lista.php?script=ok&csop_id="+$(this).attr('data-csop_id'),true);
		xmlhttp.send();
	});

