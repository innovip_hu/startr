	// Szállítás mentése
	function mentesSzallitas() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("szallitas_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Szállítási adatok módosítva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-beallitasok/szallitas.php?script=ok&ingyenes_szallitas="+document.getElementById("ingyenes_szallitas").value
			+"&szall_kolts="+document.getElementById("szall_kolts").value
			+"&szall_kolts_utanvet="+document.getElementById("szall_kolts_utanvet").value
			+"&szall_kolts_egyedi="+document.getElementById("szall_kolts_egyedi").value
			+"&szall_kolts_egyedi_utanvet="+document.getElementById("szall_kolts_egyedi_utanvet").value
			+"&postapont="+document.getElementById("postapont").value
			+"&postapont_utanvet="+document.getElementById("postapont_utanvet").value
			+"&gls_cspont="+document.getElementById("gls_cspont").value
			+"&gls_cspont_utanvet="+document.getElementById("gls_cspont_utanvet").value
			+"&szallitas_afa="+document.getElementById("szallitas_afa").value
			,true);
		xmlhttp.send();
	};
	// Új ÁFA mentése
	function ujAfa() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("afa_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Új ÁFA elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-beallitasok/afa.php?script=ok&command=uj_afa&afa="+document.getElementById("afa_uj").value,true);
		xmlhttp.send();
	};
	// ÁFA módosításának aktiválása
	function afaMentesAktiv(id) {
		$("#afa_mentes_gomb_"+id).addClass("aktiv_gomb");
		$("#afa_"+id).css("color","#DD4B39");
	};
	// ÁFA módosítása
	function afaMentes(id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("afa_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>ÁFA módosítva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-beallitasok/afa.php?script=ok&command=afa_modositas&id="+id+"&afa="+document.getElementById("afa_"+id).value,true);
		xmlhttp.send();
	};
	// ÁFA módosítása
	function afaTorles(id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("afa_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>ÁFA törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-beallitasok/afa.php?script=ok&command=afa_torles&id="+id,true);
		xmlhttp.send();
	};
