﻿<?php
session_start();
ob_start();

	include '../../config.php';

	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	// Rendelés adatai
	$query_rend = "SELECT * FROM ".$webjel."rendeles WHERE id=".$_POST['rendeles_id'];
	$res = $pdo->prepare($query_rend);
	$res->execute();
	$row_rend = $res -> fetch();
print $query_rend;

	//Fejléc
	$peldany = '';
	$fejlec = 'Rendelés';
	$sorszam = 'Rendelésszám: '.$row_rend['rendeles_id'];
	$fajlnev = 'rendeles-'.$row_rend['rendeles_id'];
	
	// Vásárló adatai
	if ($row_rend['noreg'] == 0)//regisztrált vevő
	{
		$query_user = "SELECT * FROM ".$webjel."users where id=".$row_rend['user_id'];
	}
	else //noreg vevő
	{
		$query_user = "SELECT * FROM ".$webjel."users_noreg where id=".$row_rend['user_id'];
	}
	$res = $pdo->prepare($query_user);
	$res->execute();
	$row_user  = $res -> fetch();

	$html = '
	<html>
	<head>

	<style>
	body {font-family: dejavusanscondensed;
		font-size: 10pt;
		color: 606060;
	}
	p {	margin: 0pt; }
	table.items {
		border: 0;
	}
	td { vertical-align: top; }
	.items td {
		border: 0 !important;
		padding-top: 1mm;
		padding-bottom: 1mm;
	}
	table thead td { background-color: #4791D2;
		text-align: center;
		border: 0;
		color: #fff;
	}
	.items td.blanktotal {
		background-color: #EEEEEE;
		background-color: #FFFFFF;
		border: 0mm none #000000;
		/* border: 0.1mm solid #000000;
		border-top: 0.1mm solid #000000;
		border-right: 0.1mm solid #000000; */
	}
	.items td.totals {
		text-align: right;
		border: 0mm solid #000000;
	}
	.items td.cost {
		text-align: right;
	}
	.items td.vonal {
		border-bottom: 0.1mm solid #6c6c6c;
	}
	</style>
	</head>
	<body>

	<!--mpdf
	<htmlpageheader name="myheader">
	<table width="100%"><tr>
	<td width="50%" style="color:#0000BB; "><span style="font-weight: bold; font-size: 14pt;">Acme Trading Co.</span><br />123 Anystreet<br />Your City<br />GD12 4LP<br /><span style="font-family:dejavusanscondensed;">&#9742;</span> 01777 123 567</td>
	<td width="50%" style="text-align: right;">Invoice No.<br /><span style="font-weight: bold; font-size: 12pt;">0012345</span></td>
	</tr></table>
	</htmlpageheader>

	<htmlpagefooter name="myfooter">
		<div style="padding-top: 3mm; ">
			<table width="100%" style="font-size: 8pt; color: #9d9d9d; "><tr>
			<td width="80%">A bizonylat az OVIP Vállalatirányítási Rendszerrel készült (www.ovip.hu). Az online számlázó program megfelel a 23/2014-es NGM rendeletnek</td>
			<td width="20%" style="text-align: right;">Oldal {PAGENO}/{nbpg}</td>
			</tr></table>
		</div>
	</htmlpagefooter>
	<htmlpagefooter name="myfooter2">
		<div style="padding-top: 3mm; ">
			<table width="100%" style="font-size: 8pt; color: #9d9d9d; "><tr>
			<td width="80%">A bizonylat az OVIP Vállalatirányítási Rendszerrel készült (www.ovip.hu). Az online számlázó program megfelel a 23/2014-es NGM rendeletnek</td>
			<td width="20%" style="text-align: right;">Oldal {PAGENO}/{nbpg}</td>
			</tr></table>
		</div>
	</htmlpagefooter>
<pagefooter name="myFooter1Even" content-left="{PAGENO}" content-center="myFooter1Even" content-right="{DATE j-m-Y}" footer-style="font-family:sans-serif; font-size:10pt; color:#000880;" footer-style-left="font-weight:bold; " line="on" />

	<sethtmlpageheader name="myheader" value="off" show-this-page="1" />
	<sethtmlpagefooter name="myfooter" value="on" />
	mpdf-->

	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td width="64%" style="vertical-align:bottom; border-right: 0.1mm solid #6c6c6c; border-bottom: 0.1mm solid #6c6c6c;">
				<table width="100%" cellpadding="0" cellspacing="0" style="padding-right: 6mm; padding-bottom: 6mm;">
					<tr>
						<td colspan="3" style="font-size: 36pt; color: #231f34;">'.$fejlec.'</td>
					</tr>
					<tr>
						<td colspan="3" style="text-align:right; border-bottom: 1.5mm solid #4791D2;">'.$sorszam.$peldany.'</td>
					</tr>
					<tr>
						<td style="text-align: center;  padding-top: 1mm;">Vásárlás dátuma</td>
						<td style="text-align: center;  padding-top: 1mm;">Fizetési mód</td>
						<td style="text-align: center;  padding-top: 1mm;">Szállítási mód</td>
					</tr>
					<tr>
						<td style="text-align: center;  padding-top: 1mm;"><b>'.$row_rend['datum'].'</b></td>
						<td style="text-align: center;  padding-top: 1mm;"><b>'.$row_rend['fiz_mod'].'</b></td>
						<td style="text-align: center;  padding-top: 1mm;"><b>'.$row_rend['szall_mod'].'</b></td>
					</tr>
				</table>
			</td>';
			// Vevő adatai
			$html_tart = '<td width="36%" style="vertical-align:bottom; border-bottom: 0.1mm solid #6c6c6c; padding-left: 6mm; padding-bottom: 6mm;">
				Vásárló adatai
				<br/><b>'.$row_user['vezeteknev'].'</b>';
				$html_tart.= '<br/>'.$row_user['cim_irszam'].' '.$row_user['cim_varos'].',<br/>'.$row_user['cim_utca'].'<br/>'.$row_user['cim_hszam'];
			$html_tart .= '</td>
		</tr>
		<tr>
			<td width="64%" style="border-right: 0.1mm solid #6c6c6c; padding-right: 6mm; padding-top: 6mm;">';
				if ($row_rend['megjegyzes'] != '') //Ha van megjegyzés
				{
					$html_tart.= 'Megjegyzés:<br/>'.$row_rend['megjegyzes'];
				}
			$html_tart .= '</td>
			<td width="36%" style="padding-left: 6mm; padding-top: 6mm;">';
				if($row_user['cim_szall_varos'] != '')
				{
					$html_tart .= 'Eltérő szállítási cím
					<br/><b>'.$row_user['cim_szall_nev'].'</b>';
					$html_tart.= '<br/>'.$row_user['cim_szall_irszam'].' '.$row_user['cim_szall_varos'].',<br/>'.$row_user['cim_szall_utca'].'<br/>'.$row_user['cim_szall_hszam'];
				}
			$html_tart .= '</td>
		</tr>
	</table>

	<br />';
	

	$html_tart .= '<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
	<thead>
	<tr>
	<td width="40%">Terméknév</td>
	<td width="20%" align="right">Egységár</td>
	<td width="20%" align="right">Mennyiség</td>
	<td width="20%" align="right">Összesen</td>
	</tr>
	</thead>
	<tbody>
	<!-- ITEMS HERE -->';
	$db = 0;
	$ar = 0;
	$query_tetelek = "SELECT * FROM ".$webjel."rendeles_tetelek WHERE rendeles_id=".$row_rend['id'];
	foreach ($pdo->query($query_tetelek) as $row_tetelek)
	{
		$db = $db + 1;
		if($row_tetelek['term_akcios_ar'] == 0) //ha nem akciós
		{
			$egysegar = $row_tetelek['term_ar'];
		}
		else //ha akciós
		{
			$egysegar = $row_tetelek['term_akcios_ar'];
		}
		$osszar = $egysegar * $row_tetelek['term_db'];
		
		$html_tart.= '<tr>
			<td class="vonal"><b>'.$row_tetelek['term_nev'].'</b>';
				// Jellemzők
				$query_jell = "SELECT * FROM ".$webjel."rendeles_tetelek_termek_parameterek WHERE rendeles_tetel_id = ".$row_tetelek['id']." ORDER BY termek_parameter_nev ASC";
				$elso = 0;
				foreach ($pdo->query($query_jell) as $row_tetelek_jell)
				{
					if($elso == 1) { $html_tart.= ''; $elso = 0; } else { $html_tart.= '<br/>'; }
					$html_tart.= $row_tetelek_jell['termek_parameter_nev'].': '.$row_tetelek_jell['termek_parameter_ertek_nev'];
				}						
			$html_tart.= '</td>
			<td class="cost vonal">'.number_format($egysegar, 0, ',', ' ').' Ft</td>
			<td class="cost vonal">'.$row_tetelek['term_db'].' db</td>
			<td class="cost vonal">'.number_format($osszar, 0, ',', ' ').' Ft</td>
		</tr>';
		$ar += $osszar;
	}
	$html_tart .= '<!-- END ITEMS HERE -->
	<tr>
		<td style="padding-top:4mm;" class="totals" colspan="3">Vásárlás összesen:</td>
		<td style="padding-top:4mm;" class="totals cost">'.number_format($ar, 0, ',', ' ').' Ft</td>
	</tr>
	<tr>
		<td style="padding-top:4mm;" class="totals" colspan="3">Szállítási költség:</td>
		<td style="padding-top:4mm;" class="totals cost">'.number_format($row_rend['szallitasi_dij'], 0, ',', ' ').' Ft</td>
	</tr>';
	$html_tart .= '<tr>
		<td class="totals" colspan="3"><b>Fizetendő végösszeg:</b></td>
		<td class="totals cost"><b>'.number_format(($ar + $row_rend['szallitasi_dij']), 0, ',', ' ').' Ft</b>
		</td>
	</tr>';
	$html_tart .= '
	</tbody>
	</table>';

	
	$html .= $html_tart;



	$html .= '</body>
	</html>
	';
	//==============================================================
	
	include("mpdf.php");
	$mpdf=new mPDF('','A4','','',15,15,15,25,10,10); // (mode, format, default_font_size, default_font, margin_left, margin_right ,margin_top, margin_bottom, margin_header, margin_footer)
	$mpdf->WriteHTML($html);
	$mpdf->Output($fajlnev.'.pdf','I');
	exit;
	
	// print $html;
?>