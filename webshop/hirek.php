<?php
	if (isset($_GET['nev_url']) && $_GET['nev_url'] != '') //  belépve
	{
		$res = $pdo->prepare("SELECT * FROM ".$webjel."hirek where nev_url='".$_GET['nev_url']."'");
		$res->execute();
		$row  = $res -> fetch();
		$time_input = $row['datum'];
		$date = DateTime::createFromFormat( 'Y-m-d', $time_input);
		$ujdatum = $date->format( 'Y.m.d.');
		print '<h3 class="padtop0">'.$row['cim'].'</h3>';
		// if ($row['kep'] != '' )
		// {
			// print '<img src="'.$domain.'/images/termekek/'.$row['kep'].'" width="400" style="float:right; margin: 0 0 10px 20px;"/>';
		// }
		// Kép
		$query_kep = "SELECT * FROM ".$webjel."hir_kepek WHERE hir_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
		$res = $pdo->prepare($query_kep);
		$res->execute();
		$row_kep = $res -> fetch();
		$alap_kep = $row_kep['kep'];
		if ($alap_kep != '' )
		{
			print '<img src="'.$domain.'/images/termekek/'.$alap_kep.'" style="width: 400px; max-width: 100%; float:right; margin: 0 0 10px 20px;"/>';
		}
		print $row['tartalom'];
		print '<p>'.$ujdatum.'</p>';
		print '<p style="height:20px;"><a href="'.$domain.'/hirek/" class="btn btn-default btn-sm margbot5" >Vissza</a></p>';
		
	}
	else // Hírek lista
	{
		$datum = date("Y-m-d");
		$query = "SELECT * FROM `".$webjel."hirek` Where `datum` <= '".$datum."' ORDER BY `datum` DESC, `id` DESC";
		$szamlali_hir = 0;
		foreach ($pdo->query($query) as $row)
		{
			$time_input = $row['datum'];
			$date = DateTime::createFromFormat( 'Y-m-d', $time_input);
			$datum_nap = $date->format( 'd');
			$datum_honap = $date->format( 'm');
			if($datum_honap == '01'){$datum_honap='JAN';}
			else if($datum_honap == '02'){$datum_honap='FEB';}
			else if($datum_honap == '03'){$datum_honap='MÁR';}
			else if($datum_honap == '04'){$datum_honap='ÁPR';}
			else if($datum_honap == '05'){$datum_honap='MÁJ';}
			else if($datum_honap == '06'){$datum_honap='JÚN';}
			else if($datum_honap == '07'){$datum_honap='JÚL';}
			else if($datum_honap == '08'){$datum_honap='AUG';}
			else if($datum_honap == '09'){$datum_honap='SZEP';}
			else if($datum_honap == '10'){$datum_honap='OKT';}
			else if($datum_honap == '11'){$datum_honap='NOV';}
			else if($datum_honap == '12'){$datum_honap='DEC';}
			
			print '<div class="row">
				<div class="grid_12" style="border-bottom: 2px solid #e0e0e0; margin-bottom:30px; padding-bottom:10px;">
					<div class="div5" style="margin-bottom:10px;"><h4>'.$row['cim'].'</h4></div>';
					// Kép
					$query_kep = "SELECT * FROM ".$webjel."hir_kepek WHERE hir_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
					$res = $pdo->prepare($query_kep);
					$res->execute();
					$row_kep = $res -> fetch();
					$alap_kep = $row_kep['kep'];
					if ($alap_kep != '') 
					{
						print '<figure style="margin:0;"><img src="'.$domain.'/images/termekek/'.$row_kep['kep'].'" alt="'.$row['nev_url'].'" style="float:left; margin:0 20px 20px 0; width:200px;"></figure>';
					}
					// if ($row['kep'] != '' )
					// {
						// print '<figure style="margin:0;"><img src="'.$domain.'/images/termekek/'.$row['kep'].'" alt="'.$row['nev_url'].'" style="float:left; margin:0 20px 20px 0; width:200px;"></figure>';
					// }
					print '<p>'.$row['elozetes'].'</p>
					<a href="'.$domain.'/hirek/'.$row['nev_url'].'" class="btn btn-default btn-sm margbot5" style="float: right;">Bővebben</a>
				</div>
			</div>';
		}
	}
?>
