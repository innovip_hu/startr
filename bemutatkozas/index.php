<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'bemutatkozas';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container text-justify">
                <h3 class="border">Bemutatkozás</h3>
				
				<img src="../images/rethi-istvan.jpg" class="kep_jobb">


				<p>A <strong>Start-R Biztosítási Alkusz Kft.</strong> 2001 óta működik ebben a formában. Szakmai vezetője <strong>Réthi István</strong>, 26 éve dolgozik a biztosítási szakmában, felsőfokú biztosítási szakértő oklevéllel, biztosítási tanácsadói szakképesítéssel és 8 év biztosító intézetben eltöltött szakmai vezetői tapasztalattal rendelkezik. Célunk a biztosítókkal szorosan együttműködve az ügyfeleink teljes körű, korrekt, megbízható kiszolgálása. Magyarország <strong><span class="link_bordo">15 biztosító társaságával</span></strong>, egyesületével állunk szerződéses kapcsolatban. Ügyfeleink megbízása alapján a cégünk kezelésében lévő szerződésekkel kapcsolatos <strong>szolgáltatásaink térítésmentesek</strong>.<br>
				Feladatunknak tartjuk, hogy megbízóink érdekeit szem előtt tartva a sokszínű biztosítási termékkínálatból segítsünk kiválasztani a legmegfelelőbb megoldást.<br>
				Biztosítási kapcsolatainkat, hozzáértésünket felajánljuk Önnek, családjának, vállalkozásának.</p>
				
				<p>A Start-R Biztosítási Alkusz Kft. a törvényi előírásnak megfelelően rendelkezik szakmai felelősségbiztosítással.</p>
				
				<p>Székhely: 6724 Szeged, Csemegi utca 11. Fsz. 3. 
				<br/>Adószám: 13161332-1-06 
				<br/>Cégjegyzékszám: 06-09-008900</p> 
				
				<p>Felügyeleti szerv: Magyar Nemzeti Bank (MNB)
				<br/>Felügyeleti engedély: E-II/103/2005; E-II/317/2005
				<br/>MNB nyilvántartási szám: 205031191642</p>
				
				<p>Felügyeleti szerv ügyfélszolgálata: 1013 Bupadest Krisztina krt. 39. (+3640 203 776)</p>
				
				<p>Adatkezelési nyilvántartási szám: NAIH-50986/2012
				<br/>Szakmai felelősségbiztosítás: Generali Biztosító Zrt.
				<br/>Kötvényszám: 95546101900501100</p>
				

            </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>