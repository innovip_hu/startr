<!DOCTYPE html>
<html lang="hu">
<head>
 	<?php
		$oldal = 'nyomtatvanyok';
		include '../config.php';
		include $gyoker.'/module/mod_head.php';
	?>
	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">
	<script>
		 $(window).on("load", function() {
			$('.sor').each(function(){  
				var highestBox = 0;
				$('.muntars_box', this).each(function(){

					if($(this).height() > highestBox) 
					   highestBox = $(this).height(); 
				});  
				$('.muntars_box',this).height(highestBox);
			});  
		});
	</script>
</head>

<body>
<?php
	include $gyoker.'/module/mod_body_start.php';
?>
<div class="page">
	<?php
		include $gyoker.'/module/mod_header.php';
	?>
    <main>
        <section class="well6">
            <div class="container">
                <h3 class="border">Nyomtatványok </h3>
				
					<div class="row sor">
						<div class="grid_2 wow zoomIn">
							<a href="startr_taj.pdf" target="_blank">
							<div class="box1 muntars_box">
								<div class="box_kep"><img src="tajekoztato.png"></div>
								<div class="box1_cnt">
									<p>Tájékoztató a biztosításközvetítőről </p>
								</div>
							</div>
							</a>
						</div>
						<div class="grid_2 wow zoomIn">
							<a href="megbizasi.pdf" target="_blank">
							<div class="box1 muntars_box">
								<div class="box_kep"><img src="megbizasi.png"></div>
								<div class="box1_cnt">
									<p>Megbízási szerződés</p>
								</div>
							</div>
							</a>
						</div>
						<div class="grid_2 wow zoomIn">
							<a href="felm_gpj.pdf" target="_blank">
							<div class="box1 muntars_box">
								<div class="box_kep"><img src="felb_gpj.png"></div>
								<div class="box1_cnt">
									<p>Biztosítási szerződés felmondása (gépjármű)</p>
								</div>
							</div>
							</a>
						</div>
						<div class="grid_2 wow zoomIn">
							<a href="felm_egyeb.pdf" target="_blank">
							<div class="box1 muntars_box">
								<div class="box_kep"><img src="felb_egyeb.png"></div>
								<div class="box1_cnt">
									<p>Biztosítási szerződés felmondása (egyéb)</p>
								</div>
							</div>
							</a>
						</div>
						<div class="grid_2 wow zoomIn">
							<a href="adasveteli.pdf" target="_blank">
							<div class="box1 muntars_box">
								<div class="box_kep"><img src="adasveteli.png"></div>
								<div class="box1_cnt">
									<p>Gépjármű adásvételi szerződés</p>
								</div>
							</div>
							</a>
						</div>
						<div class="grid_2 wow zoomIn">
							<a href="uzembentart_atruhazas.pdf" target="_blank">
							<div class="box1 muntars_box">
								<div class="box_kep"><img src="uzembentart.jpg"></div>
								<div class="box1_cnt">
									<p>Üzembentartó jog átruházásáról szóló szerződés</p>
								</div>
							</div>
							</a>
						</div>
					</div>

 					<div class="row sor">
						<div class="grid_2 wow zoomIn">
							<a href="gepjarmu-adasveteli-szerzodes_elktronikusan.pdf" target="_blank">
							<div class="box1 muntars_box">
								<div class="box_kep"><img src="gepjarmu-adasveteli-szerzodes_elktronikusan.jpg"></div>
								<div class="box1_cnt">
									<p>Gépjármű adásvételi szerződés ELEKTRONIKUSAN TÖLTHETŐ</p>
								</div>
							</div>
							</a>
						</div>
						<div class="grid_2 wow zoomIn">
							<a href="uzembentartoi-szerzodes-minta_elektronikusan.pdf" target="_blank">
							<div class="box1 muntars_box">
								<div class="box_kep"><img src="uzembentartoi-szerzodes-minta_elektronikusan.jpg"></div>
								<div class="box1_cnt">
									<p>Üzembentartó jog átruházásáról szóló szerződés ELEKTRONIKUSAN TÖLTHETŐ</p>
								</div>
							</div>
							</a>
						</div>						 						
						<div class="grid_2 wow zoomIn">
							<a href="baleset.pdf" target="_blank">
							<div class="box1 muntars_box">
								<div class="box_kep"><img src="baleset.pdf.png"></div>
								<div class="box1_cnt">
									<p>Baleset bejelentő</p>
								</div>
							</div>
							</a>
						</div>
					</div>
           </div> 
        </section>
	</main>

	<?php
		include $gyoker.'/module/mod_footer.php';
	?>
</div>
<?php
	include $gyoker.'/module/mod_body_end.php';
?>
</body>
</html>